# README

PubSub 프로세스 작성에 대한 규칙입니다.

## invoke()

PubSub 프로세스는 invoke 연산자를 오버로딩해야 합니다. invoke는 PubSubListener.kt에서 호출됩니다.

## throw GooglePubSubException

PubSub 프로세스 도중에 문제가 발생하면 GooglePubSubException을 던져야 합니다.
프로세스들은 담당하는 NotificationType에 따라 고유한 baseCode를 가지며 각 Exception은 발생한 프로세스의 baseCode를 offset과 결합하여 고유한 에러 코드를 가지게 됩니다.

GooglePubSubException은 GooglePubSubExceptionFactory를 통해 생성합니다.
