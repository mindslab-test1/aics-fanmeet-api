package ai.maum.kotlin.gcp.pubsub.core.proc

import ai.maum.kotlin.gcp.pubsub.core.NotificationType
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.jpa.membership.UserMembershipRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ProcessCancelled(
    val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    val userMembershipRepository: UserMembershipRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(NotificationType.SUBSCRIPTION_CANCELED)

    operator fun invoke(purchaseToken: String) {
        val history =
            userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
                ?: throw exceptionFactory.produce(
                    1,
                    "No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken"
                )
        userMembershipHistoryRepository.save(history)

        val userMembership = userMembershipRepository.findByPurchaseTokenAndActiveIsTrue(purchaseToken)
        // TODO: throw exception after 2 months of deployment
        //    ?: throw exceptionFactory.produce(2, "No such purchaseToken in UserMembership; purchaseToken=$purchaseToken")

        // Do nothing(purchase will not be renewed and expiration will be processed at ProcessExpired)
    }
}
