package ai.maum.kotlin.gcp.pubsub.core

enum class ResultCode(val value: Long) {
    OK(0),
    ERROR(1),
    UNEXPECTED_ERROR(2),
    UNDEFINED(-1)
}
