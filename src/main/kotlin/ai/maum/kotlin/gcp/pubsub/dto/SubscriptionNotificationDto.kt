package ai.maum.kotlin.gcp.pubsub.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SubscriptionNotificationDto(
    @JsonProperty("version")
    val version: String,
    @JsonProperty("notificationType")
    val notificationType: Int,
    @JsonProperty("purchaseToken")
    val purchaseToken: String,
    @JsonProperty("subscriptionId")
    val subscriptionId: String
)
