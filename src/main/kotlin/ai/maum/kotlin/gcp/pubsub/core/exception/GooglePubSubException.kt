package ai.maum.kotlin.gcp.pubsub.core.exception

class GooglePubSubException : RuntimeException {
    var errorCode: Long? = null

    constructor(errorCode: Long, message: String?, cause: Throwable?) : super(message, cause) {
        this.errorCode = errorCode
    }

    constructor(errorCode: Long, message: String?) : super(message) {
        this.errorCode = errorCode
    }

    constructor(errorCode: Long, cause: Throwable?) : super(cause) {
        this.errorCode = errorCode
    }

    constructor(errorCode: Long) : this(errorCode, "") {
        this.errorCode = errorCode
    }

    override val message: String
        get() = errorCode.toString() + " " + super.message
}
