package ai.maum.kotlin.gcp.pubsub.core

/**
 * Numberings for google pub/sub notification types.
 *
 * Reference: https://developer.android.com/google/play/billing/rtdn-reference
 *
 * @property value corresponding number for enumeration.
 */
enum class NotificationType(val value: Long) {
    /**
     * SUBSCRIPTION_RECOVERED(value:1)
     *
     * A subscription was recovered from account hold.
     */
    SUBSCRIPTION_RECOVERED(1) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_RENEWED(value:2)
     *
     * An active subscription was renewed.
     */
    SUBSCRIPTION_RENEWED(2) {
        override val valid: Boolean = true
    },

    /**
     * SUBSCRIPTION_CANCELED(value:3)
     *
     * A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.
     */
    SUBSCRIPTION_CANCELED(3) {
        override val valid: Boolean = true
    },

    /**
     * SUBSCRIPTION_PURCHASED(value:4)
     *
     * A new subscription was purchased.
     */
    SUBSCRIPTION_PURCHASED(4) {
        override val valid: Boolean = true
    },

    /**
     * SUBSCRIPTION_ON_HOLD(value:5)
     *
     * A subscription has entered account hold (if enabled).
     */
    SUBSCRIPTION_ON_HOLD(5) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_IN_GRACE_PERIOD(value:6)
     *
     * A subscription has entered grace period (if enabled).
     */
    SUBSCRIPTION_IN_GRACE_PERIOD(6) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_RESTARTED(value:7)
     *
     * User has reactivated their subscription from Play > Account > Subscriptions (requires opt-in for subscription restoration).
     */
    SUBSCRIPTION_RESTARTED(7) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_PRICE_CHANGE_CONFIRMED(value:8)
     *
     * A subscription price change has successfully been confirmed by the user.
     */
    SUBSCRIPTION_PRICE_CHANGE_CONFIRMED(8) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_DEFERRED(value:9)
     *
     * A subscription's recurrence time has been extended.
     */
    SUBSCRIPTION_DEFERRED(9) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_PAUSED(value:10)
     *
     * A subscription has been paused.
     */
    SUBSCRIPTION_PAUSED(10) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED(value:11)
     *
     * A subscription pause schedule has been changed.
     */
    SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED(11) {
        override val valid: Boolean = false
    },

    /**
     * SUBSCRIPTION_REVOKED(value:12)
     *
     * A subscription has been revoked from the user before the expiration time.
     */
    SUBSCRIPTION_REVOKED(12) {
        override val valid: Boolean = true
    },

    /**
     * SUBSCRIPTION_EXPIRED(value:13)
     *
     * A subscription has expired.
     */
    SUBSCRIPTION_EXPIRED(13) {
        override val valid: Boolean = true
    };

    /**
     * True if the notification type is used in this application.
     */
    abstract val valid: Boolean

    companion object {
        fun of(value: Long): NotificationType = values().find { it.value == value }!!
    }
}
