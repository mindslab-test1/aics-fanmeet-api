package ai.maum.kotlin.gcp.pubsub.core

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.integration.channel.DirectChannel
import org.springframework.messaging.MessageChannel

@Profile("prod")
@Configuration
class PubSubConfig {
    @Profile("prod")
    @Bean
    fun messageChannelAdapter(
        @Qualifier("inputChannel") inputChannel: MessageChannel,
        pubSubTemplate: PubSubTemplate
    ) = PubSubInboundChannelAdapter(pubSubTemplate, "MySub").also { it.outputChannel = inputChannel }

    @Profile("prod")
    @Bean
    fun inputChannel() = DirectChannel()
}
