package ai.maum.kotlin.gcp.pubsub.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SubscriptionPurchaseDto(
    @JsonProperty("version")
    val version: String,
    @JsonProperty("packageName")
    val packageName: String,
    @JsonProperty("eventTimeMillis")
    val eventTimeMillis: String,
    @JsonProperty("subscriptionNotification")
    val subscriptionNotification: SubscriptionNotificationDto
)
