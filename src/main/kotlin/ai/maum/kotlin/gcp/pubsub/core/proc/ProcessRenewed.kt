package ai.maum.kotlin.gcp.pubsub.core.proc

import ai.maum.kotlin.gcp.pubsub.core.NotificationType
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import ai.maum.kotlin.gcp.pubsub.core.helper.PurchaseInfoGetter
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.jpa.membership.UserMembershipRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.File
import java.time.Instant
import java.util.*

@Component
class ProcessRenewed(
    val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    val userMembershipRepository: UserMembershipRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(NotificationType.SUBSCRIPTION_RENEWED)

    operator fun invoke(purchaseToken: String, packageName: String) {
        val recentMembership = userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
            ?: throw exceptionFactory.produce(1, "No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken")
        val membership = recentMembership.membership
            ?: throw exceptionFactory.produce(2, "Membership not available; recentMembership.membership=${recentMembership.membership?.id?.toString()}")

        if (recentMembership.status != UserMembershipHistory.Status.NORMAL)
            throw exceptionFactory.produce(
                3,
                "Renew request for invalid purchase(status not NORMAL); recentMembership.id=${recentMembership.id!!}, recentMembership.status=${recentMembership.status!!.name}"
            )

        val purchase = PurchaseInfoGetter(
            packageName = packageName,
            productId = membership.productId!!,
            purchaseToken = purchaseToken,
            serviceAccount = File("serviceAccount.json")
        )()
            ?: throw exceptionFactory.produce(4, "Could not retrieve purchase info from google; productId=${membership.productId!!}, purchaseToke$purchaseToken")

        recentMembership.status = UserMembershipHistory.Status.EXPIRED
        recentMembership.expirationDate = Instant.now()
        userMembershipHistoryRepository.save(recentMembership)

        val newMembership = UserMembershipHistory()
        newMembership.userId = recentMembership.userId
        newMembership.membership = membership
        newMembership.purchaseToken = purchaseToken
        newMembership.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
        newMembership.status = UserMembershipHistory.Status.NORMAL
        userMembershipHistoryRepository.save(newMembership)

        val userMembership = userMembershipRepository.findByPurchaseTokenAndActiveIsTrue(purchaseToken)
        // TODO: throw exception after 2 months of deployment
        //    ?: throw exceptionFactory.produce(2, "No such purchaseToken in UserMembership; purchaseToken=$purchaseToken")

        // TODO: reduce 'let' clause after 2 months of deployment
        userMembership?.let {
            it.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
            userMembershipRepository.save(it)
        }
    }
}
