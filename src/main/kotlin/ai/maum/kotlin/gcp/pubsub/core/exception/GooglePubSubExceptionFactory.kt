package ai.maum.kotlin.gcp.pubsub.core.exception

import ai.maum.kotlin.gcp.pubsub.core.NotificationType

class GooglePubSubExceptionFactory {
    constructor(_baseCode: Long) {
        baseCode = _baseCode
    }

    constructor(notificationType: NotificationType) {
        baseCode = notificationType.value * 100
    }

    private val baseCode: Long

    fun produce(offset: Long, message: String) = GooglePubSubException(baseCode + offset, message)
}
