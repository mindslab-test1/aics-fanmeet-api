package ai.maum.kotlin.gcp.pubsub.core.proc

import ai.maum.kotlin.gcp.pubsub.core.NotificationType
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import ai.maum.kotlin.jpa.membership.UserMembership
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.jpa.membership.UserMembershipRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class ProcessRevoked(
    val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    val userMembershipRepository: UserMembershipRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(NotificationType.SUBSCRIPTION_REVOKED)

    operator fun invoke(purchaseToken: String) {
        val history =
            userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
                ?: throw exceptionFactory.produce(1, "No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken")
        history.status = UserMembershipHistory.Status.TO_BE_EXPIRED
        userMembershipHistoryRepository.save(history)

        val userMembership = userMembershipRepository.findByPurchaseTokenAndActiveIsTrue(purchaseToken)
        // TODO: throw exception after 2 months of deployment
        //    ?: throw exceptionFactory.produce(2, "No such purchaseToken in UserMembership; purchaseToken=$purchaseToken")

        // TODO: reduce 'let' clause after 2 months of deployment
        userMembership?.let {
            it.status = UserMembership.Status.EXPIRED
            it.expirationDate = Instant.now()
            userMembershipRepository.save(it)
        }
    }
}
