package ai.maum.kotlin.gcp.pubsub.core.helper

import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.*
import ai.maum.kotlin.jpa.membership.UserMembershipHistory.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import java.io.File
import java.util.*

@Component
class NewSubscriptionHandler(
    private val membershipRepository: MembershipRepository,
    private val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    private val userMembershipRepository: UserMembershipRepository,
    private val userRepository: UserRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(_baseCode = 2100)

    @Value("\${android.package.name}")
    lateinit var packageName: String
    @Value("classpath:gcp/serviceAccount.json")
    lateinit var serviceAccount: Resource

    fun run(userId: Long, purchaseToken: String, productId: String) {
        val product = membershipRepository.findByProductIdAndActiveIsTrue(productId)
            ?: throw exceptionFactory.produce(1, "NewSubscriptionHandler::invoke(): Membership not found; productId=$productId")

        val purchase = PurchaseInfoGetter(
            packageName = packageName,
            productId = productId,
            purchaseToken = purchaseToken,
            serviceAccount = File("serviceAccount.json")
        )()
            ?: throw exceptionFactory.produce(
                2,
                "NewSubscriptionHandler::invoke(): Could not retrieve purchase info from google. (\'$packageName\', \'${product.productId!!}\', \'$purchaseToken\')"
            )

        val userMembershipHistory = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, product.celeb!!)
        userMembershipHistory?.let {
            if (userMembershipHistory.status!! == Status.NORMAL || userMembershipHistory.status!! == Status.TO_BE_EXPIRED) {
                // using temporary offset because this exception will be deprecated.
                throw exceptionFactory.produce(51, "NewSubscriptionHandler::invoke(): Conflict; User already has membership from same celeb.")
            }
        }

        val newMembership = UserMembershipHistory()
        newMembership.userId = userId
        newMembership.membership = product
        newMembership.purchaseToken = purchaseToken
        newMembership.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
        newMembership.status = Status.NORMAL
        userMembershipHistoryRepository.save(newMembership)

        val duplicateToken = userMembershipRepository.findByPurchaseTokenAndActiveIsTrue(purchaseToken)?.let {
            throw exceptionFactory.produce(3, "NewSubscriptionHandler::invoke(): Duplicate purchase with purchaseToken=$purchaseToken")
        }

        val user = userRepository.findUserByIdAndActiveIsTrue(userId)
            ?: throw exceptionFactory.produce(4, "NewSubscriptionHandler::invoke(): Could not find user with userId=$userId")
        val userMembership = UserMembership()
        userMembership.user = user
        userMembership.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
        userMembership.status = UserMembership.Status.NORMAL
        userMembership.paymentPlatform = UserMembership.Platform.GOOGLE
        userMembership.membership = product
        userMembership.purchaseToken = purchaseToken
        userMembershipRepository.save(userMembership)
    }
}
