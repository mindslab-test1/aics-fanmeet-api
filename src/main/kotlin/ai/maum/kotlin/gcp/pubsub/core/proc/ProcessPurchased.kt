package ai.maum.kotlin.gcp.pubsub.core.proc

import ai.maum.kotlin.gcp.pubsub.core.NotificationType
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ProcessPurchased {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(NotificationType.SUBSCRIPTION_PURCHASED)

    operator fun invoke(purchaseToken: String) {
        // Initial purchase is processed by controller(which calls NewSubscriptionHandler)
    }
}
