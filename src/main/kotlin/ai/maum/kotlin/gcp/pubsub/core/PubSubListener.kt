package ai.maum.kotlin.gcp.pubsub.core

import ai.maum.kotlin.gcp.pubsub.core.NotificationType.*
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubException
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import ai.maum.kotlin.gcp.pubsub.core.proc.*
import ai.maum.kotlin.gcp.pubsub.dto.SubscriptionPurchaseDto
import ai.maum.kotlin.jpa.membership.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.core.io.Resource
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.stereotype.Component
import java.time.Instant

@Profile("prod")
@Component
class PubSubListener(
    private val pubSubListenerHistoryRepository: PubSubListenerHistoryRepository,
    private val userMembershipRepository: UserMembershipRepository,
    private val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    private val googlePubSubListenerHistoryRepository: GooglePubSubListenerHistoryRepository,
    private val processCancelled: ProcessCancelled,
    private val processDeferred: ProcessDeferred,
    private val processExpired: ProcessExpired,
    private val processInGracePeriod: ProcessInGracePeriod,
    private val processOnHold: ProcessOnHold,
    private val processPaused: ProcessPaused,
    private val processPauseScheduleChanged: ProcessPauseScheduleChanged,
    private val processPriceChangeConfirmed: ProcessPriceChangeConfirmed,
    private val processPurchased: ProcessPurchased,
    private val processRecovered: ProcessRecovered,
    private val processRenewed: ProcessRenewed,
    private val processRestarted: ProcessRestarted,
    private val processRevoked: ProcessRevoked
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(0)

    @Value("classpath:gcp/serviceAccount.json")
    lateinit var serviceAccountResource: Resource

    @ServiceActivator(inputChannel = "inputChannel")
    fun messageReceiver(payload: String) {
        val googlePubSubListenerHistory = GooglePubSubListenerHistory()
        googlePubSubListenerHistory.resultCode = ResultCode.UNDEFINED.value

        logger.info(payload)
        try {
            googlePubSubListenerHistory.eventTimeMillis = Instant.now().toEpochMilli()

            val json = ObjectMapper().readValue(payload, SubscriptionPurchaseDto::class.java)
            val notificationType = NotificationType.of(json.subscriptionNotification.notificationType.toLong())
            val purchaseToken = json.subscriptionNotification.purchaseToken
            val packageName = json.packageName

            googlePubSubListenerHistory.notificationType = notificationType.value
            googlePubSubListenerHistory.purchaseToken = purchaseToken
            googlePubSubListenerHistory.subscriptionId = json.subscriptionNotification.subscriptionId

            when (notificationType) {
                SUBSCRIPTION_PURCHASED -> processPurchased(purchaseToken = purchaseToken)
                SUBSCRIPTION_RENEWED -> processRenewed(purchaseToken = purchaseToken, packageName = packageName)
                SUBSCRIPTION_CANCELED -> processCancelled(purchaseToken = purchaseToken)
                SUBSCRIPTION_REVOKED -> processRevoked(purchaseToken = purchaseToken)
                SUBSCRIPTION_EXPIRED -> processExpired(purchaseToken = purchaseToken)
                else -> throw exceptionFactory.produce(1, "Notification type not processed")
            }

            googlePubSubListenerHistory.resultCode = ResultCode.OK.value
        } catch (e: GooglePubSubException) {
            logger.error(e.message)
            logger.error(e.stackTrace.toString())

            googlePubSubListenerHistory.resultCode = ResultCode.ERROR.value
            googlePubSubListenerHistory.errorCode = e.errorCode
        } catch (e: Exception) {
            logger.error(e.message)
            logger.error(e.stackTrace.toString())

            googlePubSubListenerHistory.resultCode = ResultCode.UNEXPECTED_ERROR.value
        } finally {
            saveHistory(payload)
            googlePubSubListenerHistoryRepository.save(googlePubSubListenerHistory)
        }
    }

    private fun saveHistory(string: String) {
        val pubSubListenerHistory = PubSubListenerHistory()
        pubSubListenerHistory.string = string
        pubSubListenerHistoryRepository.save(pubSubListenerHistory)
    }
}
