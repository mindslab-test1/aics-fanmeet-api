package ai.maum.kotlin.gcp.pubsub.core.helper

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.androidpublisher.AndroidPublisher
import com.google.api.services.androidpublisher.AndroidPublisherScopes
import com.google.api.services.androidpublisher.model.SubscriptionPurchase
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.GoogleCredentials
import java.io.File
import java.io.FileInputStream

class PurchaseInfoGetter(
    private val packageName: String,
    private val productId: String,
    private val purchaseToken: String,
    private val serviceAccount: File
) : () -> SubscriptionPurchase? {

    override operator fun invoke(): SubscriptionPurchase? {
        val credentials = GoogleCredentials.fromStream(FileInputStream(serviceAccount)).createScoped(AndroidPublisherScopes.ANDROIDPUBLISHER)

        val jsonFactory = JacksonFactory.getDefaultInstance()
        val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
        val httpCredentialsAdapter = HttpCredentialsAdapter(credentials)
        val publisher = AndroidPublisher.Builder(httpTransport, jsonFactory, httpCredentialsAdapter).setApplicationName(packageName).build()

        val get = publisher.purchases().subscriptions().get(packageName, productId, purchaseToken)

        return get.execute()
    }
}
