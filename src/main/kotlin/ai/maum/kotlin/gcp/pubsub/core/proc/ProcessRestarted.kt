package ai.maum.kotlin.gcp.pubsub.core.proc

import ai.maum.kotlin.gcp.pubsub.core.NotificationType
import ai.maum.kotlin.gcp.pubsub.core.exception.GooglePubSubExceptionFactory
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ProcessRestarted {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val exceptionFactory = GooglePubSubExceptionFactory(NotificationType.SUBSCRIPTION_RESTARTED)

    operator fun invoke(purchaseToken: String) {
        TODO("Not yet implemented")
    }
}
