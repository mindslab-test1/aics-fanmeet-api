package ai.maum.kotlin.jpa.membership

import org.springframework.data.repository.CrudRepository

interface UserMembershipRepository : CrudRepository<UserMembership, Long> {
    fun findByPurchaseTokenAndActiveIsTrue(purchaseToken: String): UserMembership?
}
