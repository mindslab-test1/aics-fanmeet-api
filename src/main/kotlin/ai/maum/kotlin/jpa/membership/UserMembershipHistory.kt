package ai.maum.kotlin.jpa.membership

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class UserMembershipHistory() : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "USER_MEMBERSHIP_HISTORY_SEQ_GEN",
            sequenceName = "USER_MEMBERSHIP_HISTORY_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_MEMBERSHIP_HISTORY_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var userId: Long? = null

    @ManyToOne
    var membership: Membership? = null

    @Column(nullable = false)
    var purchaseToken: String? = null

    @Column(nullable = false)
    var expirationDate: Instant? = null

    @Column(nullable = false)
    var status: Status? = Status.NORMAL

    @Column(nullable = true)
    var orgTranId: String? = null

    @Column(nullable = true)
    var receiptFilePath: String? = null
    enum class Status {
        NORMAL,
        TO_BE_EXPIRED,
        EXPIRED
    }
}