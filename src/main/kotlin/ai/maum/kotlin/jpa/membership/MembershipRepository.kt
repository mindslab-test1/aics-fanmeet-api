package ai.maum.kotlin.jpa.membership

import org.springframework.data.repository.CrudRepository

interface MembershipRepository : CrudRepository<Membership, Long> {
    fun findByIdAndActiveIsTrue(id: Long): Membership?
    fun findByProductIdAndActiveIsTrue(productId: String): Membership?
    fun findByCelebAndTierAndActiveIsTrue(celeb: Long, tier: Long): Membership?
    fun findByCelebAndActiveIsTrue(celeb: Long): List<Membership>
}