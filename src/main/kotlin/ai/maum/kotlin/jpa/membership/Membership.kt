package ai.maum.kotlin.jpa.membership

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Membership : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "MEMBERSHIP_SEQ_GEN",
            sequenceName = "MEMBERSHIP_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEMBERSHIP_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var tier: Long? = null

    @Column(nullable = false)
    var name: String? = null

    @Column(nullable = false)
    var price: Long? = null

    @Column(nullable = false)
    var badgeImageUrl: String? = null

    // <<< Membership Benefit Flags
    @Column(nullable = false)
    var badge: Boolean? = false

    @Column(nullable = false)
    var sticker: Boolean? = false

    @Column(nullable = false)
    var voiceLetter: Boolean? = false

    @Column(nullable = false)
    var ttsWrite: Boolean? = false
    // Membership Benefit Flags >>>

    // <<< Membership Benefit Values
    @Column(nullable = false)
    var ttsWriteLimit: Long? = 0

    // Membership Benefit Values >>>

    @Column(nullable = false)
    var productId: String? = null
}