package ai.maum.kotlin.jpa.membership

import org.springframework.data.repository.CrudRepository

interface GooglePubSubListenerHistoryRepository : CrudRepository<GooglePubSubListenerHistory, Long>
