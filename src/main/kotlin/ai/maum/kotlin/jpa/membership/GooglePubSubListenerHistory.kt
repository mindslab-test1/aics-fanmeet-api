package ai.maum.kotlin.jpa.membership

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
@Table(name = "GOOGLE_PUS_SUB_HISTORY")
class GooglePubSubListenerHistory : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "GOOGLE_HISTORY_SEQ_GEN",
        sequenceName = "GOOGLE_HISTORY_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GOOGLE_HISTORY_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var eventTimeMillis: Long? = null

    @Column(nullable = false)
    var notificationType: Long? = null

    @Column(nullable = false)
    var purchaseToken: String? = null

    @Column(nullable = false)
    var subscriptionId: String? = null

    @Column(nullable = false)
    var resultCode: Long? = null

    @Column(nullable = true)
    var errorCode: Long? = null
}
