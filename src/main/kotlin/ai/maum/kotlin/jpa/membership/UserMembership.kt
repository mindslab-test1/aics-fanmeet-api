package ai.maum.kotlin.jpa.membership

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.user.User
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class UserMembership() : BaseEntity() {
    @Id
    @SequenceGenerator(
        name = "USER_MEMBERSHIP_SEQ_GEN",
        sequenceName = "USER_MEMBERSHIP_SEQ",
        initialValue = 1,
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_MEMBERSHIP_SEQ_GEN")
    var id: Long? = null

    @ManyToOne
    var user: User? = null

    @ManyToOne
    var membership: Membership? = null

    @Column(nullable = false)
    var expirationDate: Instant? = null

    @Column(unique = true)
    var purchaseToken: String? = null

    @Column(nullable = false)
    var status: Status? = null
        get() = when (paymentPlatform) {
            Platform.GOOGLE -> field
            Platform.APPLE ->
                if (expirationDate!! < Instant.now()) {
                    field = Status.EXPIRED
                    field
                } else {
                    field
                }
            else -> field
        }

    enum class Status {
        NORMAL,
        EXPIRED
    }

    @Column(nullable = false)
    var paymentPlatform: Platform? = null

    enum class Platform {
        GOOGLE,
        APPLE
    }
}
