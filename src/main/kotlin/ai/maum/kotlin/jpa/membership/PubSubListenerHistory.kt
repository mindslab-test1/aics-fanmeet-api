package ai.maum.kotlin.jpa.membership

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class PubSubListenerHistory : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "PUB_SUB_LISTENER_HISTORY_SEQ_GEN",
            sequenceName = "PUB_SUB_LISTENER_HISTORY_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PUB_SUB_LISTENER_HISTORY_SEQ_GEN")
    var id: Long? = null

    @Column(length = 1000)
    var string: String? = null
}