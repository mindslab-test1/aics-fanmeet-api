//package ai.maum.kotlin.jpa.membership
//
//import ai.maum.kotlin.jpa.AllOpen
//import ai.maum.kotlin.jpa.common.BaseEntity
//import javax.persistence.*
//
//@AllOpen
//@Entity
//class AppleShoppingHistory : BaseEntity() {
//    @Id
//    @SequenceGenerator(
//            name = "ISHID_SEQ_GEN",
//            sequenceName = "ISHID_SEQ",
//            initialValue = 1,
//            allocationSize = 1
//    )
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISHID_SEQ_GEN")
//    var ishId: Long? = null
//
//    @Column(nullable = false)
//    var iosShoppingId: Long? = null
//
//    @Column(nullable = false)
//    var expDate: String? = null
//
//    @Column(nullable = false)
//    var goodsId: String? = null
//
//    @Column(nullable = false)
//    var renewState: String? = null
//
//    @Column(nullable = false)
//    var notiType: String? = null
//
//    @Column(nullable = false)
//    var webOrderId: String? = null
//
//    @Column(nullable = false)
//    var crtDate: String? = null
//
//
//
//}