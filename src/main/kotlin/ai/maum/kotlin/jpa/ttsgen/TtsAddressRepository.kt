package ai.maum.kotlin.jpa.ttsgen

import org.springframework.data.repository.CrudRepository

interface TtsAddressRepository : CrudRepository<TtsAddress, Long> {
    fun findByCeleb(celeb: Long) : TtsAddress?
}