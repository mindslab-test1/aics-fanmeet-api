package ai.maum.kotlin.jpa.common.scrap

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class ScrapBoard : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "SCRAP_BOARD_SEQ_GEN",
            sequenceName = "SCRAP_BOARD_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCRAP_BOARD_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var ownerId: Long? = null

    @Column(nullable = false)
    var name: String? = null
}