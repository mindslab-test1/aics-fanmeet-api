package ai.maum.kotlin.jpa.common.scrap

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Scrap : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "SCRAP_SEQ_GEN",
            sequenceName = "SCRAP_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCRAP_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var targetId: Long? = null

    @Column(nullable = false)
    var targetType: TargetType? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var board: Long? = null

    enum class TargetType {
        NONE,
        USER_FEED,

        //USER_FEED_COMMENT,
        CELEB_FEED,

        //CELEB_FEED_COMMENT,
        CONTENT,
        //CONTENT_COMMENT
    }
}