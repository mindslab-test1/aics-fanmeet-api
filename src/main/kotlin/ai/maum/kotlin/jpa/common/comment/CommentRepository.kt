package ai.maum.kotlin.jpa.common.comment

import org.springframework.data.repository.CrudRepository

interface CommentRepository : CrudRepository<Comment, Long> {
    fun findByIdAndWhoAndActiveIsTrue(id: Long, who: Long) : Comment?
    fun findByIdAndActiveIsTrue(id: Long) : Comment?
    fun existsByIdAndActiveIsTrue(id: Long) : Boolean
    fun findByFeedAndSectionOrderByCreatedDesc(feed: Long, section: Comment.Section) : List<Comment>?
}