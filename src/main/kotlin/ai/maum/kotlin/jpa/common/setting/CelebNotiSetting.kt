package ai.maum.kotlin.jpa.common.setting

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class CelebNotiSetting : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEB_NOTI_SETTING_SEQ_GEN",
            sequenceName = "CELEB_NOTI_SETTING_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEB_NOTI_SETTING_SEQ_GEN")
    var id: Long? = null

    var who: Long? = null

    var whom: Long? = null
}