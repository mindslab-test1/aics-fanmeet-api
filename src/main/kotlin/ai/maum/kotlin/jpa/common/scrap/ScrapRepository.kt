package ai.maum.kotlin.jpa.common.scrap

import org.springframework.data.repository.CrudRepository

interface ScrapRepository : CrudRepository<Scrap, Long> {
    fun findByIdAndActiveIsTrue(id: Long): Scrap?
    fun findAllByBoardAndActiveIsTrue(board: Long): List<Scrap>?
    fun countAllByBoardAndActiveIsTrue(board: Long): Long
    fun existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId: Long, targetId: Long, targetType: Scrap.TargetType): Boolean
    fun findByTargetIdAndTargetTypeAndActiveIsTrue(targetId: Long, targetType: Scrap.TargetType): List<Scrap>
}