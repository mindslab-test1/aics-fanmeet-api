package ai.maum.kotlin.jpa.common.content

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeed

data class CelebRecentActivityVo(
        val id: Long,
        val timestamp: String,
        val who: Long,
        val text: String?,
        val mediaUrl: String?,
        val type: String
) {
    constructor(content: Content) : this(
            id = content.id!!,
            timestamp = content.created.toString(),
            who = content.who!!,
            text = content.text,
            mediaUrl = if (content.pictureList != null && !content.pictureList!!.isEmpty() && content.pictureList!!.size > 0) content.pictureList?.get(0)?.url else null,
            type = "content"
    )

    constructor(celebFeed: CelebFeed) : this(
            id = celebFeed.id!!,
            timestamp = celebFeed.created.toString(),
            who = celebFeed.who!!,
            text = celebFeed.text,
            mediaUrl = if (celebFeed.pictureList != null && !celebFeed.pictureList!!.isEmpty() && celebFeed.pictureList!!.size > 0) celebFeed.pictureList?.get(0)?.url else null,
            type = "celebFeed"
    )
}