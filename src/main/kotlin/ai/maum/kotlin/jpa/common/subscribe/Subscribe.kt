package ai.maum.kotlin.jpa.common.subscribe

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Subscribe : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "SUBSCRIBE_SEQ_GEN",
            sequenceName = "SUBSCRIBE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUBSCRIBE_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var whom: Long? = null
}