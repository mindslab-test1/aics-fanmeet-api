package ai.maum.kotlin.jpa.common.report

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Report : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "REPORT_SEQ_GEN",
            sequenceName = "REPORT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REPORT_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var whom: Long? = null

    @Column(nullable = false)
    var which: Long? = null

    @Column(nullable = false)
    var what: ReportType? = null

    @Column(nullable = false)
    var judge: Long? = null

    var reason: String? = null

    enum class ReportType {
        NONE,
        USER_FEED,
        USER_FEED_COMMENT,
        CELEB_FEED,
        CELEB_FEED_COMMENT,
        CONTENT,
        CONTENT_COMMENT,
        TTS
    }
}