package ai.maum.kotlin.jpa.common.content

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class ContentFile : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CONTENT_FILE_SEQ_GEN",
            sequenceName = "CONTENT_FILE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTENT_FILE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var url: String? = null

    var blurredUrl: String? = null

    @Column(nullable = false)
    var what: FileType? = null
    enum class FileType {
        IMAGE,
        VIDEO
    }
}