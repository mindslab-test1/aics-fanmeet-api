package ai.maum.kotlin.jpa.common.feed

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class FeedCategory: BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "FEED_CATEGORY_SEQ_GEN",
            sequenceName = "FEED_CATEGORY_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEED_CATEGORY_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var name: String? = null
}