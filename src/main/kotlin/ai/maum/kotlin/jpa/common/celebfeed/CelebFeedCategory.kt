package ai.maum.kotlin.jpa.common.celebfeed

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class CelebFeedCategory: BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEBFEED_CATEGORY_SEQ_GEN",
            sequenceName = "CELEBFEED_CATEGORY_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEBFEED_CATEGORY_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var name: String? = null
}