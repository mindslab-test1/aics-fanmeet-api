package ai.maum.kotlin.jpa.common.celebfeed

import org.springframework.data.repository.CrudRepository

interface CelebFeedCategoryRepository : CrudRepository<CelebFeedCategory, Long> {
    fun findByIdAndActiveIsTrue(id: Long): CelebFeedCategory?
    fun findByActiveIsTrue(): List<CelebFeedCategory>
    fun findByCelebAndActiveIsTrueOrderByCreatedAsc(celeb: Long): List<CelebFeedCategory>
}