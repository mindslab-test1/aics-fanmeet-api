package ai.maum.kotlin.jpa.common.like

import org.springframework.data.repository.CrudRepository

interface LikeRepository : CrudRepository<Like, Long> {
    fun existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId: Long, targetId: Long, targetType: Like.TargetType): Boolean
    fun findByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId: Long, targetId: Long, targetType: Like.TargetType) : Like?
    fun findByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId: Long, targetId: Long, targetType: Like.TargetType) : Like?
    fun findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId: Long, targetId: Long, targetType: Like.TargetType) : List<Like>
    fun findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId: Long, targetId: Long, targetType: Like.TargetType) : List<Like>

    fun findByUserIdAndActiveIsTrueOrderByTargetIdAscTargetTypeAsc(userId: Long): List<Like>
    fun findByUserIdAndActiveIsFalseOrderByTargetIdAscTargetTypeAsc(userId: Long): List<Like>
}