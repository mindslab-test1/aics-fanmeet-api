package ai.maum.kotlin.jpa.common.report

data class ReportVo(
        val created: String,
        val updated: String,
        val active: Boolean,
        val id: Long,
        val who: Long,
        val whom: Long,
        val which: Long,
        val what: Report.ReportType,
        val judge: Long,
        val reason: String?
) {
    constructor(report: Report) : this(
            created = report.created.toString(),
            updated = report.updated.toString(),
            active = report.active!!,
            id = report.id!!,
            who = report.who!!,
            whom = report.whom!!,
            which = report.which!!,
            what = report.what!!,
            judge = report.judge!!,
            reason = report.reason
    )
}