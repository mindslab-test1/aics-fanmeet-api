package ai.maum.kotlin.jpa.common.ignore

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Ignore : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "IGNORE_SEQ_GEN",
            sequenceName = "IGNORE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IGNORE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var whom: Long? = null
}