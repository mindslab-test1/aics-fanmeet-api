package ai.maum.kotlin.jpa.common.interest

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface UserInterestedRepository : CrudRepository<UserInterested, Long> {
    @Query("select c.what from UserInterested c where c.who = :userId and c.active = true")
    fun findUserInterestedListByUserId(@Param("userId") userId : Long) : List<Long>?

    fun findWhatByWho(who: Long) : List<Long>
}