package ai.maum.kotlin.jpa.common.interest

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class ActiveInterest : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "ACTIVE_INTEREST_SEQ_GEN",
            sequenceName = "ACTIVE_INTEREST_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACTIVE_INTEREST_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var what: Long? = null
}