package ai.maum.kotlin.jpa.common.comment

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class CommentFile : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "FEED_FILE_SEQ_GEN",
            sequenceName = "FEED_FILE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEED_FILE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var url: String? = null

    @Column(nullable = false)
    var parent: Long? = null

    enum class ContentType {
        NONE,
        IMAGE,
        VIDEO
    }
}