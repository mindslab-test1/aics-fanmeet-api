package ai.maum.kotlin.jpa.common.message

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.service.Tts
import javax.persistence.*

@AllOpen
@Entity
class Message : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEB_MESSAGE_SEQ_GEN",
            sequenceName = "CELEB_MESSAGE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEB_MESSAGE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var messageType: MessageType? = null

    // If messageType = USER_MESSAGE
    var whom: Long? = null

    var text: String? = null

    @ManyToOne
    var tts: Tts? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var file: MessageFile? = null

    enum class MessageType {
        CELEB_MESSAGE,
        USER_MESSAGE
    }
}