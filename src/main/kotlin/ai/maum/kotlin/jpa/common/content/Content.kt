package ai.maum.kotlin.jpa.common.content

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.feed.FeedFile
import ai.maum.kotlin.jpa.common.service.Tts
import org.hibernate.annotations.ColumnDefault
import javax.persistence.*

@AllOpen
@Entity
class Content : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CONTENT_SEQ_GEN",
            sequenceName = "CONTENT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTENT_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var title: String? = null

    @Column(columnDefinition = "CLOB NOT NULL")
    @Lob
    var text: String? = null

    @Column(nullable = false)
    var category: Long? = null

    @Column(nullable = false)
    var likeCount: Long? = 0

    @Column(nullable = false)
    var scrapCount: Long? = 0

    @Column(nullable = false)
    var commentCount: Long? = 0

    @Column(nullable = false)
    var viewCount: Long? = 0

    @Column(nullable = false)
    var accessLevel: Long? = 1

    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "content")
    var pictureList: MutableList<ContentFile>? = null

    @ManyToOne
    var tts: Tts? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var video : ContentFile? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var thumbnail : ContentFile? = null

    var youtube : String?  = null

    @Column(nullable = false)
    @ColumnDefault("0")
    var modifyYn: Long? = null
}