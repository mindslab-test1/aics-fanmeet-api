package ai.maum.kotlin.jpa.common.celebfeed

import org.springframework.data.domain.Sort
import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface CelebFeedRepository : CrudRepository<CelebFeed, Long> {
    fun findByIdAndWhoAndActiveIsTrue(id: Long, who: Long): CelebFeed?
    fun findByIdAndActiveIsTrue(id: Long): CelebFeed?
    fun findByWhoAndActiveIsTrue(who: Long): List<CelebFeed>
    fun findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(who: Long, accessLevel: Long): CelebFeed?

    fun findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop2ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop3ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop4ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop5ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop6ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop7ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop8ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop9ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop10ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop11ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop12ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop13ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop14ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop15ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop16ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop17ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop18ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop19ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop20ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop21ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop22ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop23ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop24ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop25ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop26ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop27ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop28ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop29ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop30ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop2ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop3ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop4ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop5ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop6ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop7ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop8ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop9ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop10ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop11ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop12ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop13ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop14ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop15ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop16ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop17ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop18ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop19ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop20ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop21ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop22ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop23ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop24ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop25ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop26ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop27ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop28ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop29ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
    fun findTop30ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<CelebFeed>?
}