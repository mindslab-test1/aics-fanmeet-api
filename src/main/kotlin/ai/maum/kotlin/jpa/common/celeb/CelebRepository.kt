package ai.maum.kotlin.jpa.common.celeb

import ai.maum.kotlin.jpa.common.user.User
import org.springframework.data.repository.CrudRepository

interface CelebRepository : CrudRepository<Celeb, Long> {
//    @Query("select u from User u left join Subscribe s on u.id = s.who left join Celeb c on c.userId = s.who")
//    fun findAllCelebByWhom(who : Long) : List<User>?
    fun existsByUserAndActiveIsTrue(user: User): Boolean
    fun findByUserAndActiveIsTrue(user: User): Celeb?
}