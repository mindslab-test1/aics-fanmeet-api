package ai.maum.kotlin.jpa.common.celebfeed

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class CelebFeedFile : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEB_FEED_FILE_SEQ_GEN",
            sequenceName = "CELEB_FEED_FILE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEB_FEED_FILE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var url: String? = null

    @Column(nullable = false)
    var what: FileType? = null
    enum class FileType {
        IMAGE,
        VIDEO
    }
}