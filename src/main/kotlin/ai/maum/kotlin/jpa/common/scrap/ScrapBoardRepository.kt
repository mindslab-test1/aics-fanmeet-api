package ai.maum.kotlin.jpa.common.scrap

import org.springframework.data.repository.CrudRepository
import javax.transaction.Transactional

interface ScrapBoardRepository : CrudRepository<ScrapBoard, Long> {
    fun existsByIdAndActiveIsTrue(id: Long): Boolean?
    fun findByIdAndOwnerIdAndActiveIsTrue(id: Long, ownerId: Long): ScrapBoard?
    fun findAllByOwnerIdAndActiveIsTrue(userId: Long): List<ScrapBoard>?
    fun findTop2ByOwnerIdAndActiveIsTrue(ownerId: Long): List<ScrapBoard>?
}