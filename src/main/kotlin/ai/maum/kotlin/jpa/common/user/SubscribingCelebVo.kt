package ai.maum.kotlin.jpa.common.user

data class SubscribingCelebVo(
        val id: Long,
        val profileImageUrl: String?,
        val name: String
) {
    constructor(user: User) : this(
            id = user.id!!,
            profileImageUrl = user.profileImageUrl,
            name = user.name!!
    )
}