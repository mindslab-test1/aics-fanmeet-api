package ai.maum.kotlin.jpa.common.interest

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Interest : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "INTEREST_SEQ_GEN",
            sequenceName = "INTEREST_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INTEREST_SEQ_GEN")
    var id: Long? = null

    var name: String? = null
    var icon: String? = null
}