package ai.maum.kotlin.jpa.common.user

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface UserRepository : CrudRepository<User, Long> {
    fun findUserById(id: Long): User?
    fun findUserByIdAndActiveIsTrue(id: Long): User?

    fun findByActiveIsTrue(): List<User>

    @Query("select distinct u from User u left join ActiveInterest r on u.id = r.who where r.active = true and u.celeb.active = true and r.what in :cList")
    fun findRelatedCelebListByInterestedCategoryList(@Param("cList") categoryList: List<Long>): List<User>?

    @Query("select u from User u where u.celeb.active = true and u.id not in :uSet")
    fun findAllOtherCeleb(@Param("uSet") userSet: MutableSet<Long>): List<User>?

    @Query("select u from User u where u.celeb.active = true and u.name like %:q%")
    fun findCelebBySubstring(@Param("q") query: String): List<User>?

    fun findByIdAndActiveIsTrue(id: Long): User?
    fun findByIdAndActiveIsTrueAndCelebActiveIsTrue(id: Long): User?

    fun findByGoogleUserIdAndActiveIsTrue(googleUserId: String): User?
    fun findByKakaoUserIdAndActiveIsTrue(kakaoUserId: Long): User?
    fun findByNaverUserIdAndActiveIsTrue(naverUserId: String): User?
    fun findByAppleUserIdAndActiveIsTrue(appleUserId: String): User?
    fun findUserByNameAndActiveIsTrue(name: String): User?
    fun findByNameAndActiveIsTrue(name: String): User?

    fun findAllByActiveIsTrueAndCelebActiveIsTrue(): List<User>?

    fun existsByName(name: String): Boolean
}