package ai.maum.kotlin.jpa.common.message

import org.springframework.data.repository.CrudRepository

interface MessageFileRepository : CrudRepository<MessageFile, Long> {
}