package ai.maum.kotlin.jpa.common.setting

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Setting : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "SETTING_SEQ_GEN",
            sequenceName = "SETTING_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SETTING_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var sound: Boolean? = true
    @Column(nullable = false)
    var vibration: Boolean? = true

    @Column(nullable = false)
    var myLike: Boolean? = true
    @Column(nullable = false)
    var myScrap: Boolean? = true
    @Column(nullable = false)
    var myComment: Boolean? = true

    @Column(nullable = false)
    var celebFeed: Boolean? = true
    @Column(nullable = false)
    var celebContent: Boolean? = true
    @Column(nullable = false)
    var celebMessage: Boolean? = true

    @Column(nullable = false)
    var celebActivity: Boolean? = true

    @Column(nullable = false)
    var ttsResult: Boolean? = false

    @Column(nullable = false)
    var ttsRequest: Boolean? = true
}