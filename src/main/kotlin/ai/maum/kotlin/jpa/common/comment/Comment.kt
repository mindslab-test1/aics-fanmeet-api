package ai.maum.kotlin.jpa.common.comment

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.service.Tts
import javax.persistence.*

@AllOpen
@Entity
@javax.persistence.Table(name = "\"COMMENT\"")
class Comment : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "COMMENT_SEQ_GEN",
            sequenceName = "COMMENT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENT_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var section: Section? = null

    @Column(nullable = false)
    var feed: Long? = null

    var parent: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    var text: String? = null

    @Column(nullable = false)
    var likeCount: Long? = 0

    var picture: String? = null

    @Column(nullable = true)
    var modifyYn: Long? = null

    @ManyToOne
    var tts: Tts? = null

    enum class Section {
        USER_FEED,
        CELEB_FEED,

        CONTENT,
    }
}