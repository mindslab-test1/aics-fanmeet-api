package ai.maum.kotlin.jpa.common.service

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import java.time.Instant
import javax.persistence.*

@AllOpen
@Entity
class Tts : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "TTS_SEQ_GEN",
            sequenceName = "TTS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false, length = 300)
    var text: String? = null

    @Column(nullable = false)
    var url: String? = null

    @Column(nullable = false)
    var previewUrl: String? = null

    @Column(nullable = false)
    var celeb: Long? = null

    var subscriber: Long? = null

    var approvedDate: Instant? = null

    @Column(nullable = false)
    var status: Status? = null
    enum class Status {
        APPEALED,
        APPROVED,
        REJECTED,
        SELF,
        SAMPLE
    }
}