package ai.maum.kotlin.jpa.common.setting

import org.springframework.data.repository.CrudRepository

interface SettingRepository : CrudRepository<Setting, Long> {
    fun findByUserIdAndActiveIsTrue(userId: Long) : Setting?
}