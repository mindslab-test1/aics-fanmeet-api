package ai.maum.kotlin.jpa.common.interest

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class UserInterested : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "USER_INTERESTED_SEQ_GEN",
            sequenceName = "USER_INTERESTED_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_INTERESTED_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var what: Long? = null
}