package ai.maum.kotlin.jpa.common.setting

import org.springframework.data.repository.CrudRepository

interface CelebNotiSettingRepository : CrudRepository<CelebNotiSetting, Long> {

    fun findAllByWhoAndActiveIsTrue(who: Long) : CelebNotiSetting?
    fun findAllByWhomAndActiveIsTrue(whom: Long) : CelebNotiSetting?

    fun existsByWhoAndWhomAndActiveIsTrue(who: Long, whom: Long) : Boolean
}