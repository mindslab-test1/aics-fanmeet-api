package ai.maum.kotlin.jpa.common.interest

import org.springframework.data.repository.CrudRepository

interface ActiveInterestRepository : CrudRepository<ActiveInterest, Long> {
    fun findAllByWhoAndActiveIsTrue(who: Long): List<ActiveInterest>
    fun findByWhoAndWhat(who: Long, what: Long): ActiveInterest?
}