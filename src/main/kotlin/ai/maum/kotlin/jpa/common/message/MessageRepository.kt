package ai.maum.kotlin.jpa.common.message

import org.springframework.data.repository.CrudRepository

interface MessageRepository : CrudRepository<Message, Long> {
    fun findTop20ByWhoAndActiveIsTrueOrderByCreatedDesc(who: Long): List<Message>?
    //fun findTopByCelebAndAccessLevelLessThanEqualOrderByCreatedDesc(accessLevel: Long) : CelebMessage?
}