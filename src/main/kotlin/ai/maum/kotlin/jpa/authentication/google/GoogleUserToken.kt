package ai.maum.kotlin.jpa.authentication.google

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.authentication.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class GoogleUserToken () : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "GOOGLE_USER_TOKEN_SEQ_GEN",
            sequenceName = "GOOGLE_USER_TOKEN_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GOOGLE_USER_TOKEN_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var idTokenString: String? = null

    @Column(nullable = false)
    var googleUserId: String? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var deviceId: String? = null
}