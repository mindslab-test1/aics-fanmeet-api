package ai.maum.kotlin.jpa.authentication.google

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional

interface GoogleUserTokenRepository : CrudRepository<GoogleUserToken, Long> {
    fun findByIdTokenString(idTokenString: String): List<GoogleUserToken>?
    fun findByGoogleUserId(googleUserId: String): List<GoogleUserToken>?
    fun findByUserId(userId: Long): List<GoogleUserToken>?
    fun findAllByUserId(userId: Long): List<GoogleUserToken>?
    fun findAllByUserIdIn(userId: List<Long>): List<GoogleUserToken>?
    fun findByDeviceId(deviceId: String): List<GoogleUserToken>?
    @Query("select deviceId from GoogleUserToken where userId = :userId")
    fun findDeviceIdByUserId(@Param("userId") userId: Long) : List<String>

    @Transactional
    fun deleteByDeviceId(deviceId: String): Long?
}