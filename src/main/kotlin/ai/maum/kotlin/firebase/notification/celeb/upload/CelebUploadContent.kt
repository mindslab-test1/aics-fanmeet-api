package ai.maum.kotlin.firebase.notification.celeb.upload

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class CelebUploadContent(
        private val settingRepository: SettingRepository,
        blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 새 콘텐츠를 업로드했습니다.",
        "content",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    @Async
    fun send(celeb: Long, content: Long) {
        val tokenList = getFilteredSubscribersTokenList(celebId = celeb, excludeId = celeb)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("content", content.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebContent!!
    }
}