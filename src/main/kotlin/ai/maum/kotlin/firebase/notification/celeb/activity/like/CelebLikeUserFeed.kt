package ai.maum.kotlin.firebase.notification.celeb.activity.like

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class CelebLikeUserFeed(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 좋아하는 피드가 있습니다.",
        "like",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    val highlightMessageText = "셀럽이 내 피드를 좋아합니다."
    @Async
    fun send(celeb: Long, feedOwner: Long, feed: Long, communityOwner: Long) {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(feedOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb, feedOwner)
        if (!setting.myLike!! || isBlocked || celeb == feedOwner) return

        val tokenList = getFilteredSubscribersTokenList(celebId = communityOwner, excludeId = feedOwner)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("feed", feed.toString())
        )
        if(celeb == communityOwner){
            send(title = title, body = messageText, tokenList = tokenList, data = data)
        }
        sendHighlight(celeb = celeb, feedOwner = feedOwner, feed = feed)

    }
    @Async
    fun sendHighlight(celeb: Long, feedOwner: Long, feed:Long){
        val setting = settingRepository.findByUserIdAndActiveIsTrue(feedOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb, feedOwner)
        if(!setting.myLike!! || isBlocked) return

        val tokenList = getTokens(feedOwner)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("feed", feed.toString())
        )
        send(title = title, body = highlightMessageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebActivity!!
    }
}