package ai.maum.kotlin.firebase.notification.celeb.activity.like

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class CelebLikeComment(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        private val commentRepository: CommentRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 내 댓글을 좋아합니다.",
        "like",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    @Async
    fun send(celeb: Long, commentOwner: Long, comment: Long) {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(commentOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb, commentOwner)
        if (!setting.myLike!! || isBlocked || celeb == commentOwner) return

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        val where = when (commentEntity.section) {
            Comment.Section.USER_FEED -> "feed"
            Comment.Section.CELEB_FEED -> "celebFeed"
            Comment.Section.CONTENT -> "content"
            else -> return
        }

        val tokenList = getTokens(commentOwner)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("comment", comment.toString()),
                Pair(where, commentEntity.feed.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebActivity!!
    }
}