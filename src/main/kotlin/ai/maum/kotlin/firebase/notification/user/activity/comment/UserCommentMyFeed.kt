package ai.maum.kotlin.firebase.notification.user.activity.comment

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class UserCommentMyFeed(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        private val commentRepository: CommentRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "내 피드에 댓글을 남겼습니다.",
        "comment",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    fun send(commentOwner: Long, feedOwner: Long, comment: Long) {
        val isMuted = isCurrentNotiMuted(feedOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(feedOwner, commentOwner)
        if (isMuted || isBlocked || commentOwner == feedOwner) return

        val tokenList = getTokens(feedOwner)
        val title = getUserName(userId = commentOwner)

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        commentEntity.feed ?: return

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("comment", comment.toString()),
                Pair("feed", commentEntity.feed!!.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myComment!!
    }
}