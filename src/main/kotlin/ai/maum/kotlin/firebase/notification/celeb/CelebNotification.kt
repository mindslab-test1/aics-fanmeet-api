package ai.maum.kotlin.firebase.notification.celeb

import ai.maum.kotlin.firebase.notification.Notification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository

abstract class CelebNotification(
        val messageText: String,
        val type: String,
        private val blockRepository: BlockRepository,
        private val celebNotiSettingRepository: CelebNotiSettingRepository,
        private val subscribeRepository: SubscribeRepository,
        private val userRepository: UserRepository,
        private val membershipRepository: MembershipRepository,
        private val membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : Notification(
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    abstract fun isCurrentNotiMuted(userId: Long): Boolean

    fun getFilteredSubscribersTokenList(celebId: Long): List<String> {
        var userIdList = getSubscribingUserIdList(celebId = celebId)
        userIdList = removeBlockedUsers(celebId = celebId, userIds = userIdList)
        userIdList = removeCurrentNotiMutedUsers(userIds = userIdList)
        userIdList = removeCelebMutedUsers(celebId = celebId, userIds = userIdList)
        return getTokens(userIds = userIdList)
    }

    fun getFilteredSubscribersTokenList(celebId: Long, excludeId: Long): List<String> {
        var userIdList = getSubscribingUserIdList(celebId = celebId, excludeId = excludeId)
        userIdList = removeBlockedUsers(celebId = celebId, userIds = userIdList)
        userIdList = removeCurrentNotiMutedUsers(userIds = userIdList)
        userIdList = removeCelebMutedUsers(celebId = celebId, userIds = userIdList)
        return getTokens(userIds = userIdList)
    }

    fun getTtsFilteredSubscribersTokenList(celebId: Long, excludeId: Long): List<String> {
        var userIdList = getSubscribingUserIdList(celebId = celebId, excludeId = excludeId)
        userIdList = removeBlockedUsers(celebId = celebId, userIds = userIdList)
        userIdList = removeCurrentNotiMutedUsers(userIds = userIdList)
        userIdList = removeCelebMutedUsers(celebId = celebId, userIds = userIdList)
        userIdList = removeTtsNotAllowedUsers(celebId = celebId, userIds = userIdList)
        return getTokens(userIds = userIdList)
    }

    private fun removeCelebMutedUsers(celebId: Long, userIds: Iterable<Long>): Iterable<Long> {
        val userIdSet = userIds.toMutableSet()
        val removeTarget = mutableListOf<Long>()
        userIdSet.forEach {
            val exists = celebNotiSettingRepository.existsByWhoAndWhomAndActiveIsTrue(it, celebId)
            // After Celeb Noti Setting Feature Added
            //if (!exists) removeTarget.add(it)
        }
        userIdSet.removeAll(removeTarget)
        return userIdSet
    }

    private fun removeCurrentNotiMutedUsers(userIds: Iterable<Long>): Iterable<Long> {
        val userIdSet = userIds.toMutableSet()
        val removeTarget = mutableListOf<Long>()
        userIdSet.forEach {
            if (isCurrentNotiMuted(userId = it)) removeTarget.add(it)
        }
        userIdSet.removeAll(removeTarget)
        return userIdSet
    }

    private fun removeBlockedUsers(celebId: Long, userIds: Iterable<Long>): Iterable<Long> {
        val userIdSet = userIds.toMutableSet()
        val removeTarget = mutableListOf<Long>()
        userIdSet.forEach {
            val exists = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celebId, it)
            if (exists) removeTarget.add(it)
        }
        userIdSet.removeAll(removeTarget)
        return userIdSet
    }

    private fun removeTtsNotAllowedUsers(celebId: Long, userIds: Iterable<Long>): Iterable<Long> {
        val userIdSet = userIds.toMutableSet()
        val removeTarget = mutableListOf<Long>()
        userIdSet.forEach {
            val membershipHistory = membershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(it, celebId)
                    ?: return@forEach
            // Don't remove if membership not expired and voiceletter is allowed
            if (membershipHistory.status != UserMembershipHistory.Status.EXPIRED)
                if (membershipHistory.membership?.voiceLetter == true) return@forEach

            removeTarget.add(it)
        }
        userIdSet.removeAll(removeTarget)
        return userIdSet
    }

    private fun getSubscribingUserIdList(celebId: Long): Iterable<Long> {
        val subscribeList = subscribeRepository.findAllByWhomAndActiveIsTrue(celebId)
        val userIdList = mutableListOf<Long>()
        subscribeList.forEach {
            val user = userRepository.findUserByIdAndActiveIsTrue(it.who!!) ?: return@forEach
            if (user.active!!) userIdList.add(user.id!!)
        }
        return userIdList
    }

    private fun getSubscribingUserIdList(celebId: Long, excludeId: Long): Iterable<Long> {
        val subscribeList = subscribeRepository.findAllByWhomAndActiveIsTrue(celebId)
        val userIdList = mutableListOf<Long>()
        subscribeList.forEach {
            val user = userRepository.findUserByIdAndActiveIsTrue(it.who!!) ?: return@forEach
            if (user.active!! && user.id!! != excludeId) userIdList.add(user.id!!)
        }
        return userIdList
    }
}