package ai.maum.kotlin.firebase.notification

import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import com.google.firebase.messaging.*
import org.slf4j.LoggerFactory

abstract class Notification(
        private val userRepository: UserRepository,
        private val googleUserTokenRepository: GoogleUserTokenRepository,
        private val kakaoUserTokenRepository: KakaoUserTokenRepository,
        private val naverUserTokenRepository: NaverUserTokenRepository,
        private val appleUserTokenRepository: AppleUserTokenRepository
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun send(title: String, body: String, token: String, data: Map<String, String>) {
        val androidNotification = AndroidNotification.builder()
                .setTitle(title)
                .setBody(body)
        val apnsNotification = Notification(title, body)
        val apnsBuilder = ApnsConfig.builder().setAps(Aps.builder().build())

        val message = Message.builder()
                .setAndroidConfig(AndroidConfig.builder()
                        .setPriority(AndroidConfig.Priority.NORMAL)
                        .setNotification(androidNotification.build()).build())
                .setApnsConfig(apnsBuilder.build())
                .setNotification(apnsNotification)
                .setToken(token)
                .putAllData(data)
                .build()

        var response = FirebaseMessaging.getInstance().sendAsync(message)
    }

    fun <T> Iterable<T>.batch(chunkSize: Int) =
            withIndex().                        // create index value pairs
            groupBy { it.index / chunkSize }.   // create grouping index
            map { it.value.map { it.value } }   // split into different partitions


    fun send(title: String, body: String, tokenList: List<String>, data: Map<String, String>) {
        if (tokenList.isEmpty())
            return

        val androidNotification = AndroidNotification.builder()
                .setTitle(title)
                .setBody(body)
        val apnsNotification = Notification(title, body)
        val apnsBuilder = ApnsConfig.builder().setAps(Aps.builder().build())
        if (tokenList.size >= 500) {
            tokenList.batch(500).forEach {
                val multicastMessage = MulticastMessage.builder()
                        .setAndroidConfig(AndroidConfig.builder()
                                .setPriority(AndroidConfig.Priority.NORMAL)
                                .setNotification(androidNotification.build()).build())
                        .setApnsConfig(apnsBuilder.build())
                        .setNotification(apnsNotification)
                        .addAllTokens(it)
                        .putAllData(data)
                        .build()

                var response = FirebaseMessaging.getInstance().sendMulticastAsync(multicastMessage)
            }
        } else {
            val multicastMessage = MulticastMessage.builder()
                    .setAndroidConfig(AndroidConfig.builder()
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(androidNotification.build()).build())
                    .setApnsConfig(apnsBuilder.build())
                    .setNotification(apnsNotification)
                    .addAllTokens(tokenList)
                    .putAllData(data)
                    .build()

            var response = FirebaseMessaging.getInstance().sendMulticastAsync(multicastMessage)
        }
    }

    fun getTokens(userId: Long): List<String> {
        val tokenList = mutableListOf<String>()
        googleUserTokenRepository.findAllByUserId(userId)?.forEach {
            tokenList.add(it.deviceId!!)
        }
        kakaoUserTokenRepository.findAllByUserId(userId)?.forEach {
            tokenList.add(it.deviceId!!)
        }
        naverUserTokenRepository.findAllByUserId(userId)?.forEach {
            tokenList.add(it.deviceId!!)
        }
        appleUserTokenRepository.findAllByUserId(userId)?.forEach {
            tokenList.add(it.deviceId!!)
        }
        return tokenList
    }

    fun getTokens(userIds: Iterable<Long>): List<String> {
        val userIdList = userIds.toList()
        val tokenList = mutableListOf<String>()
//        googleUserTokenRepository.findAllByUserIdIn(userIdList)?.forEach {
//            tokenList.add(it.deviceId!!)
//        }
//        kakaoUserTokenRepository.findAllByUserIdIn(userIdList)?.forEach {
//            tokenList.add(it.deviceId!!)
//        }
//        naverUserTokenRepository.findAllByUserIdIn(userIdList)?.forEach {
//            tokenList.add(it.deviceId!!)
//        }
//        appleUserTokenRepository.findAllByUserIdIn(userIdList)?.forEach {
//            tokenList.add(it.deviceId!!)
//        }
        userIdList.forEach { (googleUserTokenRepository.findDeviceIdByUserId(it).forEach { tokenList.add(it) }) }
        userIdList.forEach { (kakaoUserTokenRepository.findDeviceIdByUserId(it).forEach { tokenList.add(it) }) }
        userIdList.forEach { (naverUserTokenRepository.findDeviceIdByUserId(it).forEach { tokenList.add(it) }) }
        userIdList.forEach { (appleUserTokenRepository.findDeviceIdByUserId(it).forEach { tokenList.add(it) }) }

        return tokenList
    }

    fun getUserName(userId: Long): String {
        return userRepository.findUserByIdAndActiveIsTrue(userId)?.name
                ?: throw Exception("no such active user $userId")
    }
}