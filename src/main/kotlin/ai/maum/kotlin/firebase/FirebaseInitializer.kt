package ai.maum.kotlin.firebase

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class FirebaseInitializer(private val firebaseServer : FirebaseServer) {
    private val logger = LoggerFactory.getLogger(FirebaseInitializer::class.java)

    @Throws(Exception::class)
    fun run(vararg args: String?) {
        try {
            firebaseServer.init()
        } catch (e: Exception) {
            logger.error(e.toString())
        }
    }
}