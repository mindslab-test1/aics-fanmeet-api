package ai.maum.kotlin.filter

import ai.maum.kotlin.filter.extension.FilterImpl
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.apache.http.HttpStatus
import org.springframework.beans.factory.ObjectFactory
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class SubAuthenticationFilterImpl(var userInfoFactory: ObjectFactory<UserInfo>,
                                  var googleUserTokenRepository: GoogleUserTokenRepository,
                                  var kakaoUserTokenRepository: KakaoUserTokenRepository,
                                  var naverUserTokenRepository: NaverUserTokenRepository,
                                  var appleUserTokenRepository: AppleUserTokenRepository) : FilterImpl {
    @Throws(IOException::class, ServletException::class)
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        try {
            if (userInfoFactory.`object`.userId != null) throw Exception()
            userInfoFactory.`object`.userId = when (request.getHeader("social")) {
                "google" -> checkFromGoogle(request.getHeader("token"))
                "kakao" -> checkFromKakao(request.getHeader("token"))
                "naver" -> checkFromNaver(request.getHeader("token"))
                "apple" -> checkFromApple(request.getHeader("token"))
                else -> throw Exception()
            }
        } catch (e: Exception) {
            // Do nothing
        }

        chain.doFilter(request, response)
    }

    private fun checkFromGoogle(token: String): Long {
        val res = googleUserTokenRepository.findByIdTokenString(token) ?: emptyList()
        if (res.isNotEmpty())
            return res[0].userId ?: throw Exception()
        throw Exception()
    }

    private fun checkFromKakao(token: String): Long {
        val res = kakaoUserTokenRepository.findByAccessToken(token) ?: emptyList()
        if (res.isNotEmpty())
            return res[0].userId ?: throw Exception()
        throw Exception()
    }

    private fun checkFromNaver(token: String): Long {
        val res = naverUserTokenRepository.findByAccessToken(token) ?: emptyList()
        if (res.isNotEmpty())
            return res[0].userId ?: throw Exception()
        throw Exception()
    }

    private fun checkFromApple(token: String): Long {
        var res = appleUserTokenRepository.findByAccessToken(token) ?: emptyList()
        if (res.isNotEmpty())
            return res[0].userId ?: throw Exception()
        throw Exception()
    }
}