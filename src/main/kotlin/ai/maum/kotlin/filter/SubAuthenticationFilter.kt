package ai.maum.kotlin.filter

import ai.maum.kotlin.filter.extension.FilterUrlWrapper
import ai.maum.kotlin.filter.extension.UrlCheckNOT
import ai.maum.kotlin.filter.extension.UrlStartsWith
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(1)
@Component
class SubAuthenticationFilter(userInfoFactory: ObjectFactory<UserInfo>,
                              googleUserTokenRepository: GoogleUserTokenRepository,
                              kakaoUserTokenRepository: KakaoUserTokenRepository,
                              naverUserTokenRepository: NaverUserTokenRepository,
                              appleUserTokenRepository: AppleUserTokenRepository)
    : FilterUrlWrapper(SubAuthenticationFilterImpl(userInfoFactory, googleUserTokenRepository, kakaoUserTokenRepository, naverUserTokenRepository, appleUserTokenRepository),
        UrlCheckNOT(UrlStartsWith("/test"))
)