package ai.maum.kotlin.filter.functional.logout

import ai.maum.kotlin.filter.extension.FilterImpl
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import org.springframework.transaction.annotation.Transactional
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class LogoutFilterImpl(var googleUserTokenRepository: GoogleUserTokenRepository,
                       var kakaoUserTokenRepository: KakaoUserTokenRepository,
                       var naverUserTokenRepository: NaverUserTokenRepository,
                       var appleUserTokenRepository: AppleUserTokenRepository) : FilterImpl {
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        try {
            when (request.getHeader("social")) {
                "google" -> logoutGoogle(request.getHeader("token"))
                "kakao" -> logoutKakao(request.getHeader("token"))
                "naver" -> logoutNaver(request.getHeader("token"))
                "apple" -> logoutApple(request.getHeader("token"))
                else -> throw Exception()
            }
        } catch (e: Exception) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            return
        }

        // In case of requested logout
        if (request.requestURI == "/auth/logout")
            response.status = HttpServletResponse.SC_OK
        // In case of anomaly from former filters
        else
            response.status = HttpServletResponse.SC_RESET_CONTENT

        /* Finished logout; don't go further */
        //chain.doFilter(request, response)
    }

    private fun logoutGoogle(token: String) {
        val userTokens = googleUserTokenRepository.findByIdTokenString(token) ?: emptyList()
        for (each in userTokens)
            googleUserTokenRepository.deleteByDeviceId(each.deviceId!!)
    }

    private fun logoutKakao(token: String) {
        val userTokens = kakaoUserTokenRepository.findByAccessToken(token) ?: emptyList()
        for (each in userTokens)
            kakaoUserTokenRepository.deleteByDeviceId(each.deviceId!!)
    }

    private fun logoutNaver(token: String) {
        val userTokens = naverUserTokenRepository.findByAccessToken(token) ?: emptyList()
        for (each in userTokens)
            naverUserTokenRepository.deleteByDeviceId(each.deviceId!!)
    }

    private fun logoutApple(token: String) {
        val userTokens = appleUserTokenRepository.findByAccessToken(token) ?: emptyList()
        for (each in userTokens)
            appleUserTokenRepository.deleteByDeviceId(each.deviceId!!)
    }
}