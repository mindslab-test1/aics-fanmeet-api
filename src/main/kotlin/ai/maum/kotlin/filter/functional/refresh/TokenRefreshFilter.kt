package ai.maum.kotlin.filter.functional.refresh

import ai.maum.kotlin.filter.extension.FilterUrlWrapper
import ai.maum.kotlin.filter.extension.UrlWhitelist
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

// Filter-level API
@Order(101)
@Component
class TokenRefreshFilter(userInfoFactory: ObjectFactory<UserInfo>,
                         googleUserTokenRepository: GoogleUserTokenRepository,
                         kakaoUserTokenRepository: KakaoUserTokenRepository,
                         naverUserTokenRepository: NaverUserTokenRepository,
                         appleUserTokenRepository: AppleUserTokenRepository,
                         restTemplate: RestTemplate)
    : FilterUrlWrapper(TokenRefreshFilterImpl(userInfoFactory, googleUserTokenRepository, kakaoUserTokenRepository, naverUserTokenRepository, appleUserTokenRepository, restTemplate),
        UrlWhitelist(
                arrayOf(
                        "/auth/refresh"
                )
        )
)