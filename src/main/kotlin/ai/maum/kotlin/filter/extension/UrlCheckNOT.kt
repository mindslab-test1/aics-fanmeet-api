package ai.maum.kotlin.filter.extension

class UrlCheckNOT(private val check: UrlCheck) : UrlCheck {
    override fun assert(url: String): Boolean = !check.assert(url)
}