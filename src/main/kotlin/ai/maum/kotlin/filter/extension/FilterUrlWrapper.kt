package ai.maum.kotlin.filter.extension

import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

abstract class FilterUrlWrapper (private val wrappedFilter: FilterImpl, private val urlCheck: UrlCheck) : OncePerRequestFilter() {
    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        if (urlCheck.assert(request.requestURI)) {
            wrappedFilter.run(request, response, chain)
        } else
            chain.doFilter(request, response)
    }
}