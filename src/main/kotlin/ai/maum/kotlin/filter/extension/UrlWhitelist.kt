package ai.maum.kotlin.filter.extension

class UrlWhitelist(private val whitelist: Array<String>) : UrlCheck {
    override fun assert(url: String): Boolean = whitelist.contains(url)
}