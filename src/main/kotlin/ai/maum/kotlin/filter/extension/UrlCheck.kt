package ai.maum.kotlin.filter.extension

interface UrlCheck {
    fun assert(url: String): Boolean
}