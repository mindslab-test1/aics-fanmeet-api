package ai.maum.kotlin.filter

import ai.maum.kotlin.filter.extension.*
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(0)
@Component
class AuthenticationFilter(userInfoFactory: ObjectFactory<UserInfo>,
                           googleUserTokenRepository: GoogleUserTokenRepository,
                           kakaoUserTokenRepository: KakaoUserTokenRepository,
                           naverUserTokenRepository: NaverUserTokenRepository,
                           appleUserTokenRepository: AppleUserTokenRepository)
    : FilterUrlWrapper(AuthenticationFilterImpl(userInfoFactory, googleUserTokenRepository, kakaoUserTokenRepository, naverUserTokenRepository, appleUserTokenRepository),
        UrlCheckAND(
                UrlCheckNOT(
                        UrlCheckOR(
                                UrlStartsWith("/test"),
                                UrlStartsWith("/actuator")
                        )
                ),
                UrlBlacklist(
                        /* Exceptions for authentication-less URIs */
                        arrayOf(
                                /* Logins */
                                "/auth/login/google",
                                "/auth/login/kakao",
                                "/auth/login/naver",
                                "/auth/login/apple",
                                /* Registrations */
                                "/auth/register/google",
                                "/auth/register/kakao",
                                "/auth/register/naver",
                                "/auth/register/apple",
                                /* Public APIs */
                                "/home/guest",
                                "/community/header",
                                "/community/feed/search",
                                "/community/feed/list",
                                "/community/feed/listcategory",
                                "/community/feed/view",
                                "/community/celebfeed/search",
                                "/community/celebfeed/list",
                                "/community/celebfeed/view",
                                "/community/celeb/listcategory",
                                "/community/content/listcategory",
                                "/community/content/view",
                                "/community/content/list",
                                "/community/service/apple/membership/verifyReceipt",
                                "/community/service/apple/membership/subscribeNoti",
                                "/community/service/apple/membership/completePurchase",
                                "/profile/view",
                                "/misc/name/duplication",
                                "/misc/version/check",
                                /* Self-Authenticated Calls */
                                "/admin/db/initsettings",
                                "/admin/db/checklocation",
                                "/admin/db/boundimage",
                                "/admin/db/notificationtest",
                                "/admin/db/subscribefanmeet",
                                "/admin/db/likeuniqueness",
                                /* Filter Testing */
                                "/something",
                                "/nothing",
                                "/terms"
                        )
                )
        )
)