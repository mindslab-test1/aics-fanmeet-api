package ai.maum.kotlin

import ai.maum.kotlin.firebase.FirebaseInitializer
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinApplication(
        private val firebaseInitializer: FirebaseInitializer
) : CommandLineRunner {
    private val logger = LoggerFactory.getLogger(KotlinApplication::javaClass.toString())

    override fun run(vararg args: String?) {
        firebaseInitializer.run()
    }
}

fun main(args: Array<String>) {
    runApplication<KotlinApplication>(*args)
}

// scp /home/wookje/Documents/git/rf-tap-fanseem/kotlin/target/kotlin-0.0.1-SNAPSHOT.jar minds@114.108.173.102:/home/minds/kotlin/kotlin-0.0.7.jar