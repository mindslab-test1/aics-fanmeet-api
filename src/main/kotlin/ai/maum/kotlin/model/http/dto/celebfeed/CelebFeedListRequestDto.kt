package ai.maum.kotlin.model.http.dto.celebfeed

import java.time.Instant
import javax.validation.constraints.NotNull

data class CelebFeedListRequestDto(
        @field:NotNull
        var celeb: Long? = null,
        @field:NotNull
        var get: Long? = null,

        var category: Long? = null,
        var from: Instant? = null
)