package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageTtsWriteViewResponse(
        @field:NotNull
        var monthlyRate: Long ?= null,
        @field:NotNull
        var monthlyUsed: Long ?= null,
        @field:NotNull
        var requestLimit: Long ?= null
)