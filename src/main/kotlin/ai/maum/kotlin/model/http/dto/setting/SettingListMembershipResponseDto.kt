package ai.maum.kotlin.model.http.dto.setting

import javax.validation.constraints.NotNull

data class SettingListMembershipResponseDto (
        @field:NotNull
        var activeList: List<SettingListMembershipItemDto>? = null
)