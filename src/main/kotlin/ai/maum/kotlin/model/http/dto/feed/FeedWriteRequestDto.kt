package ai.maum.kotlin.model.http.dto.feed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class FeedWriteRequestDto(
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var category: Long? = null,

        var pictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null
)