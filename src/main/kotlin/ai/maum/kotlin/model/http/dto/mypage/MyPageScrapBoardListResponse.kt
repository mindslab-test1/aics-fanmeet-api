package ai.maum.kotlin.model.http.dto.mypage

import ai.maum.kotlin.jpa.common.scrap.ScrapBoard
import java.time.Instant
import javax.validation.constraints.NotNull

data class MyPageScrapBoardListResponse(
        @field:NotNull
        var scrapBoardList: List<ScrapBoardData>? = null
)

data class ScrapBoardData(
        @field:NotNull
        var id: Long ?= null,
        @field:NotNull
        var updated: Instant ?= null,
        @field:NotNull
        var ownerId: Long ?= null,
        @field:NotNull
        var name: String ?= null,
        @field:NotNull
        var feedCount: Long ?= null
)