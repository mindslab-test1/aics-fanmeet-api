package ai.maum.kotlin.model.http.dto.service

import java.time.Instant
import javax.validation.constraints.NotNull

data class IosPurchaseDataDto (
        @field:NotNull
        var ishId: Int? = null,
        @field:NotNull
        var iosShoppingId: Int? = null,
        @field:NotNull
        var expDate: Instant? = null,
        @field:NotNull
        var goodsId: String? = null,
        @field:NotNull
        var renewState: String? = null,
        @field:NotNull
        var notiType: String? = null,
        @field:NotNull
        var webOrderId: String? = null
){
    fun IosPurchaseDataDto(iosShoppingId: Int, expDate: Instant?, goodId: String?, renewState: String?, notiType: String?, webOrderId: String?) {
        this.iosShoppingId = iosShoppingId
        this.expDate = expDate
        this.goodsId = goodId
        this.renewState = renewState
        this.notiType = notiType
        this.webOrderId = webOrderId
    }
}