package ai.maum.kotlin.model.http.dto.service

import org.springframework.http.HttpStatus

class AppleReceiptException : Exception {
    private var state: HttpStatus
    override var message: String? = null

    constructor(message: String?) : this(HttpStatus.INTERNAL_SERVER_ERROR, message) {}
    constructor(state: HttpStatus, message: String?) {
        this.state = state
        this.message = message
    }

    constructor(e: Exception) {
        state = HttpStatus.INTERNAL_SERVER_ERROR
        message = e.message
    }
}
