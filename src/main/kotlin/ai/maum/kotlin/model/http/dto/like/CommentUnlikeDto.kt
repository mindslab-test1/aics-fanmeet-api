package ai.maum.kotlin.model.http.dto.like

data class CommentUnlikeDto (
        val commentNo: Long,
        val userNo: Long
)