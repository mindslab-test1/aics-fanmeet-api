package ai.maum.kotlin.model.http.dto.block

data class BlockedUserListResponse (
        val userNo: Long,
        val name: String,
        val profileImageUrl: String
)