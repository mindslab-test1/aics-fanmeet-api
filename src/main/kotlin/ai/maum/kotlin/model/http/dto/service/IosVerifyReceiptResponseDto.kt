package ai.maum.kotlin.model.http.dto.service

data class IosVerifyReceiptResponseDto (
    var result: Int
)