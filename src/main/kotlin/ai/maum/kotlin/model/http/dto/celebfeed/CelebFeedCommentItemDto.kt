package ai.maum.kotlin.model.http.dto.celebfeed

import java.time.Instant
import javax.validation.constraints.NotNull

data class CelebFeedCommentItemDto(
        @field:NotNull
        var id: Long? = null,

        @field: NotNull
        var text: String? = null,

        @field:NotNull
        var commentOwner: Long? = null,

        @field:NotNull
        var commentOwnerName: String? = null,

        var commentOwnerProfileImage: String? = null,
        var badgeUrl: String? = null,

        @field:NotNull
        var likeCount: Long? = null,

        @field: NotNull
        var blocked: Boolean? = null,

        @field: NotNull
        var deleted: Boolean? = null,

        @field: NotNull
        var liked: Boolean? = null,

        var picture: String? = null,

        var tts: String? = null,

        @field: NotNull
        var updated: Instant? = null,

        var parent: Long? = null,

        @field: NotNull
        var offspring: MutableList<CelebFeedCommentItemDto>? = null,

        @field: NotNull
        var modifyYn: Long? = null
)