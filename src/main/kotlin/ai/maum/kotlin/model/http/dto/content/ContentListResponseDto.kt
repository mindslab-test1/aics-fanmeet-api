package ai.maum.kotlin.model.http.dto.content

import java.time.Instant
import javax.validation.constraints.NotNull

data class ContentListResponseDto (
        @field:NotNull
        var feeds: MutableList<ContentItemDto>? = null,

        var tailCreated: Instant? = null
)
