package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentViewResponseDto(
        @field:NotNull
        var feedItem: ContentItemDto? = null,

        @field:NotNull
        var comments: MutableList<ContentCommentItemDto>? = null
)