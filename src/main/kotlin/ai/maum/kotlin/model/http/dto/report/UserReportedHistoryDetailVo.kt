package ai.maum.kotlin.model.http.dto.report

data class UserReportedHistoryDetailVo (
        val objectId: Long,
        val type: String,
        val reportedDate: String,
        val text: String,
        val reportCount: Long
)