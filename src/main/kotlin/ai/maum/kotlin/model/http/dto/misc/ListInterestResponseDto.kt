package ai.maum.kotlin.model.http.dto.misc

import javax.validation.constraints.NotNull

data class ListInterestResponseDto (
        @field:NotNull
        var interests: List<InterestItem>? = null
)