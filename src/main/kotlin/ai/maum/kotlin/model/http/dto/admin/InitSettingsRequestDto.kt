package ai.maum.kotlin.model.http.dto.admin

import javax.validation.constraints.NotNull

data class InitSettingsRequestDto (
        @field:NotNull
        var secret: String? = null
)