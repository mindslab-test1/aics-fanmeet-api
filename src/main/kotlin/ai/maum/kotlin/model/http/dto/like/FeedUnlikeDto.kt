package ai.maum.kotlin.model.http.dto.like

data class FeedUnlikeDto (
        val feedNo: Long,
        val userNo: Long
)