package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedDeleteRequestDto(
        @field:NotNull
        var celebFeed: Long? = null
)