package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageReportListBlockedRequestDto(
        @field:NotNull
        var reverse: Boolean? = null
)