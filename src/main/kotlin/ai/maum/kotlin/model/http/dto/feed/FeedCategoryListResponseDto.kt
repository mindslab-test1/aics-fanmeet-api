package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedCategoryListResponseDto(
        @field:NotNull
        var categories: List<CategoryItemDto>? = null
)