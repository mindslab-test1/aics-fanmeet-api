package ai.maum.kotlin.model.http.dto.content

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class ContentEditRequestDto(
        @field:NotNull
        var content: Long? = null,

        var category: Long? = null,

        var title: String? = null,
//        flutter자체 multiPartFile의 body가 String으로 정해져있음
//        var removedPictures: List<Long>? = null,
        var removedPictures: String? = null,
        var addedPictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null,

        var accessLevel: Long? = null
)