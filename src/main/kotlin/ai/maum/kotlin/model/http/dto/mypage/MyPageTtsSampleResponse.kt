package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageTtsSampleResponse(
        @field:NotNull
        var url : String ?= null,
        @field:NotNull
        var id : Long ?= null
)