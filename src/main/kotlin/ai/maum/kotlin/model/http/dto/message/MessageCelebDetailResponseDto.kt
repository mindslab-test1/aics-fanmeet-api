package ai.maum.kotlin.model.http.dto.message

import javax.validation.constraints.NotNull

data class MessageCelebDetailResponseDto(
        @field:NotNull
        var messages: List<MessageCelebDetailItemDto>? = null
)