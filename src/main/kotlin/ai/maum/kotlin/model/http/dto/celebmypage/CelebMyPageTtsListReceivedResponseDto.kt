package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageTtsListReceivedResponseDto(
        @field:NotNull
        var receivedList: List<CelebMyPageTtsListReceivedItemDto>? = null
)