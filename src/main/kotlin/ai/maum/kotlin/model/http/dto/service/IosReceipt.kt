package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosReceipt (

        @field:NotNull
        var receiptType: String? = null,
        @field:NotNull
        var adamId: Int? = 0,
        @field:NotNull
        var appItemId: Int? = 0,
        @field:NotNull
        var bundleId: String? = null,
        @field:NotNull
        var downloadId: Long?,
        @field:NotNull
        var receiptCreationDateMs: String? = null,
        @field:NotNull
        var requestDateMs: String? = null,
        @field:NotNull
        var inApp: List<IosNotiReceiptInfo>? = null
)