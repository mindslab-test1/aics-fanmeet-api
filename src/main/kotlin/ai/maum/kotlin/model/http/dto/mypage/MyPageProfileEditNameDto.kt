package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageProfileEditNameDto(
     @field:NotNull
     var name: String ?= null
)