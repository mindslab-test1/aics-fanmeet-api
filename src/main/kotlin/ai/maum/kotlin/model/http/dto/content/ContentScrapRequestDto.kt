package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentScrapRequestDto(
        @field:NotNull
        var scrapBoard: Long? = null,

        @field:NotNull
        var content: Long? = null
)