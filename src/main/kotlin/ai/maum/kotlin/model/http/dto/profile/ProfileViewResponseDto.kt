package ai.maum.kotlin.model.http.dto.profile

import javax.validation.constraints.NotNull

data class ProfileViewResponseDto(
        @field:NotNull
        var isCeleb: Boolean? = null,

        @field:NotNull
        var isSubscribing: Boolean? = null,

        @field:NotNull
        var name: String? = null,

        var subscriberCount: Long? = null,
        var feedCount: Long? = null,
        var activeInterests: List<Long>? = null,
        var introduction: Map<String, String>? = null,
        var hello: String? = null,
        var sns: Map<String, String>? = null,

        var profileImageUrl: String? = null,
        var bannerImageUrl: String? = null
)