package ai.maum.kotlin.model.http.dto.feed.legacy

import ai.maum.kotlin.model.http.dto.comment.CommentResponseDto

data class FeedResponseDto (
        val feedNo: Long,
        val categoryNo: Long,

        val timestamp: String,
        val isBlured: Boolean,
        val isBlocked: Boolean,

        val text: String?,
        val fileUrlList: List<String>?,
        val youtubeUrl: String?,

        val likeCount: Long,
        val seemCount: Long,
        val commentCount: Long,

        val commentList: List<CommentResponseDto>?,

        val writerNo: Long,
        val writerName: String,
        val writerProfileImageUrl: String?,
        val writerMembershipNo: Long,

        val reportedCount: Long
)