package ai.maum.kotlin.model.http.dto.home

import org.springframework.web.multipart.MultipartFile

data class HomeRequestDto (
        val dummy: String?
)