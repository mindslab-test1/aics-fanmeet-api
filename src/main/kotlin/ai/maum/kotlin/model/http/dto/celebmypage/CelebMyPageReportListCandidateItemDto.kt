package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageReportListCandidateItemDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null,

        @field:NotNull
        var reportAccumulation: Long? = null,

        var profileImageUrl: String? = null
)