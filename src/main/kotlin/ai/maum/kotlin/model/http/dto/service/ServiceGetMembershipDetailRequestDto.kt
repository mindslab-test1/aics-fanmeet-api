package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class ServiceGetMembershipDetailRequestDto(
        @field:NotNull
        var productId: String? = null
)