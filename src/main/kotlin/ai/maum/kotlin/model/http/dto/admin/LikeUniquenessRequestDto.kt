package ai.maum.kotlin.model.http.dto.admin

import javax.validation.constraints.NotNull

data class LikeUniquenessRequestDto (
        @field:NotNull
        var secret:String? = null
)