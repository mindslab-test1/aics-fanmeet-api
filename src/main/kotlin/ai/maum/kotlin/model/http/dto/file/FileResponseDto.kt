package ai.maum.kotlin.model.http.dto.file

data class FileResponseDto (
        val fileNo: Long,
        val fileUrl: String,
        val fileExtension: String
)