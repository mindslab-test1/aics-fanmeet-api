package ai.maum.kotlin.model.http.dto.auth

import javax.validation.constraints.NotNull

data class KakaoLoginDto (
        @field:NotNull
        var deviceId: String?,
        @field:NotNull
        var accessToken: String?
)