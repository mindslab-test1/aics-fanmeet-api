package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageReportListBlockedItemDto(
        @field:NotNull
        var name: String? = null,

        @field:NotNull
        var user: Long? = null,

        var profileImageUrl: String? = null
)