package ai.maum.kotlin.model.http.dto.setting

import javax.validation.constraints.NotNull

data class SettingLoadFlagsResponseDto(
        @field:NotNull
        var sound: Boolean? = null,
        @field:NotNull
        var vibration: Boolean? = null,
        @field:NotNull
        var myLike: Boolean? = null,
        @field:NotNull
        var myScrap: Boolean? = null,
        @field:NotNull
        var myComment: Boolean? = null,
        @field:NotNull
        var celebFeed: Boolean? = null,
        @field:NotNull
        var celebContent: Boolean? = null,
        @field:NotNull
        var celebMessage: Boolean? = null,
        @field:NotNull
        var celebActivity: Boolean? = null

        /* 이것들 뭐임;;
        @field:NotNull
        var ttsResult: Boolean? = null,
        @field:NotNull
        var ttsRequest: Boolean? = null,
         */
)