package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class ServiceGetMembershipDetailResponseDto(
        @field:NotNull
        var tier: Long? = null,

        @field:NotNull
        var name: String? = null,

        var badgeImageUrl: String? = null,

        @field:NotNull
        var badge: Boolean? = null,
        @field:NotNull
        var sticker: Boolean? = null,
        @field:NotNull
        var voiceLetter: Boolean? = null,
        @field:NotNull
        var ttsWrite: Boolean? = null,
        @field:NotNull
        var ttsWriteLimit: Long? = null,

        var benefitDescriptionList: List<String>? = null
)