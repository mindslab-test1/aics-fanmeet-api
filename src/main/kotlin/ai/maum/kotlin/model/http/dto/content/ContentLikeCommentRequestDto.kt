package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentLikeCommentRequestDto(
        @field:NotNull
        var comment: Long? = null
)