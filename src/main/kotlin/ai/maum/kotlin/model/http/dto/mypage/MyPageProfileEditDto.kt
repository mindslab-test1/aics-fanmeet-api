package ai.maum.kotlin.model.http.dto.mypage

import org.springframework.web.multipart.MultipartFile

data class MyPageProfileEditDto(
        var profileImage: MultipartFile? = null,
        var bannerImage: MultipartFile? = null
)