package ai.maum.kotlin.model.http.dto.auth

import javax.validation.constraints.NotNull

data class LoginResponseDto (
        @field:NotNull
        var userId: Long?,
        @field:NotNull
        var isCeleb: Boolean?
)