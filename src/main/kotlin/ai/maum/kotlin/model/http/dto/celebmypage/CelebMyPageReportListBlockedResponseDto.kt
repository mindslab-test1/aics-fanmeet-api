package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageReportListBlockedResponseDto(
        @field:NotNull
        var blockedList: List<CelebMyPageReportListBlockedItemDto>? = null
)