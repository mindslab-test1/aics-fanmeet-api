package ai.maum.kotlin.model.http.dto.report

data class ReportCelebFeedRequestDto (
        val whom: Long,
        val feedId: Long,
        val judge: Long,
        val reason: String?
)