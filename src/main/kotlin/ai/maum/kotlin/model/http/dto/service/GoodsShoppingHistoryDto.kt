package ai.maum.kotlin.model.http.dto.service

import java.time.Instant
import java.util.*
import javax.validation.constraints.NotNull

data class GoodsShoppingHistoryDto (
        @field:NotNull
        var active: Int? = 1,
        var created: Instant? = null,
        var updated: Instant? = null,
        var expirationDate: Instant? = null,
        var purchaseToken: String? = null,
        @field:NotNull
        var status: Int? = null,
        @field: NotNull
        var userId: Int? = null,
        var membershipId: Int? = null,
        var webOrderId: String?,
        var platform: String? = null,
        var orgTranId: String? = null,
        var receiptFilePath: String? = null,
        var notificationtypeCode: String? = null
) {
}