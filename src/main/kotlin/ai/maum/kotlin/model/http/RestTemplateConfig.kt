package ai.maum.kotlin.model.http

import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.time.Duration

@Configuration
class RestTemplateConfig {
    @Bean
    fun restTemplate() : RestTemplate {
        val httpRequestFactory = HttpComponentsClientHttpRequestFactory()
        var httpClient = HttpClientBuilder.create().setMaxConnTotal(200).setMaxConnPerRoute(20).build()
        httpRequestFactory.httpClient = httpClient
        return RestTemplateBuilder()
                .setConnectTimeout(Duration.ofSeconds(5))
                .setReadTimeout(Duration.ofSeconds(5))
                .requestFactory{httpRequestFactory}
                .build()
    }
}