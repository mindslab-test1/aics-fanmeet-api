package ai.maum.kotlin.model.http.dto.community

import javax.validation.constraints.NotNull

data class CommunityHeaderRequestDto(
        @field:NotNull
        var celeb: Long? = null
)