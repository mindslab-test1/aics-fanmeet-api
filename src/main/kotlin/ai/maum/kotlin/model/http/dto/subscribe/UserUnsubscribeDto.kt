package ai.maum.kotlin.model.http.dto.subscribe

data class UserUnsubscribeDto (
        val who: Long,
        val whom: Long
)