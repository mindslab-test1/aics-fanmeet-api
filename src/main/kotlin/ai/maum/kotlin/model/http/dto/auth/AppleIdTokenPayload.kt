package ai.maum.kotlin.model.http.dto.auth

class AppleIdTokenPayload {
    var iss: String? = null
    var aud: String? = null
    var exp: Long? = null
    var iat: Long? = null
    var sub // users unique id
            : String? = null
    var at_hash: String? = null
    var auth_time: Long? = null
    var nonce_supported: Boolean? = null
    var email_verified: Boolean? = null
    var email: String? = null

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + if (at_hash == null) 0 else at_hash.hashCode()
        result = prime * result + if (aud == null) 0 else aud.hashCode()
        result = prime * result + if (auth_time == null) 0 else auth_time.hashCode()
        result = prime * result + if (exp == null) 0 else exp.hashCode()
        result = prime * result + if (iat == null) 0 else iat.hashCode()
        result = prime * result + if (iss == null) 0 else iss.hashCode()
        result = prime * result + if (sub == null) 0 else sub.hashCode()
        return result
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) return true
        if (obj == null) return false
        if (javaClass != obj.javaClass) return false
        val other = obj as AppleIdTokenPayload
        if (at_hash == null) {
            if (other.at_hash != null) return false
        } else if (at_hash != other.at_hash) return false
        if (aud == null) {
            if (other.aud != null) return false
        } else if (aud != other.aud) return false
        if (auth_time == null) {
            if (other.auth_time != null) return false
        } else if (auth_time != other.auth_time) return false
        if (exp == null) {
            if (other.exp != null) return false
        } else if (exp != other.exp) return false
        if (iat == null) {
            if (other.iat != null) return false
        } else if (iat != other.iat) return false
        if (iss == null) {
            if (other.iss != null) return false
        } else if (iss != other.iss) return false
        if (sub == null) {
            if (other.sub != null) return false
        } else if (sub != other.sub) return false
        return true
    }

    override fun toString(): String {
        return ("AppleIDTokenPayload [at_hash=" + at_hash + ", aud=" + aud + ", auth_time=" + auth_time + ", email="
                + email + ", email_verified=" + email_verified + ", exp=" + exp + ", iat=" + iat + ", iss=" + iss
                + ", nonce_supported=" + nonce_supported + ", sub=" + sub + "]")
    }

}