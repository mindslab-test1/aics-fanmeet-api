package ai.maum.kotlin.model.http.dto.like

data class CommentLikeDto (
        val commentNo: Long,
        val userNo: Long
)