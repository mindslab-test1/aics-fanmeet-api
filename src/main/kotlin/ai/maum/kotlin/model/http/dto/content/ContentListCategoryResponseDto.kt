package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentListCategoryResponseDto(
        @field:NotNull
        var categories: List<ContentCategoryItemDto>? = null
)