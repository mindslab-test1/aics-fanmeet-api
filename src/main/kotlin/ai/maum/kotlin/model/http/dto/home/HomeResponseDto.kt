package ai.maum.kotlin.model.http.dto.home

data class HomeResponseDto (
        val subscribingCelebList: MutableList<HomeSubscribingCelebDto>?,
        val recentActivityList: MutableList<HomeListElementDto>?
)