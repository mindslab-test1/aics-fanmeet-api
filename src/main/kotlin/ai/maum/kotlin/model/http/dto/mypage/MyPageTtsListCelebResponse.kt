package ai.maum.kotlin.model.http.dto.mypage

import ai.maum.kotlin.jpa.common.service.Tts
import java.time.Instant
import javax.validation.constraints.NotNull

data class MyPageTtsListCelebResponse (
//        @field:NotNull
//        var ttsListCeleb: List<Tts>? = null,
        @field:NotNull
        var ttsListCeleb: List<TtsData>? = null
)

data class TtsData(
        @field:NotNull
        var id: Long? = null,
        @field:NotNull
        var celeb: Long? = null,
        @field:NotNull
        var celebName: String? = null,
        @field:NotNull
        var timestamp: Instant? = null,
        @field:NotNull
        var text: String? = null,
        @field:NotNull
        var url: String? = null
)

//ttsId: data["id"],
//celebId: data["celeb"],
//celebName: data["celebName"],
//timestamp: data["created"],
//text: data["text"],
//url: data["url"]