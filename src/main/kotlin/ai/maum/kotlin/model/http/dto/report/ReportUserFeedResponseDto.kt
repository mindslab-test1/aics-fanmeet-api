package ai.maum.kotlin.model.http.dto.report

data class ReportUserFeedResponseDto (
        val message: String
)