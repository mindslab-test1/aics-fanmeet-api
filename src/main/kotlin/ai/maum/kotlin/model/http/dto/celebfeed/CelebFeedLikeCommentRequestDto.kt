package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedLikeCommentRequestDto(
        @field:NotNull
        var comment: Long? = null
)