package ai.maum.kotlin.model.http.dto.celebmypage

import java.time.Instant
import javax.validation.constraints.NotNull

data class CelebMyPageTtsListApprovedItemDto(
        @field:NotNull
        var subscriberName: String? = null,

        @field:NotNull
        var text: String? = null,

        @field:NotNull
        var created: Instant? = null,

        @field:NotNull
        var tts: Long? = null,

        @field:NotNull
        var ttsUrl: String? = null
)