package ai.maum.kotlin.model.http.dto.tts

data class CelebTtsRejectRequest (
        val ttsNo: Long
)