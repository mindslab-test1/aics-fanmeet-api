package ai.maum.kotlin.model.http.dto.tts

data class CelebTtsAcceptedListRequest (
        val celebNo: Long
)