package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageTtsWriteViewDto(
        @field:NotNull
        var celebId: Long ?= null
)