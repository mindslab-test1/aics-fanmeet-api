package ai.maum.kotlin.model.http.dto.comment

data class CommentDeleteDto (
        val commentNo: Long
)