package ai.maum.kotlin.model.http.dto.feed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class FeedEditRequestDto(
        @field:NotNull
        var feed: Long? = null,

        var category: Long? = null,
//        flutter자체 multiPartFile의 body가 String으로 정해져있음
//        var removedPictures: List<int>? = null,
        var removedPictures: String? = null,
        var addedPictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null
)