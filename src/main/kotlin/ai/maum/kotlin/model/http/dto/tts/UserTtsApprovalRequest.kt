package ai.maum.kotlin.model.http.dto.tts

data class UserTtsApprovalRequest (
        val who: Long,
        val whom: Long,
        val text: String
)