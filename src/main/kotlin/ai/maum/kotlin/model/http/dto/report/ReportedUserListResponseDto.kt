package ai.maum.kotlin.model.http.dto.report

data class ReportedUserListResponseDto (
        val userList: MutableList<ReportedUserListElementVo>?
)