package ai.maum.kotlin.model.http.dto.profile

import javax.validation.constraints.NotNull

data class ProfileSubscribeRequestDto(
        @field:NotNull
        var whom: Long? = null
)