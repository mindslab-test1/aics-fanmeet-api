package ai.maum.kotlin.model.http.dto.service

import java.util.*
import javax.validation.constraints.NotNull

data class IosPurchaseIdDto(
        @field:NotNull
        var iosShoppingId: Int? = null,
        @field:NotNull
        var userId: Int? = 0,
        @field:NotNull
        var orgTranId: String? = null,
        @field:NotNull
        var receiptFilePath: String? = null,
        @field:NotNull
        var crtDate: Date? = null,
        @field:NotNull
        var active: Int? = 0
) {

    fun IosPurchaseIdDto() {}

    fun IosPurchaseIdDto(orgTranId: String?) {
        this.orgTranId = orgTranId
        active = 0
    }
}