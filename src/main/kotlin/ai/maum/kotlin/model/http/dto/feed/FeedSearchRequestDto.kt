package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedSearchRequestDto(
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var keyword: String? = null
)