package ai.maum.kotlin.model.http.dto.auth

data class AppleTokenResponseDto (

        var id_token: String?,
        var access_token: String?,
        var token_type: String?,
        var expires_in: Long?,
        var refresh_token: String?
){
//
//
//    fun getIdToken(): String? {
//        return idToken
//    }
//
//    fun getAccessToken(): String? {
//        return accessToken
//    }
//
//    fun getTokenType(): String? {
//        return tokenType
//    }
//
//    fun getExpiresIn(): Long? {
//        return expiresIn
//    }
//
//    fun getRefreshToken(): String? {
//        return refreshToken
//    }
//
//    override fun toString(): String {
//        return "TokenResponse{" +
//                "idToken='" + idToken + '\'' +
//                ", accessToken='" + accessToken + '\'' +
//                ", tokenType='" + tokenType + '\'' +
//                ", expiresIn=" + expiresIn +
//                ", refreshToken='" + refreshToken + '\'' +
//                '}'
//    }
}