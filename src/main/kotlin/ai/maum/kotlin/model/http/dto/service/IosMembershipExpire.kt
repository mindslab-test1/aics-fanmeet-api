package ai.maum.kotlin.model.http.dto.service

import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import ai.maum.kotlin.model.authorization.general.Privilege
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class IosMembershipExpire(val userRepository: UserRepository,
                          val userMembershipHistoryRepository: UserMembershipHistoryRepository) {

    fun load(celeb: Long, userId: Long) {
        print(userRepository)
        var membership:  UserMembershipHistory? = null
        userId.let{ membership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, celeb)}
        print(membership)
        if (membership?.status == UserMembershipHistory.Status.EXPIRED)
            membership = null

        // Apple expiration check
        // Apple expiration need to be calculated here
        membership?.let {
            it.purchaseToken ?: let { // is apple purchase
                if (membership!!.expirationDate!! < Instant.now()) {
                    // expiration process
                    membership!!.status = UserMembershipHistory.Status.EXPIRED
                    userMembershipHistoryRepository.save(membership!!)

                    // also affect current authorization
                    membership = null
                }
            }
        }
    }
}