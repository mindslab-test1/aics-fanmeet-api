package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageHeaderResponseDto(
        @field:NotNull
        var name: String? = null,

        @field:NotNull
        var subscriberCount: Long? = null,

        @field:NotNull
        var feedCount: Long? = null,

        var profileImageUrl: String? = null
)