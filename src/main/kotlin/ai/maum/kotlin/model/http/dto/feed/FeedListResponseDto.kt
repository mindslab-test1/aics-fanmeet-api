package ai.maum.kotlin.model.http.dto.feed

import java.time.Instant
import javax.validation.constraints.NotNull

data class FeedListResponseDto(
        @field:NotNull
        var feeds: MutableList<FeedItemDto>? = null,

        var tailCreated: Instant? = null
)