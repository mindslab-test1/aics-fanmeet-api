package ai.maum.kotlin.model.http.dto.profile

import org.springframework.web.multipart.MultipartFile

data class ProfileEditRequestDto(
        var bannerImage: MultipartFile? = null,
        var profileImage: MultipartFile? = null,
        var name: String? = null,
        var introductionTitle0: String? = null,
        var introductionTitle1: String? = null,
        var introductionTitle2: String? = null,
        var introductionTitle3: String? = null,
        var introductionDescription0: String? = null,
        var introductionDescription1: String? = null,
        var introductionDescription2: String? = null,
        var introductionDescription3: String? = null
)