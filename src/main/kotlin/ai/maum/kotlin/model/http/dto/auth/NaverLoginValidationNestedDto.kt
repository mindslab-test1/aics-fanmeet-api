package ai.maum.kotlin.model.http.dto.auth

data class NaverLoginValidationNestedDto(
        var id: String?,
        var nickname: String?,
        var name: String?,
        var email: String?,
        var gender: String?,
        var age: String?,
        var birthday: String?,
        var profile_image: String?
)