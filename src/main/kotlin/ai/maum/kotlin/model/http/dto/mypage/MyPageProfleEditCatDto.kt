package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageProfleEditCatDto(
        @field:NotNull
        var interestedCategory: List<Long> ?= null
)