package ai.maum.kotlin.model.http.dto.visit

data class BoardVisitDto (
        val boardNo: Long,
        val userNo: Long
)