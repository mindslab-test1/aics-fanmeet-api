package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedViewResponseDto(
        @field:NotNull
        var feedItem: FeedItemDto? = null,

        @field:NotNull
        var comments: MutableList<CommentItemDto>? = null
)