package ai.maum.kotlin.model.http.dto.content

import java.time.Instant
import javax.validation.constraints.NotNull

data class ContentListRequestDto(
        @field:NotNull
        var celeb: Long? = null,
        @field:NotNull
        var get: Long? = null,

        var category: Long? = null,
        var from: Instant? = null
)
