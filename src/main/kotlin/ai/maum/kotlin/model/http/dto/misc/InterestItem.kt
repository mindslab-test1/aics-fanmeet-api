package ai.maum.kotlin.model.http.dto.misc

import javax.validation.constraints.NotNull

data class InterestItem(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null,

        @field:NotNull
        var icon: String? = null
)