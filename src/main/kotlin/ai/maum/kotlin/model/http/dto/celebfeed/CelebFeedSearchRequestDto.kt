package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedSearchRequestDto(
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var keyword: String? = null
)