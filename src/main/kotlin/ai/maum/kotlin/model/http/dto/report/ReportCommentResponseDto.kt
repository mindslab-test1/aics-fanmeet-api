package ai.maum.kotlin.model.http.dto.report

data class ReportCommentResponseDto (
        val message: String
)