package ai.maum.kotlin.model.http.dto.community

import javax.validation.constraints.NotNull

data class CommunityCelebMembershipListRequestDto(
        @field:NotNull
        var celeb: Long? = null
//        @field:NotNull
//        var userId: Long? = null
)