package ai.maum.kotlin.model.http.dto.report

import javax.validation.constraints.NotNull

data class ReportUserFeedRequestDto (
        @field:NotNull
        val whom: Long?,
        @field:NotNull
        val feedId: Long?,
        val reason: String?
)