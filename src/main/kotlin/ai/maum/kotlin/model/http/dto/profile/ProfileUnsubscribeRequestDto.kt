package ai.maum.kotlin.model.http.dto.profile

import javax.validation.constraints.NotNull

data class ProfileUnsubscribeRequestDto(
        @field:NotNull
        var whom: Long? = null
)
