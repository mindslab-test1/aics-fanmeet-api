package ai.maum.kotlin.model.http.dto.auth

import javax.validation.constraints.NotNull

data class GoogleLoginDto(
        @field:NotNull
        var deviceId: String?,
        @field:NotNull
        var idToken: String?
)