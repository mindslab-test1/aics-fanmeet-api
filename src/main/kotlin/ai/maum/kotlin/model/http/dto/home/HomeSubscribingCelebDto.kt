package ai.maum.kotlin.model.http.dto.home

data class HomeSubscribingCelebDto (
        val id: Long,
        val profileImageUrl: String?,
        val name: String,
        val badgeUrl: String? = null
)