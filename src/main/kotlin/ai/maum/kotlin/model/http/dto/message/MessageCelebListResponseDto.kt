package ai.maum.kotlin.model.http.dto.message

import javax.validation.constraints.NotNull

data class MessageCelebListResponseDto(
        @field:NotNull
        var list: List<MessageCelebListItemDto>? = null
)