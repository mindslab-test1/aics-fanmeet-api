package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedUnlikeCommentRequestDto(
        @field:NotNull
        var comment: Long? = null
)
