package ai.maum.kotlin.model.http.dto.message

data class MessageCelebWriteRequestDto(
        var text: String? = null,
        var tts: Long? = null
)