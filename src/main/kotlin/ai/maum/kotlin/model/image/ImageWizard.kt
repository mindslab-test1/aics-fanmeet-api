package ai.maum.kotlin.model.image

import com.sksamuel.scrimage.ImmutableImage
import com.sksamuel.scrimage.filter.GaussianBlurFilter
import com.sksamuel.scrimage.nio.GifWriter
import com.sksamuel.scrimage.nio.JpegWriter
import com.sksamuel.scrimage.nio.PngWriter
import org.slf4j.LoggerFactory
import org.springframework.core.io.ByteArrayResource
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

class ImageWizard(image: MultipartFile) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val original: ByteArrayResource
    private val bytes: ByteArray = image.bytes
    private lateinit var blurred: ByteArrayResource
    private lateinit var boundedForProfileImage: ByteArrayResource
    private lateinit var boundedForFeedImage: ByteArrayResource
    private lateinit var boundedForBannerImage: ByteArrayResource
    val extension: String = image.originalFilename!!.split(".").last()
    private val gif: Boolean

    init {
        gif = extension.toUpperCase() == "GIF"
        logger.info("ImageWizard \$name: ${image.name}")
        original = object : ByteArrayResource(bytes) {
            override fun getFilename(): String {
                return "originalImage.${extension.toLowerCase()}"
            }
        }
    }

    fun isGif(): Boolean = gif

    fun getOriginal(): ByteArrayResource {
        return original
    }

    fun getBlurred(): ByteArrayResource {
        if (!::blurred.isInitialized)
            blurred = object : ByteArrayResource(blur(bytes)) {
                override fun getFilename(): String {
                    return "blurredImage.${extension.toLowerCase()}"
                }
            }
        return if (gif) original else blurred
    }

    fun getBoundedForProfileImage(): ByteArrayResource {
        if (!::boundedForProfileImage.isInitialized)
            boundedForProfileImage = object : ByteArrayResource(boundProfileImage(bytes)) {
                override fun getFilename(): String {
                    return "boundedForProfileImage.${extension.toLowerCase()}"
                }
            }
        return if (gif) original else boundedForProfileImage
    }

    fun getBoundedForFeedImage(): ByteArrayResource {
        if (!::boundedForFeedImage.isInitialized)
            boundedForFeedImage = object : ByteArrayResource(boundFeedImage(bytes)) {
                override fun getFilename(): String {
                    return "boundedForFeedImage.${extension.toLowerCase()}"
                }
            }
        return if (gif) original else boundedForFeedImage
    }

    fun getBoundedForBannerImage(): ByteArrayResource {
        if (!::boundedForBannerImage.isInitialized)
            boundedForBannerImage = object : ByteArrayResource(boundBannerImage(bytes)) {
                override fun getFilename(): String {
                    return "boundedForBannerImage.${extension.toLowerCase()}"
                }
            }
        return if (gif) original else boundedForBannerImage
    }

    private fun blur(bytes: ByteArray): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        val resImage = image.bound(512, 512).scale(0.5).filter(GaussianBlurFilter(50)).scale(2.0)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, resImage.metadata, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    private fun boundProfileImage(bytes: ByteArray): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        // 256x256
        val resImage = image.bound(256, 256)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, resImage.metadata, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    private fun boundFeedImage(bytes: ByteArray): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        // 1440x822
        // Use bigger number to prevent over-downscaling for inverse-ratio images
        val resImage = image.bound(1440, 1440)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, resImage.metadata, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    private fun boundBannerImage(bytes: ByteArray): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        // 1600x396
        // Use bigger number to prevent over-downscaling for inverse-ratio images
        val resImage = image.bound(1600, 1600)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, resImage.metadata, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    // 댓글 사진 크게 보았을 때 다운스케일로 저장할 해상도는 1440*3120
    // 댓글에 달린 사진 표시는 가로 세로 520*278
    // 영상 썸네일 720 * 405
}