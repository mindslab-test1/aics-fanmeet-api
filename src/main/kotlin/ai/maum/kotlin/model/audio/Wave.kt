package ai.maum.kotlin.model.audio

import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.sound.sampled.AudioFileFormat
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import kotlin.experimental.and
import kotlin.experimental.or

class Wave(wavBytes: ByteArray) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    private var pcmBytes: ByteArray
    private var audioFormat: AudioFormat

    init {
        try {
            val byteArrayInputStream = ByteArrayInputStream(wavBytes)
            val audioInputStream = AudioSystem.getAudioInputStream(byteArrayInputStream)

            val pcmSize = audioInputStream.available()
            val pcmBytes = ByteArray(pcmSize)
            audioInputStream.read(pcmBytes, 0, pcmSize)

            val audioFormat = audioInputStream.format

            this.pcmBytes = pcmBytes
            this.audioFormat = audioFormat

            audioInputStream.close()
            byteArrayInputStream.close()
        } catch (e: Exception) {
            logger.error("Wave::init error")
            throw e
        }
    }

    fun setNoise(noise: Wave) {
        try {
            val base = this

            val baseBytes = base.getPcmBytes()
            val noiseBytes = noise.getPcmBytes()

            var noiseIdx = 0
            for (baseIdx in baseBytes.indices) {
                baseBytes[baseIdx] = ((baseBytes[baseIdx].toInt()/2+noiseBytes[noiseIdx].toInt()/2)).toByte()
                noiseIdx = (noiseIdx+1)%noiseBytes.size
            }

            audioFormat = base.getAudioFormat()
        } catch (e: Exception) {
            logger.error("Wave::setNoise Error")
            throw e
        }
    }

    fun setWatermark() {
        val pcmBytes = getPcmBytes()
        val offsets = byteArrayOf(3, 5, 6, 7)
        val mod = 8
        for (idx in pcmBytes.indices step mod) {
            pcmBytes[idx] = pcmBytes[idx] or offsets[idx/mod%4]
        }
    }

    fun isWatermarked(): Boolean {
        val pcmBytes = getPcmBytes()
        val offsets = byteArrayOf(3, 5, 6, 7)
        val mod = 8
        var flag = false
        for (base in 0 until mod) {
            var tmpFlag = true
            for (idx in base until pcmBytes.size step mod) {
                if (pcmBytes[idx] and offsets[idx/mod%4] != offsets[idx/mod%4]) {
                    tmpFlag = false
                    break
                }
            }
            if (tmpFlag) {
                flag = true
                break
            }
        }
        return flag
    }

    fun getPcmBytes(): ByteArray = pcmBytes
    fun getAudioFormat(): AudioFormat = audioFormat
    fun getWaveBytes(): ByteArray {
        try {
            val pcmBytes = getPcmBytes()
            val audioFormat = getAudioFormat()
            val byteArrayInputStream = ByteArrayInputStream(pcmBytes)
            val audioInputStream = AudioInputStream(byteArrayInputStream, audioFormat, (pcmBytes.size/audioFormat.frameSize).toLong())

            val outputStream = ByteArrayOutputStream()
            AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, outputStream)
            val result = outputStream.toByteArray()

            audioInputStream.close()
            byteArrayInputStream.close();
            outputStream.close()

            return result
        } catch (e: Exception) {
            logger.error("Wave::getWaveBytes Error")
            throw e
        }
    }

    fun getPreviewWaveBytes(startSecond: Long, secondsToCopy: Long): ByteArray {
        try {
            val pcmBytes = getPcmBytes()
            val audioFormat = getAudioFormat()
            val byteArrayInputStream = ByteArrayInputStream(pcmBytes)
            val audioInputStream = AudioInputStream(byteArrayInputStream, audioFormat, (pcmBytes.size/audioFormat.frameSize).toLong())

            val bytesPerSecond = audioFormat.frameSize * audioFormat.frameRate.toLong()
            audioInputStream.skip(startSecond * bytesPerSecond);
            val framesOfAudioToCopy = secondsToCopy * audioFormat.frameRate.toLong();
            val shortenedStream = AudioInputStream(audioInputStream, audioFormat, framesOfAudioToCopy);

            val outputStream = ByteArrayOutputStream()
            AudioSystem.write(shortenedStream, AudioFileFormat.Type.WAVE, outputStream)
            val result = outputStream.toByteArray()

            audioInputStream.close()
            shortenedStream.close()
            byteArrayInputStream.close();
            outputStream.close()

            return result
        } catch (e: Exception) {
            logger.error("Wave::getWaveBytes Error")
            throw e
        }
    }
}

/*
    // test codes

    val tts = Files.readAllBytes(File("/home/wookje/Desktop/tts.wav").toPath())
    val bgm = Files.readAllBytes(File("/home/wookje/Desktop/bgm4.wav").toPath())

    val ttsWave = Wave(tts)
    val bgmWave = Wave(bgm)

    ttsWave.setNoise(bgmWave)

    var waveBytes = ttsWave.getWaveBytes()
    Files.write(Path.of("/home/wookje/Desktop/no_watermark.wav"), waveBytes)

    ttsWave.setWatermark()
    println(ttsWave.isWatermarked())

    waveBytes = ttsWave.getWaveBytes()
    Files.write(Path.of("/home/wookje/Desktop/yes_watermark.wav"), waveBytes)
 */