package ai.maum.kotlin.model.media

import ai.maum.kotlin.model.http.dto.media.MediaFileUrlResponseDto
import ai.maum.kotlin.model.image.ImageWizard
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile
import java.time.Instant

@Component
class MediaUploader(
        private val restTemplate: RestTemplate
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val mediaServerLocation = MediaModel().mediaServerLocation

    fun uploadTtsAsync(byteArray: ByteArray): String? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val hashCode = byteArray.hashCode() % 1000
        val generatedFileName = timestamp + "_" + hashCode.toString() + ".wav"

        val file = object : ByteArrayResource(byteArray) {
            override fun getFilename(): String {
                return generatedFileName
            }
        }

        val mediaUrl: String
        // Reserve file url from media server
        try {
            mediaUrl = getUrl(generatedFileName)
            if (mediaUrl.isEmpty())
                return null
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        GlobalScope.launch {
            safeUpload(mediaUrl, file)
        }

        return mediaUrl
    }

    // For Pair<A, B>, A=original, B=blurred
    fun uploadAsyncFeedImageWithBlurred(files: List<MultipartFile>): List<Pair<String, String>>? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val split = """((.+)/)?(.+)\.(.+)""".toRegex()

        // Reserve file url from media server
        val fileNameList = mutableListOf<String>()
        val blurredFileNameList = mutableListOf<String>()
        try {
            for ((i, file) in files.withIndex()) {
                val matchResult = split.matchEntire(file.originalFilename!!) ?: return null
                val (_, directory, fileName, extension) = matchResult.destructured
                if (fileName.isEmpty() || extension.isEmpty())
                    return null
                val hashCode = fileName.hashCode() % 1000

                val generatedName = timestamp + "_" + i + "_" + hashCode.toString() + "." + extension
                val blurredName = timestamp + "_" + i + "_" + hashCode.toString() + "BLUR" + "." + extension

                val url = getUrl(generatedName)
                val blurredUrl = getUrl(blurredName)
                if (url.isEmpty())
                    return null
                if (blurredUrl.isEmpty())
                    return null

                fileNameList.add(url)
                blurredFileNameList.add(blurredUrl)
            }
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(fileNameList[i], ImageWizard(file).getBoundedForFeedImage())
            }
        }
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(blurredFileNameList[i], ImageWizard(file).getBlurred())
            }
        }

        return fileNameList.zip(blurredFileNameList)
    }

    fun uploadAsyncProfileImage(file: MultipartFile): String? = uploadAsyncProfileImage(listOf(file))?.get(0)
    fun uploadAsyncProfileImage(files: List<MultipartFile>): List<String>? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val split = """((.+)/)?(.+)\.(.+)""".toRegex()

        // Reserve file url from media server
        val fileNameList = mutableListOf<String>()
        try {
            for ((i, file) in files.withIndex()) {
                val matchResult = split.matchEntire(file.originalFilename!!) ?: return null
                val (_, directory, fileName, extension) = matchResult.destructured
                if (fileName.isEmpty() || extension.isEmpty())
                    return null
                val hashCode = fileName.hashCode() % 1000

                val generatedName = timestamp + "_" + i + "_" + hashCode.toString() + "." + extension

                val url = getUrl(generatedName)
                if (url.isEmpty())
                    return null

                fileNameList.add(url)
            }
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(fileNameList[i], ImageWizard(file).getBoundedForProfileImage())
            }
        }

        return fileNameList
    }

    fun uploadAsyncFeedImage(file: MultipartFile): String? = uploadAsyncFeedImage(listOf(file))?.get(0)
    fun uploadAsyncFeedImage(files: List<MultipartFile>): List<String>? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val split = """((.+)/)?(.+)\.(.+)""".toRegex()

        // Reserve file url from media server
        val fileNameList = mutableListOf<String>()
        try {
            for ((i, file) in files.withIndex()) {
                val matchResult = split.matchEntire(file.originalFilename!!) ?: return null
                val (_, directory, fileName, extension) = matchResult.destructured
                if (fileName.isEmpty() || extension.isEmpty())
                    return null
                val hashCode = fileName.hashCode() % 1000

                val generatedName = timestamp + "_" + i + "_" + hashCode.toString() + "." + extension

                val url = getUrl(generatedName)
                if (url.isEmpty())
                    return null

                fileNameList.add(url)
            }
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(fileNameList[i], ImageWizard(file).getBoundedForFeedImage())
            }
        }

        return fileNameList
    }

    fun uploadAsyncBannerImage(file: MultipartFile): String? = uploadAsyncBannerImage(listOf(file))?.get(0)
    fun uploadAsyncBannerImage(files: List<MultipartFile>): List<String>? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val split = """((.+)/)?(.+)\.(.+)""".toRegex()

        // Reserve file url from media server
        val fileNameList = mutableListOf<String>()
        try {
            for ((i, file) in files.withIndex()) {
                val matchResult = split.matchEntire(file.originalFilename!!) ?: return null
                val (_, directory, fileName, extension) = matchResult.destructured
                if (fileName.isEmpty() || extension.isEmpty())
                    return null
                val hashCode = fileName.hashCode() % 1000

                val generatedName = timestamp + "_" + i + "_" + hashCode.toString() + "." + extension

                val url = getUrl(generatedName)
                if (url.isEmpty())
                    return null

                fileNameList.add(url)
            }
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(fileNameList[i], ImageWizard(file).getBoundedForBannerImage())
            }
        }

        return fileNameList
    }

    fun uploadAsync(file: MultipartFile): String? = uploadAsync(listOf(file))?.get(0)
    fun uploadAsync(files: List<MultipartFile>): List<String>? {
        val timestamp = Instant.now().toEpochMilli().toString()
        val split = """((.+)/)?(.+)\.(.+)""".toRegex()

        // Reserve file url from media server
        val fileNameList = mutableListOf<String>()
        try {
            for ((i, file) in files.withIndex()) {
                val matchResult = split.matchEntire(file.originalFilename!!) ?: return null
                val (_, directory, fileName, extension) = matchResult.destructured
                if (fileName.isEmpty() || extension.isEmpty())
                    return null
                val hashCode = fileName.hashCode() % 1000

                val generatedName = timestamp + "_" + i + "_" + hashCode.toString() + "." + extension

                val url = getUrl(generatedName)
                if (url.isEmpty())
                    return null

                fileNameList.add(url)
            }
        } catch (e: Exception) {
            return null
        }

        // Start uploading
        for ((i, file) in files.withIndex()) {
            GlobalScope.launch {
                safeUpload(fileNameList[i], file)
            }
        }

        return fileNameList
    }

    suspend fun safeUpload(fileName: String, file: MultipartFile) {
        withTimeout(59999) {
            while (!uploadToMediaServer(file, fileName))
                delay(100)
        }
    }

    suspend fun safeUpload(fileName: String, file: Resource) {
        withTimeout(59999) {
            while (!uploadToMediaServer(file, fileName))
                delay(100)
        }
    }

    private fun getUrl(filename: String): String {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val body = emptyMap<String, Any>().toMutableMap()
        body["filename"] = filename
        body["uploadDirectory"] = "/aics/fanmeet"
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file/path:get", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<MediaFileUrlResponseDto?>() {})
        logger.info(response.body.toString())
        return response.body!!.pathUrl!!
    }

    private fun uploadToMediaServer(file: MultipartFile, url: String): Boolean {
        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA
        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file.resource)
        body.add("uploadDirectory", url.substring(6))
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file:upload", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<String?>() {})
        logger.info(response.body.toString())
        return response.statusCode == HttpStatus.CREATED
    }

    private fun uploadToMediaServer(file: Resource, url: String): Boolean {
        val headers = HttpHeaders()
        headers.contentType = MediaType.MULTIPART_FORM_DATA
        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", file)
        body.add("uploadDirectory", url.substring(6))
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file:upload", HttpMethod.POST, requestEntity, object : ParameterizedTypeReference<String?>() {})
        logger.info(response.body.toString())
        return response.statusCode == HttpStatus.CREATED
    }
/*
    private fun deleteFile(fileFullPath: String) {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("deleteFileFullPath", fileFullPath)
        val requestEntity = HttpEntity(body, headers)
        val response = restTemplate.exchange("$mediaServerLocation/media/file:delete", HttpMethod.DELETE, requestEntity, object : ParameterizedTypeReference<String?>() {})
        logger.info(response.body.toString())
    }
*/
}
