package ai.maum.kotlin.model.authorization.fanmeet.privilege

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import ai.maum.kotlin.model.authorization.general.Privilege
import org.springframework.stereotype.Component
import java.time.Instant

@Requires(ContextProperty.COMMUNITY_OWNER, ContextProperty.CONTENT)
@Requires(ContextProperty.COMMUNITY_OWNER, ContextProperty.CELEBFEED)
//@Requires(ContextProperty.COMMUNITY_OWNER, ContextProperty.COMMENT)
@Component
class MembershipTierPrivilege(
        val userRepository: UserRepository,
        val userMembershipHistoryRepository: UserMembershipHistoryRepository,
        val contentRepository: ContentRepository,
        val celebFeedRepository: CelebFeedRepository,
        val commentRepository: CommentRepository
) : Privilege<ResponseType> {
    override var permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        context.communityOwner ?: return
        context.content ?: context.celebFeed ?: context.comment ?: return // At least one of them is required nonnull

        var membership:  UserMembershipHistory? = null
        context.userId?.let{ membership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(context.userId!!, context.communityOwner!!)}

        // Google expiration check
        // Google-payed membership expirations are checked by pub/sub listener
        if (membership?.status == UserMembershipHistory.Status.EXPIRED)
            membership = null

        // Apple expiration check
        // Apple expiration need to be calculated here
        membership?.let {
            it.purchaseToken ?: let { // is apple purchase
                if (membership!!.expirationDate!! < Instant.now()) {
                    // expiration process
                    membership!!.status = UserMembershipHistory.Status.EXPIRED
                    userMembershipHistoryRepository.save(membership!!)

                    // also affect current authorization
                    membership = null
                }
            }
        }

        context.content?.let {
            val content = contentRepository.findByIdAndActiveIsTrue(context.content!!) ?: return
            if (content.who != context.communityOwner)
                return

            val userLevel = membership?.membership?.tier ?: 0
            val accessLevel = content.accessLevel ?: 0

            if (userLevel < accessLevel)
                return

            context.subject.grant(this)
            return
        }

        context.celebFeed?.let {
            val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(context.celebFeed!!) ?: return
            if (celebFeed.who != context.communityOwner)
                return

            val userLevel = membership?.membership?.tier ?: 0
            val accessLevel = celebFeed.accessLevel ?: 0

            if (userLevel < accessLevel)
                return

            context.subject.grant(this)
        }
/*
        context.comment?.let {
            val comment = commentRepository.findByIdAndActiveIsTrue(context.comment!!) ?: return
            if (comment.celeb != context.communityOwner)
                return

            val userLevel = membership?.membership?.tier ?: 0
            var accessLevel = 0
            if (comment.section == Comment.Section.CONTENT) {
                val article = contentRepository.findByIdAndActiveIsTrue(comment.feed!!) ?: return
                accessLevel = article.accessLevel?.toInt() ?: 0
            }
            if (comment.section == Comment.Section.CELEB_FEED) {
                val article = celebFeedRepository.findByIdAndActiveIsTrue(comment.feed!!) ?: return
                accessLevel = article.accessLevel?.toInt() ?: 0
            }

            if (userLevel < accessLevel)
                return

            context.subject.grant(this)
        }*/
    }
}