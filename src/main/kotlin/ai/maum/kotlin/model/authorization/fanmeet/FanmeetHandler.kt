package ai.maum.kotlin.model.authorization.fanmeet

import ai.maum.kotlin.model.authorization.general.HandlerMethod

interface FanmeetHandler<T> : HandlerMethod<T> {
    fun setController(controller: Any)
    fun setDto(dto: Any)
}