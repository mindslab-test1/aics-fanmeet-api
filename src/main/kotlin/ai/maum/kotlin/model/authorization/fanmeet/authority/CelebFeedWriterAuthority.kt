package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.springframework.stereotype.Component

@Requires(ContextProperty.CELEBFEED)
@Component
class CelebFeedWriterAuthority(
        private val celebFeedRepository: CelebFeedRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        val feed = celebFeedRepository.findByIdAndActiveIsTrue(context.celebFeed ?: return) ?: return

        feed.who ?: return
        context.userId ?: return

        if (feed.who == context.userId)
            context.subject.grant(this)
    }
}