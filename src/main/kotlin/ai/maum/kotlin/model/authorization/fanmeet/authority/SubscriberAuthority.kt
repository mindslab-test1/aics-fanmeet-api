package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.springframework.stereotype.Component

@Requires(ContextProperty.COMMUNITY_OWNER)
@Component
class SubscriberAuthority(
        private val subscribeRepository: SubscribeRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        context.userId ?: return
        context.communityOwner ?: return

        var subscribe = subscribeRepository.findByWhoAndWhomAndActiveIsTrue(context.userId!!, context.communityOwner!!) ?: return
        context.subject.grant(this)
    }
}