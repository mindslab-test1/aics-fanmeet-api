package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.springframework.stereotype.Component

@Requires(ContextProperty.COMMENT)
@Component
class CommentWriterAuthority(
        private val commentRepository: CommentRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        val comment = commentRepository.findByIdAndActiveIsTrue(context.comment ?: return) ?: return

        comment.who ?: return
        context.userId ?: return

        if (comment.who == context.userId)
            context.subject.grant(this)
    }
}