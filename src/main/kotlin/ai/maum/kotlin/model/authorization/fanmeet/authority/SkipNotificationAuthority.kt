package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component

@Requires()
@Component
class SkipNotificationAuthority(
        private val userRepository: UserRepository,
        private val userInfoFactory: ObjectFactory<UserInfo>
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        val user = userRepository.findUserByIdAndActiveIsTrue(context.userId ?: return)

        if (userInfoFactory.`object`.notification == false)
            context.subject.grant(this)
    }
}