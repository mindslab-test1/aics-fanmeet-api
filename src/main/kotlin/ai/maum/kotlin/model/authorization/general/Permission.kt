package ai.maum.kotlin.model.authorization.general

interface Permission<T> {
    fun evaluate(context: AuthorizationContext<T>): Boolean = true
}