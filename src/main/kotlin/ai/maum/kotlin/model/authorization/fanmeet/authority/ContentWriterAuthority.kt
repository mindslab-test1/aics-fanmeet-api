package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.springframework.stereotype.Component

@Requires(ContextProperty.CONTENT)
@Component
class ContentWriterAuthority(
        private val contentRepository: ContentRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        val content = contentRepository.findByIdAndActiveIsTrue(context.content ?: return) ?: return

        content.who ?: return
        context.userId ?: return

        if (content.who == context.userId)
            context.subject.grant(this)
    }
}