package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.report.ReportComment
import ai.maum.kotlin.controller.impl.report.ReportFeed
import ai.maum.kotlin.controller.impl.report.ReportedUserDetail
import ai.maum.kotlin.controller.impl.report.ReportedUserList
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.report.ReportCommentRequestDto
import ai.maum.kotlin.model.http.dto.report.ReportUserFeedRequestDto
import ai.maum.kotlin.model.http.dto.report.ReportedUserDetailRequestDto
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class ReportController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    @PostMapping("/report/feed")
    fun reportUserFeed(@RequestBody @Valid dto: ReportUserFeedRequestDto) = begin(this, dto, ReportFeed())
            .handle().end()

    @PostMapping("/report/comment")
    fun reportComment(@RequestBody @Valid dto: ReportCommentRequestDto) = begin(this, dto, ReportComment())
            .handle().end()

    @PostMapping("/report/list")
    fun reportedUserList() = begin(this, ReportedUserList())
            .handle().end()

    @PostMapping("/report/list/detail")
    fun reportedUserDetail(@RequestBody @Valid dto: ReportedUserDetailRequestDto) = begin(this, dto, ReportedUserDetail())
            .handle().end()
}