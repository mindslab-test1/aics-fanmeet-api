package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.community.CommunityCelebMembershipList
import ai.maum.kotlin.controller.impl.community.CommunityHeader
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.community.CommunityCelebMembershipListRequestDto
import ai.maum.kotlin.model.http.dto.community.CommunityHeaderRequestDto
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class CommunityController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    @PostMapping("/community/header")
    fun communityHeader(@RequestBody @Valid dto: CommunityHeaderRequestDto) = begin(this, dto, CommunityHeader())
            .handle().end()

    @PostMapping("/community/celeb/membership/list")
    fun communityCelebMembershipList(@RequestBody @Valid dto: CommunityCelebMembershipListRequestDto) = begin(this, dto, CommunityCelebMembershipList()).also { fanmeet.iosMembershipExpire.load(dto.celeb!!,auth.userId()!!) }
            .handle().end()
}