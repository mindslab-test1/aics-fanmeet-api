package ai.maum.kotlin.controller

import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.interest.*
import ai.maum.kotlin.jpa.common.celeb.Celeb
import ai.maum.kotlin.jpa.common.celeb.CelebRepository
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedFileRepository
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.content.Content
import ai.maum.kotlin.jpa.common.content.ContentFile
import ai.maum.kotlin.jpa.common.content.ContentFileRepository
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.feed.*
import ai.maum.kotlin.jpa.common.like.LikeRepository
import ai.maum.kotlin.jpa.common.report.Report
import ai.maum.kotlin.jpa.common.report.ReportRepository
import ai.maum.kotlin.jpa.common.scrap.ScrapBoardRepository
import ai.maum.kotlin.jpa.common.scrap.ScrapRepository
import ai.maum.kotlin.jpa.common.subscribe.Subscribe
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.model.http.rdo.UserInfo
import com.google.firebase.messaging.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@RestController
class SampleDataController(
        val restTemplate: RestTemplate,
        val userInfoFactory: ObjectFactory<UserInfo>,
        val userRepository: UserRepository,
        val subscribeRepository: SubscribeRepository,
        val scrapRepository: ScrapRepository,
        val scrapBoardRepository: ScrapBoardRepository,
        val reportRepository: ReportRepository,
        val likeRepository: LikeRepository,
        val celebFeedRepository: CelebFeedRepository,
        val feedRepository: FeedRepository,
        val contentRepository: ContentRepository,
        val commentRepository: CommentRepository,
        val celebRepository: CelebRepository,
        val interestRepository: InterestRepository,
        val userInterestedRepository: UserInterestedRepository,
        val activeInterestRepository: ActiveInterestRepository,
        val blockRepository: BlockRepository,
        val celebFeedFileRepository: CelebFeedFileRepository,
        val feedFileRepository: FeedFileRepository,
        val contentFileRepository: ContentFileRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var userId1 = 0L
    var userId2 = 0L
    var userId3 = 0L
    var celebId1 = 0L
    var celebId2 = 0L
    var celebId3 = 0L
    var celebId4 = 0L
/*
    @PostMapping("/test/upload/file")
    fun go(@Valid requestUploadFilesDirectoryDto: RequestUploadFilesDirectoryDto) {
        val mediaServerEndPoint = MediaUploader(restTemplate)
        mediaServerEndPoint.uploadFiles(requestUploadFilesDirectoryDto.files!!)
    }
*/
//    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = {   "multipart/form-data" })
//    @ResponseBody
//    public boolean uploadImage(@RequestPart("obj") YourDTO dto, @RequestPart("file") MultipartFile file) {
//        // your logic
//        return true;
//    }`

    @GetMapping("/test/insert/all")
    fun all() {
        user()
        subscribe()
        category()
        interestedCategory()
        relatedCategory()
        userFeed()
        userFeedComment()
        celebFeed()
        celebFeedComment()
        content()
        report()
    }

    @GetMapping("/test/insert/user")
    fun user() {
        var user = User()
        user.name = "권욱제"
        user.gender = "M"
        user.age = 22
        userRepository.save(user)
        userId1 = user.id!!

        user = User()
        user.name = "피카츄"
        user.profileImageUrl = "https://bit.ly/2MimsTt"
        user.birthday = "03.03"
        userRepository.save(user)
        userId2 = user.id!!

        user = User()
        user.name = "아이유"
        user.profileImageUrl = "https://bit.ly/2XUGaud"
        user.gender = "F"
        user.age = 40
        userRepository.save(user)
        var celeb = Celeb()
        celeb.hello = "나 도 아 이 유"
        celeb.user = user
        user.celeb = celeb
        celebRepository.save(celeb)
        userRepository.save(user)
        celebId1 = user.id!!

        user = User()
        user.name = "한소희"
        user.profileImageUrl = "https://bit.ly/3drIvTN"
        user.gender = "F"
        user.birthday = "04.04"
        userRepository.save(user)
        celeb = Celeb()
        celeb.hello = "hi ~ hi ~ nice to meet you ~"
        celeb.user = user
        user.celeb = celeb
        celebRepository.save(celeb)
        userRepository.save(user)
        celebId2 = user.id!!

        user = User()
        user.name = "궁예"
        user.profileImageUrl = "https://bit.ly/3exRbrY"
        user.age = 99999999
        userRepository.save(user)
        celeb = Celeb()
        celeb.hello = "누가 퇴근 소리를 내었는가?"
        celeb.user = user
        celeb.active = false
        user.celeb = celeb
        celebRepository.save(celeb)
        userRepository.save(user)
        celebId3 = user.id!!

        user = User()
        user.name = "wookje.TV"
        user.profileImageUrl = "https://bit.ly/3eH0TbG"
        user.age = 222
        userRepository.save(user)
        celeb = Celeb()
        celeb.hello = "욱제 티비입니다"
        celeb.user = user
        celeb.active = false
        user.celeb = celeb
        celebRepository.save(celeb)
        userRepository.save(user)
        celebId4 = user.id!!

        user = User()
        user.name = "장용운"
        user.profileImageUrl = "https://bit.ly/304CaKl"
        user.age = 414141
        userRepository.save(user)
        userId3 = user.id!!
    }

    @GetMapping("/test/insert/subscribe")
    fun subscribe() {
        var sub = Subscribe()
        sub.who = userId1
        sub.whom = celebId1
        subscribeRepository.save(sub)

        sub = Subscribe()
        sub.who = userId1
        sub.whom = celebId2
        sub.active = false
        subscribeRepository.save(sub)

        sub = Subscribe()
        sub.who = userId2
        sub.whom = celebId3
        subscribeRepository.save(sub)

        sub = Subscribe()
        sub.who = userId3
        sub.whom = celebId1
        subscribeRepository.save(sub)

        sub = Subscribe()
        sub.who = userId3
        sub.whom = celebId4
        subscribeRepository.save(sub)
    }

    @GetMapping("/test/report")
    fun report() {
        var report = Report()
        report.who = 61
        report.whom = 62
        report.which = 22
        report.what = Report.ReportType.USER_FEED
        report.judge = 63
        report.reason = "너 왜 눈을 그렇게 떠?"
        reportRepository.save(report)

        report = Report()
        report.who = 64
        report.whom = 62
        report.which = 22
        report.what = Report.ReportType.USER_FEED
        report.judge = 63
        report.reason = "너 왜 눈을 그렇게 떠? 222"
        reportRepository.save(report)
    }

    @GetMapping("/test/push")
    fun push() {
        val sound = false
        val vibration = true
//        val token = "is_wookje_smart?"
        val token2 = "cSo7rJy13IQ:APA91bERFXEO_9NdWU1tOldds900gyTnMND54w6P6nSJ8NLEpSVUUHHvvotYRNiXZpxyQ5nDrGboF7Fzdp_BWErJ5-X5c_M6BSb3aVOpyACHuJ3W62pQR2-GTWtlYF_KEK10UgRVjTOF"

        println(sound)
        println(vibration)

        val androidNotification = AndroidNotification.builder()
                .setTitle("팬밋")
                .setBody("test")
                .setColor("#fc660c")
        if (sound) androidNotification.setDefaultSound(true)
        else androidNotification.setDefaultSound(false).setSound("TT")
        if (vibration) androidNotification.setDefaultVibrateTimings(false).setVibrateTimingsInMillis(LongArray(4) { 500L+it*100 })
        else androidNotification.setDefaultVibrateTimings(false).setVibrateTimingsInMillis(LongArray(0))
        //androidNotification.setVisibility(AndroidNotification.Visibility.PUBLIC)

        val multicastMessage = MulticastMessage.builder()
                .setAndroidConfig(AndroidConfig.builder()
                        .setPriority(AndroidConfig.Priority.NORMAL)
                        .setNotification(androidNotification.build()).build())
                .addToken(token2)
//                .addToken(token)
                .build()

        var response = FirebaseMessaging.getInstance().sendMulticastAsync(multicastMessage)

//        println("----")
//        for (iter in response.responses) {
//            println(iter.isSuccessful)
//            println(iter.messageId)
//            println(iter.exception)
//            println("----")
//        }
    }

    @GetMapping("/test/insert/category")
    fun category() {
        var cat = Interest()
        cat.name = "youtuber"
        cat.icon = "youtuber"
        interestRepository.save(cat)

        cat = Interest()
        cat.name = "sns"
        cat.icon = "sns"
        interestRepository.save(cat)

        cat = Interest()
        cat.name = "broadcast"
        cat.icon = "broadcast"
        interestRepository.save(cat)

        cat = Interest()
        cat.name = "athlete"
        cat.icon = "athlete"
        interestRepository.save(cat)

        cat = Interest()
        cat.name = "actor"
        cat.icon = "actor"
        interestRepository.save(cat)

        cat = Interest()
        cat.name = "beauty"
        cat.icon = "beauty"
        interestRepository.save(cat)
    }

    @GetMapping("/test/insert/category/interested")
    fun interestedCategory() {
        var cat = UserInterested()
        cat.who = userId1
        cat.what = 1
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId1
        cat.what = 2
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId1
        cat.what = 3
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId2
        cat.what = 6
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId2
        cat.what = 3
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId3
        cat.what = 5
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId3
        cat.what = 4
        userInterestedRepository.save(cat)

        cat = UserInterested()
        cat.who = userId3
        cat.what = 6
        userInterestedRepository.save(cat)
    }

    @GetMapping("/test/insert/category/related")
    fun relatedCategory() {
        var cat = ActiveInterest()
        cat.who = celebId1
        cat.what = 1
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId1
        cat.what = 5
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId2
        cat.what = 2
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId2
        cat.what = 6
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId3
        cat.what = 4
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId4
        cat.what = 6
        activeInterestRepository.save(cat)

        cat = ActiveInterest()
        cat.who = celebId4
        cat.what = 2
        activeInterestRepository.save(cat)
    }

    @GetMapping("/test/insert/feed/user")
    fun userFeed() {
        var feed = Feed()
        feed.celeb = celebId1
        feed.who = userId1
        feed.text = "아이유 콘서트 마렵다"
        feed.category = 1L
        feedRepository.save(feed)

        feed = Feed()
        feed.celeb = celebId1
        feed.who = userId2
        feed.text = "너두? 야, 나두."
        feed.category = 1L
        feedRepository.save(feed)

        feed = Feed()
        feed.celeb = celebId2
        feed.who = userId3
        feed.text = "궁예님 어디에 있나요"
        feed.category = 1L
        feedRepository.save(feed)

        feed = Feed()
        feed.celeb = celebId3
        feed.who = userId1
        feed.text = "부부의 세계 안 봤어요"
        feed.category = 1L
        feedRepository.save(feed)
    }

    @GetMapping("/test/insert/report/feed/user/comment")
    fun userFeedComment() {
        var com = Comment()
        com.section = Comment.Section.USER_FEED
        com.feed = 1
        com.who = userId1
        com.text = "ㄹㅇ ㅋㅋ"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.USER_FEED
        com.feed = 1
        com.who = userId1
        com.text = "ㄹㅇ ㅋㅋ 22"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.USER_FEED
        com.feed = 2
        com.who = userId3
        com.text = "ㄹㅇ ㅋㅋ;;;"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.USER_FEED
        com.feed = 3
        com.who = userId2
        com.text = "ㄹㅇ ㅋㅋ;;; 22"
        com.celeb = celebId2
        commentRepository.save(com)
    }

    @GetMapping("/test/insert/feed/celeb")
    fun celebFeed() {
//        var feed = CelebFeed()
//        feed.who = celebId1
//        feed.text = "야~ 내 콘서트가 언젠지 알어?!?!"
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//        var file = CelebFeedFile()
//        file.parent = feed.id
//        file.who = celebId1
//        file.url = "feed_file_url_1"
//        celebFeedFileRepository.save(file)
//        feed.pictureList = listOf(file).toMutableList()
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//
//        feed = CelebFeed()
//        feed.who = celebId1
//        feed.text = "야 ~ 코로나 때문에 콘서트도 못하고~~ 어?!"
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//        file = CelebFeedFile()
//        file.parent = feed.id
//        file.who = celebId1
//        file.url = "feed_file_url_2"
//        celebFeedFileRepository.save(file)
//        feed.pictureList = listOf(file).toMutableList()
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//
//        feed = CelebFeed()
//        feed.who = celebId2
//        feed.text = "한소희 celebFeed1"
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//        feed.pictureList = listOf(file).toMutableList()
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//
//        feed = CelebFeed()
//        feed.who = celebId2
//        feed.text = "한소희 celebFeed2"
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//        feed.pictureList = listOf(file).toMutableList()
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//
//        feed = CelebFeed()
//        feed.who = celebId3
//        feed.text = "누가 콘서트 소리를 내었어?"
//        feed.category = 1L
//        celebFeedRepository.save(feed)
//        file = CelebFeedFile()
//        file.parent = feed.id
//        file.who = celebId3
//        file.url = "feed_file_url_4"
//        celebFeedFileRepository.save(file)
//        feed.pictureList = listOf(file).toMutableList()
//        feed.category = 1L
//        celebFeedRepository.save(feed)
    }

    @GetMapping("/test/insert/report/feed/celeb/comment")
    fun celebFeedComment() {
        var com = Comment()
        com.section = Comment.Section.CELEB_FEED
        com.feed = 1
        com.who = userId1
        com.text = "ㄹㅇ ㅋㅋ"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.CELEB_FEED
        com.feed = 1
        com.who = userId1
        com.text = "ㄹㅇ ㅋㅋ 22"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.CELEB_FEED
        com.feed = 2
        com.who = userId3
        com.text = "ㄹㅇ ㅋㅋ;;;"
        com.celeb = celebId1
        commentRepository.save(com)

        com = Comment()
        com.section = Comment.Section.CELEB_FEED
        com.feed = 3
        com.who = celebId1
        com.text = "ㄹㅇ ㅋㅋ;;; 22"
        com.celeb = celebId1
        commentRepository.save(com)
    }

    @GetMapping("/test/insert/content")
    fun content() {
        var content = Content()
        content.who = celebId1
        content.title = "content title 1"
        content.text = "content text 1"
        contentRepository.save(content)
        var file = ContentFile()
        file.who = celebId1
        file.url = "content_file_url_1"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)

        content = Content()
        content.who = celebId2
        content.title = "content title 2"
        content.text = "content text 2"
        contentRepository.save(content)
        file = ContentFile()
        file.who = celebId2
        file.url = "content_file_url_2"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)

        content = Content()
        content.who = celebId2
        content.title = "content title 22"
        content.text = "content text 22"
        contentRepository.save(content)
        file = ContentFile()
        file.who = celebId2
        file.url = "content_file_url_3"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)

        content = Content()
        content.who = celebId4
        content.title = "content title 4"
        content.text = "content text 4"
        contentRepository.save(content)
        file = ContentFile()
        file.who = celebId4
        file.url = "content_file_url_4"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)

        content = Content()
        content.who = celebId4
        content.title = "content title 44"
        content.text = "content text 44"
        contentRepository.save(content)
        file = ContentFile()
        file.who = celebId4
        file.url = "content_file_url_5"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)

        content = Content()
        content.who = celebId4
        content.title = "content title 444"
        content.text = "content text 444"
        contentRepository.save(content)
        file = ContentFile()
        file.who = celebId4
        file.url = "content_file_url_6"
        content.pictureList = listOf(file).toMutableList()
        contentRepository.save(content)
    }
}