package ai.maum.kotlin.controller.impl.home

import ai.maum.kotlin.controller.HomeController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.content.CelebRecentActivityVo
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.interest.UserInterestedRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.home.HomeListElementDto
import ai.maum.kotlin.model.http.dto.home.HomeResponseDto
import ai.maum.kotlin.model.http.dto.home.HomeSubscribingCelebDto
import ai.maum.kotlin.model.media.mediaUrl
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class LoginHome : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var homeController: HomeController? = null

    var userId: Long? = null

    var userRepository: UserRepository? = null
    var subscribeRepository: SubscribeRepository? = null
    var userInterestedRepository: UserInterestedRepository? = null
    var celebFeedRepository: CelebFeedRepository? = null
    var contentRepository: ContentRepository? = null
    var membershipRepository: MembershipRepository? = null
    var userMembershipHistoryRepository: UserMembershipHistoryRepository? = null

    private val subscribingCelebSet = mutableSetOf<Long>()
    private val relatedCelebSet = mutableSetOf<Long>()
    private val otherCelebSet = mutableSetOf<Long>()

    private fun getSubscribingCelebList(): MutableList<User> {
        return subscribeRepository?.findAllByWhoAndActiveIsTrue(userId!!)?.mapNotNull {
            userRepository?.findByIdAndActiveIsTrueAndCelebActiveIsTrue(it.whom!!)
        }?.toMutableList() ?: mutableListOf()
    }

    private fun getRelatedCelebList(): MutableList<User> {
        val userInterestedIdList = userInterestedRepository?.findUserInterestedListByUserId(userId!!) ?: emptyList()
        val relatedCelebList =
            userRepository?.findRelatedCelebListByInterestedCategoryList(userInterestedIdList)?.toMutableList()
                ?: mutableListOf()
        relatedCelebList.forEach {
            if (!subscribingCelebSet.contains(it.id!!)) {
                relatedCelebSet.add(it.id!!)
            }
        }
        relatedCelebList.sortWith(compareBy { it.id })
        return relatedCelebList.toMutableList()
    }

    private fun getOtherCelebList(): MutableList<User> {
        val existingCelebSet = subscribingCelebSet
        existingCelebSet.addAll(relatedCelebSet)
        if (existingCelebSet.isEmpty()) existingCelebSet.add(-1000000000L)
        val otherCelebList = userRepository?.findAllOtherCeleb(existingCelebSet) ?: emptyList()
        otherCelebList.forEach {
            existingCelebSet.add(it.id!!)
            otherCelebSet.add(it.id!!)
        }
        return otherCelebList.toMutableList()
    }

    private fun getRecentActivityList(targetCelebSet: Iterable<Long>): MutableList<CelebRecentActivityVo> {
        val recentContentVoList = mutableListOf<CelebRecentActivityVo>()
        targetCelebSet.forEach {
            val membership =
                userMembershipHistoryRepository?.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(
                    userId!!,
                    it
                )
            var userLevel = membership?.membership?.tier ?: 0
            if (membership != null) {
                if (membership.status != UserMembershipHistory.Status.NORMAL && membership.status != UserMembershipHistory.Status.TO_BE_EXPIRED) {
                    userLevel = 0
                } else {
                    userLevel = membership.membership?.tier!!
                }
            }
            val entity = contentRepository?.findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(
                it,
                userLevel
            )
            if (entity != null) {
                val vo = CelebRecentActivityVo(entity)
                recentContentVoList.add(vo)
            }
        }

        val recentCelebFeedVoList = mutableListOf<CelebRecentActivityVo>()
        targetCelebSet.forEach {
            val membership =
                userMembershipHistoryRepository?.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(
                    userId!!,
                    it
                )
            var userLevel = membership?.membership?.tier ?: 0
            if (membership != null) {
                if (membership.status != UserMembershipHistory.Status.NORMAL && membership.status != UserMembershipHistory.Status.TO_BE_EXPIRED) {
                    userLevel = 0
                } else {
                    userLevel = membership.membership?.tier!!
                }
            }
            val entity = celebFeedRepository?.findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(
                it,
                userLevel
            )
            if (entity != null) {
                val vo = CelebRecentActivityVo(entity)
                recentCelebFeedVoList.add(vo)
            }
        }

        val resultVoList = mutableListOf<CelebRecentActivityVo>()
        recentContentVoList.sortWith(compareBy { it.who })
        recentCelebFeedVoList.sortWith(compareBy { it.who })
        var contentIndex = 0
        var feedIndex = 0
        while (contentIndex < recentContentVoList.size || feedIndex < recentCelebFeedVoList.size) {
            if (contentIndex >= recentContentVoList.size) {
                resultVoList.add(recentCelebFeedVoList[feedIndex])
                feedIndex++
                continue
            }
            if (feedIndex >= recentCelebFeedVoList.size) {
                resultVoList.add(recentContentVoList[contentIndex])
                contentIndex++
                continue
            }

            if (recentContentVoList[contentIndex].who < recentCelebFeedVoList[feedIndex].who) {
                resultVoList.add(recentContentVoList[contentIndex])
                contentIndex++
            } else if (recentContentVoList[contentIndex].who > recentCelebFeedVoList[feedIndex].who) {
                resultVoList.add(recentCelebFeedVoList[feedIndex])
                feedIndex++
            } else {
                if (recentContentVoList[contentIndex].timestamp > recentCelebFeedVoList[feedIndex].timestamp) {
                    resultVoList.add(recentContentVoList[contentIndex])
                } else if (recentContentVoList[contentIndex].timestamp < recentCelebFeedVoList[feedIndex].timestamp) {
                    resultVoList.add(recentCelebFeedVoList[feedIndex])
                }
                contentIndex++
                feedIndex++
            }
        }

        return resultVoList
    }

    private fun mergeSubscribingCelebActivity(
        subscribingCelebList: MutableList<User>,
        subscribingCelebActivityVoList: MutableList<CelebRecentActivityVo>
    ): MutableList<HomeListElementDto> {
        val response = mutableListOf<HomeListElementDto>()

        subscribingCelebList.sortWith(compareBy { it.id })
        subscribingCelebActivityVoList.sortWith(compareBy { it.who })

        if (subscribingCelebActivityVoList.isEmpty()) {
            return response
        }

        var activityIndex = 0
        subscribingCelebList.forEach {
            while (
                activityIndex < subscribingCelebActivityVoList.size &&
                subscribingCelebActivityVoList[activityIndex].who < it.id!!
            ) activityIndex++
            if (activityIndex >= subscribingCelebActivityVoList.size) return@forEach
            if (subscribingCelebActivityVoList[activityIndex].who == it.id) {
                val currentActivity = subscribingCelebActivityVoList[activityIndex]
                response.add(
                    HomeListElementDto(
                        currentActivity.timestamp,
                        it.id!!,
                        it.name!!,
                        it.profileImageUrl?.mediaUrl(),
                        currentActivity.type,
                        currentActivity.id,
                        currentActivity.text,
                        currentActivity.mediaUrl?.mediaUrl()
                    )
                )
                activityIndex++
            }
        }

        response.sortWith(compareByDescending { it.timestamp })

        return response
    }

    private fun mergeRelatedCelebActivity(
        relatedCelebList: MutableList<User>,
        relatedCelebActivityVoList: MutableList<CelebRecentActivityVo>
    ): MutableList<HomeListElementDto> {
        val response = mutableListOf<HomeListElementDto>()

        relatedCelebList.sortWith(compareBy { it.id })
        relatedCelebActivityVoList.sortWith(compareBy { it.who })

        if (relatedCelebActivityVoList.isEmpty()) {
            return response
        }

        var activityIndex = 0
        relatedCelebList.forEach {
            while (
                activityIndex < relatedCelebActivityVoList.size &&
                relatedCelebActivityVoList[activityIndex].who < it.id!!
            ) activityIndex++
            if (activityIndex >= relatedCelebActivityVoList.size) return@forEach
            if (relatedCelebActivityVoList[activityIndex].who == it.id) {
                val currentActivity = relatedCelebActivityVoList[activityIndex]
                response.add(
                    HomeListElementDto(
                        currentActivity.timestamp,
                        it.id!!,
                        it.name!!,
                        it.profileImageUrl?.mediaUrl(),
                        currentActivity.type,
                        currentActivity.id,
                        currentActivity.text,
                        currentActivity.mediaUrl?.mediaUrl()
                    )
                )
                activityIndex++
            }
        }

        response.sortWith(compareByDescending { it.timestamp })

        return response
    }

    private fun mergeOtherCelebActivity(
        otherCelebList: MutableList<User>,
        otherCelebActivityVoList: MutableList<CelebRecentActivityVo>
    ): MutableList<HomeListElementDto> {
        val response = mutableListOf<HomeListElementDto>()

        otherCelebList.sortWith(compareBy { it.id })
        otherCelebActivityVoList.sortWith(compareBy { it.who })

        if (otherCelebActivityVoList.isEmpty()) {
            return response
        }

        var activityIndex = 0
        otherCelebList.forEach {
            while (
                activityIndex < otherCelebActivityVoList.size &&
                otherCelebActivityVoList[activityIndex].who < it.id!!
            ) activityIndex++
            if (activityIndex >= otherCelebActivityVoList.size) return@forEach
            if (otherCelebActivityVoList[activityIndex].who == it.id) {
                val currentActivity = otherCelebActivityVoList[activityIndex]
                val add = response.add(
                    HomeListElementDto(
                        currentActivity.timestamp,
                        it.id!!,
                        it.name!!,
                        it.profileImageUrl?.mediaUrl(),
                        currentActivity.type,
                        currentActivity.id,
                        currentActivity.text,
                        currentActivity.mediaUrl?.mediaUrl()
                    )
                )
                activityIndex++
            }
        }

        response.sortWith(compareByDescending { it.timestamp })

        return response
    }

    override operator fun invoke(): ResponseType {
        val controller = homeController!!

        userId =
            controller.fanmeet.userInfoFactory.`object`.userId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        userRepository = controller.fanmeet.userRepository
        subscribeRepository = controller.fanmeet.subscribeRepository
        userInterestedRepository = controller.fanmeet.userInterestedRepository
        celebFeedRepository = controller.fanmeet.celebFeedRepository
        contentRepository = controller.fanmeet.contentRepository
        membershipRepository = controller.fanmeet.membershipRepository
        val userMembershipHistoryRepository = controller.fanmeet.userMembershipHistoryRepository

        val self = userRepository!!.findByIdAndActiveIsTrue(userId!!)
            ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Self")

        val subscribingCelebList = getSubscribingCelebList()
        val subscribingCelebActivityVoList = getRecentActivityList(subscribingCelebSet)
        val subscribingCelebResponse =
            mergeSubscribingCelebActivity(subscribingCelebList, subscribingCelebActivityVoList)

        val relatedCelebList = getRelatedCelebList()
        val relatedCelebActivityVoList = getRecentActivityList(relatedCelebSet)
        val relatedCelebResponse = mergeRelatedCelebActivity(relatedCelebList, relatedCelebActivityVoList)

        val otherCelebList = getOtherCelebList()
        val otherCelebActivityVoList = getRecentActivityList(otherCelebSet)
        val otherCelebResponse = mergeOtherCelebActivity(otherCelebList, otherCelebActivityVoList)

        val homeSubscribingCelebList = mutableListOf<HomeSubscribingCelebDto>()
        subscribingCelebList.forEach {
            val dto = HomeSubscribingCelebDto(
                it.id!!,
                it.profileImageUrl?.mediaUrl(),
                it.name!!,
                userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                    userId!!,
                    it.id!!,
                    UserMembershipHistory.Status.EXPIRED
                )?.membership?.badgeImageUrl?.mediaUrl()
            )
            homeSubscribingCelebList.add(dto)
        }
        self.celeb?.let {
            homeSubscribingCelebList.removeIf { it.id == userId }
            homeSubscribingCelebList.add(
                0,
                HomeSubscribingCelebDto(userId!!, self.profileImageUrl?.mediaUrl(), self.name!!)
            )
        }

        val recentActivityResponse = mutableListOf<HomeListElementDto>()
        recentActivityResponse.addAll(subscribingCelebResponse)
        recentActivityResponse.addAll(relatedCelebResponse)
        recentActivityResponse.addAll(otherCelebResponse)

        val response = HomeResponseDto(
            homeSubscribingCelebList,
            recentActivityResponse
        )

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        homeController = controller as HomeController
    }

    override fun setDto(dto: Any) {
    }
}
