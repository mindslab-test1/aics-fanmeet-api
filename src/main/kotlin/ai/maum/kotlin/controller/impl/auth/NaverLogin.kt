package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.authentication.naver.NaverUserToken
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.LoginResponseDto
import ai.maum.kotlin.model.http.dto.auth.NaverLoginDto
import ai.maum.kotlin.model.http.dto.auth.NaverLoginValidationDto
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException

class NaverLogin(private val controller: AuthController, private val naverLoginDto: NaverLoginDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        val restTemplate = controller.restTemplate

        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer ${naverLoginDto.accessToken}")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        // https://openapi.naver.com/v1/nid/verify를 쓰는 방법도 있는데, response 필드명에 슬래시가 있어서 DTO로 하기 껄끄럽다
        val response = restTemplate.exchange("https://openapi.naver.com/v1/nid/me", HttpMethod.GET, entity, NaverLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val validationResult = response.body ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        // Check existence in DB and retrieve user id(row id)
        val user = controller.userRepository.findByNaverUserIdAndActiveIsTrue(validationResult.response?.id ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        val userId = user?.id ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED)

        // Duplication check (deviceId)
        DeviceIdDuplicationHelper(controller, naverLoginDto.deviceId!!)()

        // Token registration
        val token = NaverUserToken()
        token.accessToken = naverLoginDto.accessToken
        token.naverUserId = validationResult.response!!.id!!
        token.userId = userId
        token.deviceId = naverLoginDto.deviceId
        controller.naverUserTokenRepository.save(token)

        return ResponseEntity.ok(LoginResponseDto(
                userId = userId,
                isCeleb = (user.celeb != null)
        ))
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}