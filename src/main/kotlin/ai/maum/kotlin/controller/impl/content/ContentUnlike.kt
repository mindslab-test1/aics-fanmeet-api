package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentUnlikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentUnlike : HandlerType {
    var contentController: ContentController? = null
    var contentUnlikeRequestDto: ContentUnlikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val contentRepository = fanmeet.contentRepository
        val dto = contentUnlikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val content = contentRepository.findByIdAndActiveIsTrue(dto.content!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Content")

        val like = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.content!!, Like.TargetType.CONTENT)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Like")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.content!!, Like.TargetType.CONTENT)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        content.likeCount = content.likeCount!! - 1
        like.active = false

        contentRepository.save(content)
        likeRepository.save(like)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentUnlikeRequestDto = dto as ContentUnlikeRequestDto
    }
}