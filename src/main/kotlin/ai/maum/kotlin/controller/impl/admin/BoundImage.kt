package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.BoundImageRequestDto
import ai.maum.kotlin.model.media.mediaUrl
import com.sksamuel.scrimage.ImmutableImage
import com.sksamuel.scrimage.metadata.ImageMetadata
import com.sksamuel.scrimage.nio.GifWriter
import com.sksamuel.scrimage.nio.JpegWriter
import com.sksamuel.scrimage.nio.PngWriter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.URI

class BoundImage : HandlerType {
    var adminController: AdminController? = null
    var boundImageRequestDto: BoundImageRequestDto? = null
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val feedFileRepository = fanmeet.feedFileRepository
        val celebFeedFileRepository = fanmeet.celebFeedFileRepository
        val contentFileRepository = fanmeet.contentFileRepository
        val dto = boundImageRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val restTemplate = fanmeet.restTemplate

        if (dto.secret != "djaakwOgmfrajrdj")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val split = """((.+)/)?(.+)\.(.+)""".toRegex()
        val users = userRepository.findByActiveIsTrue()

        // Profile Images
        for (user in users) {
            val profileRelative = user.profileImageUrl ?: continue
            val profileAbsolute = profileRelative.mediaUrl()
            val profileMatchResult = split.matchEntire(profileAbsolute) ?: continue
            val (_, profileDirectory, profileFilename, profileExtension) = profileMatchResult.destructured

            val profileURI = URI.create(profileAbsolute)
            val profileBytes = restTemplate.getForObject(profileURI, ByteArray::class.java) ?: continue

            GlobalScope.launch {
                try {
                    if (profileExtension.toUpperCase() != "GIF")
                        fanmeet.mediaUploader.safeUpload(profileRelative, object : ByteArrayResource(boundProfileImage(profileBytes, profileExtension)) {
                            override fun getFilename(): String {
                                return "profileImage.${profileExtension.toLowerCase()}"
                            }
                        })
                } catch (e: Exception) {
                    logger.debug("ProfileImage " + e.message)
                }
            }
        }

        // Banner Images
        for (user in users) {
            val bannerRelative = user.bannerImageUrl ?: continue
            val bannerAbsolute = bannerRelative.mediaUrl()
            val bannerMatchResult = split.matchEntire(bannerAbsolute) ?: continue
            val (_, bannerDirectory, bannerFilename, bannerExtension) = bannerMatchResult.destructured

            val bannerURI = URI.create(bannerAbsolute)
            val bannerBytes = restTemplate.getForObject(bannerURI, ByteArray::class.java) ?: continue

            GlobalScope.launch {
                try {
                    if (bannerExtension.toUpperCase() != "GIF")
                        fanmeet.mediaUploader.safeUpload(bannerRelative, object : ByteArrayResource(boundBannerImage(bannerBytes, bannerExtension)) {
                            override fun getFilename(): String {
                                return "bannerImage.${bannerExtension.toLowerCase()}"
                            }
                        })
                } catch (e: Exception) {
                    logger.debug("BannerImage " + e.message)
                }
            }
        }

        // Article Images
        val feedFiles = feedFileRepository.findByActiveIsTrue()
        for (feedFile in feedFiles) {
            val feedRelative = feedFile.url ?: continue
            val feedAbsolute = feedRelative.mediaUrl()
            val feedMatchResult = split.matchEntire(feedAbsolute) ?: continue
            val (_, feedDirectory, feedFilename, feedExtension) = feedMatchResult.destructured

            val feedURI = URI.create(feedAbsolute)
            val feedBytes = restTemplate.getForObject(feedURI, ByteArray::class.java) ?: continue

            GlobalScope.launch {
                try {
                    if (feedExtension.toUpperCase() != "GIF")
                        fanmeet.mediaUploader.safeUpload(feedRelative, object : ByteArrayResource(boundFeedImage(feedBytes, feedExtension)) {
                            override fun getFilename(): String {
                                return "feedImage.${feedExtension.toLowerCase()}"
                            }
                        })
                } catch (e: Exception) {
                    logger.debug("ArticleImage " + e.message)
                }
            }
        }

        val celebFeedFiles = celebFeedFileRepository.findByActiveIsTrue()
        for (celebFeedFile in celebFeedFiles) {
            val celebFeedRelative = celebFeedFile.url ?: continue
            val celebFeedAbsolute = celebFeedRelative.mediaUrl()
            val celebFeedMatchResult = split.matchEntire(celebFeedAbsolute) ?: continue
            val (_, celebFeedDirectory, celebFeedFilename, celebFeedExtension) = celebFeedMatchResult.destructured

            val celebFeedURI = URI.create(celebFeedAbsolute)
            val celebFeedBytes = restTemplate.getForObject(celebFeedURI, ByteArray::class.java) ?: continue

            GlobalScope.launch {
                try {
                    if (celebFeedExtension.toUpperCase() != "GIF")
                        fanmeet.mediaUploader.safeUpload(celebFeedRelative, object : ByteArrayResource(boundFeedImage(celebFeedBytes, celebFeedExtension)) {
                            override fun getFilename(): String {
                                return "celebFeedImage.${celebFeedExtension.toLowerCase()}"
                            }
                        })
                } catch (e: Exception) {
                    logger.debug("CelebFeedFileImage " + e.message)
                }
            }
        }

        val contentFiles = contentFileRepository.findByActiveIsTrue()
        for (contentFile in contentFiles) {
            val contentRelative = contentFile.url ?: continue
            val contentAbsolute = contentRelative.mediaUrl()
            val contentMatchResult = split.matchEntire(contentAbsolute) ?: continue
            val (_, contentDirectory, contentFilename, contentExtension) = contentMatchResult.destructured

            val contentURI = URI.create(contentAbsolute)
            val contentBytes = restTemplate.getForObject(contentURI, ByteArray::class.java) ?: continue

            GlobalScope.launch {
                try {
                    if (contentExtension.toUpperCase() != "GIF")
                        fanmeet.mediaUploader.safeUpload(contentRelative, object : ByteArrayResource(boundFeedImage(contentBytes, contentExtension)) {
                            override fun getFilename(): String {
                                return "contentImage.${contentExtension.toLowerCase()}"
                            }
                        })
                } catch (e: Exception) {
                    logger.debug("ContentFileImage " + e.message)
                }
            }
        }

        return ResponseEntity.ok(Unit)
    }

    private fun boundProfileImage(bytes: ByteArray, extension: String): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        val gif = extension.toUpperCase() == "GIF"
        // 256x256
        val resImage = if (gif) image else image.bound(256, 256)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, ImageMetadata.empty, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    private fun boundFeedImage(bytes: ByteArray, extension: String): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        // 1440x822
        // Use bigger number to prevent over-downscaling for inverse-ratio images
        val gif = extension.toUpperCase() == "GIF"
        val resImage = if (gif) image else image.bound(1440, 1440)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, ImageMetadata.empty, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    private fun boundBannerImage(bytes: ByteArray, extension: String): ByteArray {
        val bis = ByteArrayInputStream(bytes)
        val bos = ByteArrayOutputStream()

        val image = ImmutableImage.loader().fromStream(bis)
        // 1600x396
        // Use bigger number to prevent over-downscaling for inverse-ratio images
        val gif = extension.toUpperCase() == "GIF"
        val resImage = if (gif) image else image.bound(1600, 1600)

        val writer = when (extension.toUpperCase()) {
            "JPEG" -> JpegWriter()
            "JPG" -> JpegWriter()
            "PNG" -> PngWriter()
            "GIF" -> GifWriter(true)
            else -> throw Exception("Unsupported image type: $extension. JPEG(JPG), PNG, GIF allowed.")
        }
        writer.write(resImage, ImageMetadata.empty, bos)
        val result = bos.toByteArray()

        bis.close()
        bos.close()

        return result
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        boundImageRequestDto = dto as BoundImageRequestDto
    }
}