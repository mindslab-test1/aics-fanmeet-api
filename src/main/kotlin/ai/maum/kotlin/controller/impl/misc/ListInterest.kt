package ai.maum.kotlin.controller.impl.misc

import ai.maum.kotlin.controller.MiscController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.misc.InterestItem
import ai.maum.kotlin.model.http.dto.misc.ListInterestResponseDto
import org.springframework.http.ResponseEntity

class ListInterest(private val miscController: MiscController) : HandlerType {
    override operator fun invoke(): ResponseType {
        val repository = miscController.fanmeet.interestRepository
        val interestList = repository.findAllByActiveIsTrue()

        val response = ListInterestResponseDto()
        val interestItemList = mutableListOf<InterestItem>()

        for (interest in interestList) {
            val item = InterestItem()
            item.id = interest.id
            item.name = interest.name
            item.icon = interest.icon

            interestItemList.add(item)
        }
        response.interests = interestItemList

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        TODO("Not yet implemented")
    }

    override fun setDto(dto: Any) {
        TODO("Not yet implemented")
    }
}