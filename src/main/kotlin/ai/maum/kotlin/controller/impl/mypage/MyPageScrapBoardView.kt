package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageScrapBoardViewDto
import ai.maum.kotlin.model.http.dto.mypage.MyPageScrapBoardViewResponse
import ai.maum.kotlin.model.http.dto.mypage.ScrapBoardVo
import ai.maum.kotlin.model.http.dto.mypage.ScrapFeedKind
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageScrapBoardView: HandlerType {
    var myPageController: MyPageController ?= null
    var myPageScrapBoardViewDto : MyPageScrapBoardViewDto ?= null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageScrapBoardViewDto = dto as MyPageScrapBoardViewDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw  ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val dto = myPageScrapBoardViewDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")


        val scrapRepository = fanmeet.scrapRepository
        val userRepository = fanmeet.userRepository

        val feedRepository = fanmeet.feedRepository
        val celebFeedRepository = fanmeet.celebFeedRepository
        val contentRepository = fanmeet.contentRepository

        val likeRepository = fanmeet.likeRepository
        val feedCategoryRepository = fanmeet.feedCategoryRepository
        val celebFeedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val contentCategoryRepository = fanmeet.contentCategoryRepository

        val boardFeedIdList = scrapRepository.findAllByBoardAndActiveIsTrue(dto.boardId!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "board feed list")
        val resultMutableList = mutableListOf<Any>()
        for (value in boardFeedIdList) {
            when(value.targetType!!){
                Scrap.TargetType.NONE -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "switch case")
                Scrap.TargetType.USER_FEED -> {
                    val userFeed = feedRepository.findByIdAndActiveIsTrue(value.targetId!!)
                    if(userFeed != null){
                        val writeUser = userRepository.findByIdAndActiveIsTrue(userFeed.who!!)
                        val scrapData = ScrapBoardVo()
                        scrapData.userId = userFeed.who
                        scrapData.feedId = userFeed.id
                        scrapData.userName = writeUser?.name
                        scrapData.profileImg = writeUser?.profileImageUrl?.mediaUrl()
                        scrapData.postTimeStamp = userFeed.created
                        scrapData.category = feedCategoryRepository.findByIdAndActiveIsTrue(userFeed.category!!)?.name
                        scrapData.imgUrl = userFeed.pictureList?.map {
                            return@map it.url?.mediaUrl().toString()
                        }
                        scrapData.videoUrl = userFeed.video?.url
                        scrapData.text = userFeed.text
                        scrapData.scrapCount = userFeed.scrapCount
                        scrapData.isScrap = true
                        scrapData.likeCount = userFeed.likeCount
                        scrapData.isLike = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(
                                userId,
                                userFeed.id!!,
                                Like.TargetType.USER_FEED
                        )
                        scrapData.commentCount = userFeed.commentCount
                        scrapData.canView = true
                        scrapData.scrapFeedKind = ScrapFeedKind.USER_FEED
                        resultMutableList.add(scrapData)
                    }
                }
                Scrap.TargetType.CELEB_FEED -> {
                    val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(value.targetId!!)
                    if(celebFeed != null) {
                        val writeCeleb = userRepository.findByIdAndActiveIsTrue(celebFeed.who!!)
                        val scrapData = ScrapBoardVo()
                        scrapData.userId = celebFeed.who
                        scrapData.feedId = celebFeed.id
                        scrapData.userName = writeCeleb?.name
                        scrapData.profileImg = writeCeleb?.profileImageUrl
                        scrapData.postTimeStamp = celebFeed.created
                        scrapData.category = celebFeedCategoryRepository.findByIdAndActiveIsTrue(celebFeed.category!!)?.name
                        scrapData.imgUrl = celebFeed.pictureList?.map {
                            return@map it.url?.mediaUrl().toString()
                        }
                        scrapData.videoUrl = celebFeed.video?.url
                        scrapData.text = celebFeed.text
                        scrapData.scrapCount = celebFeed.scrapCount
                        scrapData.isScrap = true
                        scrapData.likeCount = celebFeed.likeCount
                        scrapData.isLike = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(
                                userId,
                                celebFeed.id!!,
                                Like.TargetType.CELEB_FEED
                        )
                        scrapData.commentCount = celebFeed.commentCount
                        scrapData.canView = true
                        scrapData.scrapFeedKind = ScrapFeedKind.CELEB_FEED
                        resultMutableList.add(scrapData)
                    }
                }
                Scrap.TargetType.CONTENT -> {
                    val content = contentRepository.findByIdAndActiveIsTrue(value.targetId!!)
                    if(content != null){
                        val writeCeleb = userRepository.findByIdAndActiveIsTrue(content.who!!)
                        val scrapData = ScrapBoardVo()
                        scrapData.userId = content.who
                        scrapData.feedId = content.id
                        scrapData.title = content.title
                        scrapData.userName = writeCeleb?.name
                        scrapData.profileImg = writeCeleb?.profileImageUrl
                        scrapData.postTimeStamp = content.created
                        scrapData.category = contentCategoryRepository.findByIdAndActiveIsTrue(content.category!!)?.name
                        scrapData.imgUrl = content.pictureList?.map {
                            return@map it.url?.mediaUrl().toString()
                        }
                        scrapData.videoUrl = content.video?.url
                        scrapData.text = content.text
                        scrapData.scrapCount = content.scrapCount
                        scrapData.isScrap = true
                        scrapData.likeCount = content.likeCount
                        scrapData.isLike = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(
                                userId,
                                content.id!!,
                                Like.TargetType.CELEB_FEED
                        )
                        scrapData.commentCount = content.commentCount
                        scrapData.canView = true
                        scrapData.scrapFeedKind = ScrapFeedKind.CONTENT
                        resultMutableList.add(scrapData)
                    }
                }
            }
        }

        val myPageScrapBoardViewResponse = MyPageScrapBoardViewResponse()
        myPageScrapBoardViewResponse.scrapList = resultMutableList
        return ResponseEntity.ok(myPageScrapBoardViewResponse)
    }

}