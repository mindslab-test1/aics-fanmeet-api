package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.content.ContentFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentEditRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentEdit : HandlerType {
    var contentController: ContentController? = null
    var contentEditRequestDto: ContentEditRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val contentCategoryRepository = fanmeet.contentCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = contentEditRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val removedList = dto.removedPictures?.replace(" ","")?.removeSurrounding("[", "]")?.split(",")?.map { it.toLong() }
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // Data range check
        dto.text?.let { if (it.length > 10000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.addedPictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        dto.category?.let {
            if (dto.category!! > 0)
                contentCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
            else dto.category = null
        }

        // Access level validity check
        dto.accessLevel?.let {
            if (it <= 0)
//                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Access Level")
                return@let
            val minimumMembership = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(userId, dto.accessLevel!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No Membership With Given Access level")
        }

        // TTS Ownership check
        val tts = dto.tts?.let { ttsRepository.findByIdAndActiveIsTrue(dto.tts!!) }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Retrieve Feed
        val content = contentRepository.findByIdAndActiveIsTrue(contentEditRequestDto!!.content!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Content")

        // Picture Count & Index Check
        var pictureCount = content.pictureList?.size ?: 0
        removedList?.let {
            if (removedList.size > pictureCount)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto RemovedPictures Size")
            for (index in removedList)
                if (index >= content.pictureList!!.size)
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Feed PictureList Size")
            pictureCount -= removedList.size
        }
        dto.addedPictures?.let {
            pictureCount += dto.addedPictures!!.size
            if (pictureCount > 5)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "PictureCount")
        }

        val updatedPictureList = content.pictureList?.toMutableList() ?: mutableListOf()
        // Remove Pictures
        removedList?.let {
            val descendingOrder = removedList.sortedDescending()
            for (index in descendingOrder) {
                updatedPictureList.removeAt(index.toInt())
            }
        }

        // Picture Upload
        dto.addedPictures?.let {
            if (dto.addedPictures!!.isEmpty())
                return@let
            val uploadedFiles = contentController!!.fanmeet.mediaUploader.uploadAsyncFeedImageWithBlurred(dto.addedPictures!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "UploadedFiles")
            for (fileName in uploadedFiles) {
                val contentFile = ContentFile()
                contentFile.url = fileName.first
                contentFile.blurredUrl = fileName.second
                contentFile.what = ContentFile.FileType.IMAGE
                contentFile.who = userId
                updatedPictureList.add(contentFile)
            }
        }

        // Apply Updated Picture List
        content.pictureList = updatedPictureList

        // Video Upload & Apply Updated
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")

            val thumbnailFile = ContentFile()
            thumbnailFile.url = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            thumbnailFile.what = ContentFile.FileType.IMAGE
            content.thumbnail = thumbnailFile

            val videoFile = ContentFile()
            videoFile.url = fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
            videoFile.what = ContentFile.FileType.VIDEO
            content.video = videoFile
        }

        dto.title?.let { content.title = dto.title }
        dto.category?.let { content.category = dto.category }
        dto.text?.let { content.text = dto.text }
        dto.youtube?.let { content.youtube = dto.youtube }
        dto.tts?.let { content.tts = tts }
        dto.accessLevel?.let { content.accessLevel = dto.accessLevel }
        content.modifyYn = 1
        contentRepository.save(content)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentEditRequestDto = dto as ContentEditRequestDto
    }
}