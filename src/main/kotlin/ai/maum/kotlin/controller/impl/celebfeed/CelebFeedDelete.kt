package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedDeleteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedDelete : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedDeleteRequestDto: CelebFeedDeleteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val scrapRepository = fanmeet.scrapRepository
        val dto = celebFeedDeleteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(dto.celebFeed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeed")

        celebFeed.active = false

        val scraps = scrapRepository.findByTargetIdAndTargetTypeAndActiveIsTrue(celebFeed.id!!, Scrap.TargetType.CELEB_FEED)
        for (scrap in scraps) {
            scrap.active = false
        }

        celebFeedRepository.save(celebFeed)
        scrapRepository.saveAll(scraps)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedDeleteRequestDto = dto as CelebFeedDeleteRequestDto
    }
}