package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.feed.Feed
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedItemDto
import ai.maum.kotlin.model.http.dto.feed.FeedListRequestDto
import ai.maum.kotlin.model.http.dto.feed.FeedListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class FeedList : HandlerType {
    var feedController: FeedController? = null
    var feedListRequestDto: FeedListRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val feedCategoryRepository = fanmeet.feedCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val scrapRepository = fanmeet.scrapRepository
        val likeRepository = fanmeet.likeRepository
        val dto = feedListRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId

        // Input calibration
        dto.from = dto.from ?: Instant.now()
        dto.category = dto.category ?: 0

        // Response init
        val responseBody = FeedListResponseDto()
        responseBody.feeds = mutableListOf()

        // Get feeds as much as requested
        var tailCreated = dto.from!!
        var from = dto.from!!
        while (responseBody.feeds!!.size < dto.get!!) {
            // Retrieve feeds by category
            val feeds = if (dto.category!! > 0) {
                FeedListingHelper(feedRepository).listFeedsWith(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        dto.category!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            } else {
                FeedListingHelper(feedRepository).listFeeds(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            }

            // Fill feed items
            for (feed: Feed in feeds) {
                val feedItem = FeedItemDto()

                // If logged in, skip feed created by blocked user
                feedItem.blocked = false
                userId?.let {
                    val blocks = blockRepository.findByWhoAndActiveIsTrue(userId)
                    for (block in blocks)
                        if (block.whom == feed.who)
                            continue
                }

                // If logged in, check if user liked/scraped the feed
                userId?.let {
                    feedItem.liked = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, feed.id!!, Like.TargetType.USER_FEED)
                    feedItem.scraped = scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, feed.id!!, Scrap.TargetType.USER_FEED)
                }

                // Information about who created the feed
                feedItem.feedOwner = feed.who
                val feedOwner = userRepository.findByIdAndActiveIsTrue(feed.who ?: continue)
                        ?: continue
                feedItem.feedOwnerName = feedOwner.name
                feedItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

                // Membership Badge
                val lastPurchase = fanmeet.userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(feed.who!!, dto.celeb!!)
                lastPurchase?.let {
                    if (it.status == UserMembershipHistory.Status.EXPIRED)
                        return@let
                    feedItem.feedOwnerMembershipBadgeImageUrl = it.membership!!.badgeImageUrl?.mediaUrl()
                }

                // Information about category specified in the feed
                feedItem.category = feed.category
                try {
                    if (feed.category!! == 0L) throw NullPointerException()
                    val category = feedCategoryRepository.findById(feed.category!!)
                    feedItem.categoryName = category.get().name ?: throw NullPointerException()
                } catch (e: NullPointerException) {
                    feedItem.category = 0
                    feedItem.categoryName = ""
                }

                feedItem.feed = feed.id
                feedItem.celeb = feed.celeb
                feedItem.commentCount = feed.commentCount
                feedItem.likeCount = feed.likeCount
                feedItem.scrapCount = feed.scrapCount
                feedItem.feedOwnerMembershipBadgeImageUrl
                feedItem.updated = feed.created
                feedItem.text = feed.text
                feedItem.modifyYn = feed.modifyYn
                val pictures = mutableListOf<String>()
                feed.pictureList?.let {
                    for (picture in feed.pictureList!!)
                        pictures.add(picture.url!!.mediaUrl())
                }

                feedItem.pictures = pictures
                feedItem.video = feed.video?.url?.mediaUrl()
                feedItem.thumbnail = feed.thumbnail?.url?.mediaUrl()
                feedItem.tts = feed.tts?.url?.mediaUrl()
                feedItem.youtube = feed.youtube

                if (feed.created!! < from)
                    from = feed.created!!
                responseBody.feeds!!.add(feedItem)
            }
            if (from < tailCreated)
                tailCreated = from

            // Check if there is no more feeds
            // Avoid infinite loop
            if (feeds.size < dto.get!!)
                break
        }
        responseBody.tailCreated = tailCreated

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedListRequestDto = dto as FeedListRequestDto
    }
}