package ai.maum.kotlin.controller.impl.service.apple

object ConstValue {
    private const val BASE_DIR = "tmp/"
    const val RECEIPT_DIR = BASE_DIR + "receipt/"
    const val RECEIPT_SUFFIX = ".rece"
    const val EXCEPTION_DIR = BASE_DIR + "exception/"
    const val EXCEPTION_SUFFIX = ".exce"
    const val OTHERCASE_DIR = BASE_DIR + "outher_case/"
    const val OTHERCASE_SUFFIX = ".case"
}
