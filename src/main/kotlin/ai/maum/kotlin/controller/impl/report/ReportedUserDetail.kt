package ai.maum.kotlin.controller.impl.report

import ai.maum.kotlin.controller.ReportController
import ai.maum.kotlin.jpa.common.report.Report.ReportType
import ai.maum.kotlin.jpa.common.report.ReportVo
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.report.ReportedUserDetailRequestDto
import ai.maum.kotlin.model.http.dto.report.ReportedUserDetailResponseDto
import ai.maum.kotlin.model.http.dto.report.UserReportedHistoryDetailVo
import ai.maum.kotlin.model.media.mediaUrl
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ReportedUserDetail : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var reportController: ReportController? = null
    var reportedUserDetailRequestDto: ReportedUserDetailRequestDto? = null

    override operator fun invoke(): ResponseType {
        val controller = reportController!!

        val celebId: Long = controller.fanmeet.userInfoFactory.`object`.userId!!
        val userId = reportedUserDetailRequestDto!!.userId

        val userRepository = controller.fanmeet.userRepository
        val commentRepository = controller.fanmeet.commentRepository
        val userFeedRepository = controller.fanmeet.feedRepository
        val reportRepository = controller.fanmeet.reportRepository

        val user = userRepository.findUserById(userId) ?: throw Exception("No such User(userId: $userId) exists")
        val reportList = reportRepository.findAllByWhomAndActiveIsTrue(userId)?.toMutableList()
                ?: return ResponseEntity.ok(
                        ReportedUserDetailResponseDto(
                                userId = userId,
                                userName = user.name!!,
                                profileImageUrl = user.profileImageUrl?.mediaUrl(),
                                reportCount = 0,
                                historyDetailList = null
                        ))
        val comparator = Comparator<ReportVo> { a, b ->
            when {
                (a.what == b.what && a.which == b.which) -> 0
                (a.what < b.what) -> 1
                (a.what == b.what && a.which < b.which) -> 1
                else -> -1
            }
        }
        val reportMap = emptyMap<ReportVo, Long>().toSortedMap(comparator)
        reportList.forEach {
            if (!it.active!!) return@forEach
            val vo = ReportVo(it)
            if (reportMap[vo] == null) reportMap[vo] = 1L
            else reportMap[vo] = reportMap[vo]!!+1
        }

        var reportTotalCount = 0L
        val historyDetailList = mutableListOf<UserReportedHistoryDetailVo>()
        reportMap.forEach {
            var type = ""
            var text = ""
            var objectId = 0L
            when (it.key.what) {
                ReportType.USER_FEED_COMMENT,
                ReportType.CELEB_FEED_COMMENT,
                ReportType.CONTENT_COMMENT -> {
                    type = "comment"
                    val comment = commentRepository.findByIdAndActiveIsTrue(it.key.which) ?: return@forEach
                    if (comment.text != null) text = comment.text!!
                    objectId = comment.id!!
                }
                ReportType.USER_FEED -> {
                    type = "userFeed"
                    val feed = userFeedRepository.findByIdAndActiveIsTrue(it.key.which) ?: return@forEach
                    if (feed.text != null) text = feed.text!!
                    objectId = feed.id!!
                }
//                Report.ReportType.CONTENT -> {
//                    type = "contents"
//                }
//                Report.ReportType.CELEB_FEED -> {
//                    type = "celebFeed"
//                }
                else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
            }
            reportTotalCount += it.value
            historyDetailList.add(UserReportedHistoryDetailVo(
                    objectId = objectId,
                    type = type,
                    reportedDate = it.key.created,
                    text = text,
                    reportCount = it.value
            ))
        }

        return ResponseEntity.ok(ReportedUserDetailResponseDto(
                userId = userId,
                userName = user.name!!,
                profileImageUrl = user.profileImageUrl?.mediaUrl(),
                reportCount = reportTotalCount,
                historyDetailList = historyDetailList
        ))
    }
    override fun setController(controller: Any) {
        reportController = controller as ReportController
    }

    override fun setDto(dto: Any) {
        reportedUserDetailRequestDto = dto as ReportedUserDetailRequestDto
    }
}
