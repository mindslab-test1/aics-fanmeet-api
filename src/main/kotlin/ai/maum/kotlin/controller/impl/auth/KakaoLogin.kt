package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserToken
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.KakaoLoginDto
import ai.maum.kotlin.model.http.dto.auth.KakaoLoginValidationDto
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException

class KakaoLogin(private val authController: AuthController, private val kakaoLoginDto: KakaoLoginDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        val restTemplate = authController.restTemplate

        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer ${kakaoLoginDto.accessToken}")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        val response = restTemplate.exchange("http://kapi.kakao.com/v1/user/access_token_info", HttpMethod.GET, entity, KakaoLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val validationResult = response.body ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        // Check existence in DB and retrieve user id(row id)
        val user = authController.userRepository.findByKakaoUserIdAndActiveIsTrue(validationResult.id ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        val userId: Long = user?.id ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED)

        // Duplication check (deviceId)
        DeviceIdDuplicationHelper(authController, kakaoLoginDto.deviceId!!)()

        // Token registration
        val token = KakaoUserToken()
        token.accessToken = kakaoLoginDto.accessToken!!
        token.kakaoUserId = validationResult.id!!
        token.userId = userId
        token.deviceId = kakaoLoginDto.deviceId!!
        authController.kakaoUserTokenRepository.save(token)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}