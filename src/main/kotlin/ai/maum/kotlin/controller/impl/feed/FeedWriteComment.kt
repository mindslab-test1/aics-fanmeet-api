package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedWriteCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedWriteComment : HandlerType {
    var feedController: FeedController? = null
    var feedWriteCommentRequestDto: FeedWriteCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val commentRepository = fanmeet.commentRepository
        val feedRepository = fanmeet.feedRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = feedWriteCommentRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val feed = feedRepository.findByIdAndActiveIsTrue(dto.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")
        feed.commentCount = feed.commentCount!! + 1

        // Check only if dto.parent is not null
        dto.parent?.let {
            if (!commentRepository.existsByIdAndActiveIsTrue(dto.parent!!)) throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Parent")
        }

        val comment = Comment()
        comment.id // AUTO_INCREMENT
        comment.celeb = dto.celeb
        comment.feed = dto.feed
        comment.who = userId
        comment.parent = dto.parent
        comment.likeCount = 0
        comment.section = Comment.Section.USER_FEED

        // TTS Ownership check
        val tts = dto.tts?.let {
            ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto TTS")
        }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }
        comment.tts = tts

        // Uploading media files
        comment.picture = null
        dto.picture?.let {
            comment.picture = fanmeet.mediaUploader.uploadAsync(dto.picture!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Pictures")
        }

        comment.text = dto.text
        comment.active = true

        feedRepository.save(feed)
        val savedResult = commentRepository.save(comment)
        fanmeet.authorization.comment(savedResult.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedWriteCommentRequestDto = dto as FeedWriteCommentRequestDto
    }
}