package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.*
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageReportListCandidate : HandlerType {
    var celebMyPageController: CelebMyPageController? = null
    var celebMyPageReportListCandidateRequestDto: CelebMyPageReportListCandidateRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val blockRepository = fanmeet.blockRepository
        val reportRepository = fanmeet.reportRepository
        val userRepository = fanmeet.userRepository
        val dto = celebMyPageReportListCandidateRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // Get reported users and exclude already blocked users
        val alreadyBlockedList = blockRepository.findByWhoAndActiveIsTrue(userId)
        val candidateDuplicateList =
                if (dto.reverse!!)
                    reportRepository.findAllByJudgeAndActiveIsTrue(userId)!!.sortedBy { it.updated }
                else
                    reportRepository.findAllByJudgeAndActiveIsTrue(userId)!!.sortedByDescending { it.updated }
        val candidateList = candidateDuplicateList.distinctBy { it.whom }

        val filteredCandidateList = candidateList.map { it.whom }.toMutableList()
        filteredCandidateList.removeAll(alreadyBlockedList.map { it.whom })

        val candidateItemList = mutableListOf<CelebMyPageReportListCandidateItemDto>()
        for (candidate in filteredCandidateList) {
            val candidateItem = CelebMyPageReportListCandidateItemDto()

            val count = reportRepository.countByWhomAndJudgeAndActiveIsTrue(candidate!!, userId)
            val targetUser = userRepository.findByIdAndActiveIsTrue(candidate)
                    ?: continue

            candidateItem.id = targetUser.id
            candidateItem.name = targetUser.name
            candidateItem.profileImageUrl = targetUser.profileImageUrl?.mediaUrl()
            candidateItem.reportAccumulation = count

            candidateItemList.add(candidateItem)
        }

        val responseBody = CelebMyPageReportListCandidateResponseDto()
        responseBody.candidateList = candidateItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        celebMyPageReportListCandidateRequestDto = dto as CelebMyPageReportListCandidateRequestDto
    }
}