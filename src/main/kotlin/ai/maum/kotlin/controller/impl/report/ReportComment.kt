package ai.maum.kotlin.controller.impl.report

import ai.maum.kotlin.controller.ReportController
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.report.Report
import ai.maum.kotlin.jpa.common.report.Report.ReportType
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.report.ReportCommentRequestDto
import ai.maum.kotlin.model.http.dto.report.ReportCommentResponseDto
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ReportComment : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var reportController: ReportController? = null
    var reportCommentRequestDto: ReportCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val controller = reportController!!

        val commentRepository = controller.fanmeet.commentRepository
        val reportRepository = controller.fanmeet.reportRepository

        val userId: Long = controller.fanmeet.userInfoFactory.`object`.userId!!

        val commentId = reportCommentRequestDto?.commentId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val whom = reportCommentRequestDto?.whom ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val reason = reportCommentRequestDto?.reason

        val comment = commentRepository.findByIdAndWhoAndActiveIsTrue(commentId, whom) ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val report = Report()
        val reportType = when (comment.section) {
            Comment.Section.USER_FEED -> ReportType.USER_FEED_COMMENT
            Comment.Section.CELEB_FEED -> ReportType.CELEB_FEED_COMMENT
            Comment.Section.CONTENT -> ReportType.CONTENT_COMMENT
            else -> throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        }

        report.who = userId
        report.whom = whom
        report.which = commentId
        report.what = reportType
        report.judge = comment.celeb
        report.reason = reason

        reportRepository.save(report)

        return ResponseEntity.ok(ReportCommentResponseDto("OK"))
    }

    override fun setController(controller: Any) {
        reportController = controller as ReportController
    }

    override fun setDto(dto: Any) {
        reportCommentRequestDto = dto as ReportCommentRequestDto
    }
}
