package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedCategoryItemDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedListCategoryRequestDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedListCategoryResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedListCategory : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedListCategoryRequestDto: CelebFeedListCategoryRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val repository = fanmeet.celebFeedCategoryRepository
        val dto = celebFeedListCategoryRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val categoryList = repository.findByCelebAndActiveIsTrueOrderByCreatedAsc(dto.celeb!!)

        val response = CelebFeedListCategoryResponseDto()

        val categories = mutableListOf<CelebFeedCategoryItemDto>()
        for (category in categoryList) {
            val item = CelebFeedCategoryItemDto()
            item.id = category.id
            item.name = category.name
            categories.add(item)
        }
        response.categories = categories

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedListCategoryRequestDto = dto as CelebFeedListCategoryRequestDto
    }
}