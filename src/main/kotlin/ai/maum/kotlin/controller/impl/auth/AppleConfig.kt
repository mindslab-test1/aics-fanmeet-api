package ai.maum.kotlin.controller.impl.auth

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.Resource

@Configuration
class AppleConfig(
        @Value("apple/AuthKey_V77W54M5G2.p8")
        val appleKeyFile: Resource
)