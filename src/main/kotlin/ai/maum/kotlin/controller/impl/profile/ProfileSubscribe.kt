package ai.maum.kotlin.controller.impl.profile

import ai.maum.kotlin.controller.ProfileController
import ai.maum.kotlin.jpa.common.subscribe.Subscribe
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.profile.ProfileSubscribeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ProfileSubscribe : HandlerType {
    var profileController: ProfileController? = null
    var profileSubscribeRequestDto: ProfileSubscribeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = profileController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val subscribeRepository = fanmeet.subscribeRepository
        val userRepository = fanmeet.userRepository
        val dto = profileSubscribeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")
        val target = userRepository.findByIdAndActiveIsTrue(dto.whom!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Target")

        // Check if already subscribing
        if (subscribeRepository.existsByWhoAndWhomAndActiveIsTrue(userId, dto.whom!!))
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Subscription Exists")

        // If unsubscription record exists, activate it
        subscribeRepository.findByWhoAndWhomAndActiveIsFalse(userId, dto.whom!!)?.let {
            it.active = true

            target.subscriberCount = target.subscriberCount?.let { return@let target.subscriberCount!! + 1 } ?: 1

            subscribeRepository.save(it)
            userRepository.save(target)

            return ResponseEntity.ok(Unit)
        }

        // New subscription
        val subscribe = Subscribe()
        subscribe.who = userId
        subscribe.whom = dto.whom

        if (subscribe.who == subscribe.whom)
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Self Subscription")

        subscribe.active = true

        target.subscriberCount = target.subscriberCount?.let { return@let target.subscriberCount!! + 1 }
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "SubscriberCount Null")

        subscribeRepository.save(subscribe)
        userRepository.save(target)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        profileController = controller as ProfileController
    }

    override fun setDto(dto: Any) {
        profileSubscribeRequestDto = dto as ProfileSubscribeRequestDto
    }
}