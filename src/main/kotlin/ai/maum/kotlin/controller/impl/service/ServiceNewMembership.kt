package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedDeleteRequestDto
import ai.maum.kotlin.model.http.dto.service.ServiceNewMembershipRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ServiceNewMembership : HandlerType {
    var serviceController: ServiceController? = null
    var serviceNewMembershipRequestDto: ServiceNewMembershipRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = serviceController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = serviceNewMembershipRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        dto.purchaseToken ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "purchaseToken")
        dto.productId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "productId")

        try {
            fanmeet.newSubscriptionHandler.run(
                    userId = userId,
                    purchaseToken = dto.purchaseToken!!,
                    productId = dto.productId!!
            )
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Membership insertion failed; ${e.message}")
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        serviceController = controller as ServiceController
    }

    override fun setDto(dto: Any) {
        serviceNewMembershipRequestDto = dto as ServiceNewMembershipRequestDto
    }
}