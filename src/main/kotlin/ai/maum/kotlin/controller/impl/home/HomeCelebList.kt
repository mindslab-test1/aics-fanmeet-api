package ai.maum.kotlin.controller.impl.home

import ai.maum.kotlin.controller.Fanmeet
import ai.maum.kotlin.controller.HomeController
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.home.HomeCelebListDto
import ai.maum.kotlin.model.http.dto.home.HomeSubscribingCelebDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class HomeCelebList : HandlerType {
    var homeController: HomeController? = null

    private fun getAllCelebList(fanmeet: Fanmeet): MutableList<User> {
        val userRepository = fanmeet.userRepository
        val subscribeRepository = fanmeet.subscribeRepository
        val userId = fanmeet.userInfoFactory.`object`.userId!!
        return userRepository.findAllByActiveIsTrueAndCelebActiveIsTrue()?.toMutableList() ?: mutableListOf()
    }

    private fun getSubscribingCelebList(fanmeet: Fanmeet): MutableList<User> {
        val userRepository = fanmeet.userRepository
        val subscribeRepository = fanmeet.subscribeRepository
        val userId = fanmeet.userInfoFactory.`object`.userId!!
        return subscribeRepository.findAllByWhoAndActiveIsTrue(userId).mapNotNull {
            userRepository.findByIdAndActiveIsTrueAndCelebActiveIsTrue(it.whom!!)
        }.toMutableList()
    }

    override operator fun invoke(): ResponseType {
        val fanmeet = homeController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val membershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val userId = fanmeet.userInfoFactory.`object`.userId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val self = userRepository.findByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Self")

        val allCelebList = getAllCelebList(fanmeet).map {
            HomeSubscribingCelebDto(
                    it.id!!,
                    it.profileImageUrl?.mediaUrl(),
                    it.name!!,
                    membershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(userId, it.id!!, UserMembershipHistory.Status.EXPIRED)
                            ?.membership?.badgeImageUrl?.mediaUrl()
            )
        }.toList()
        val subscribingCelebList = getSubscribingCelebList(fanmeet).map {
            HomeSubscribingCelebDto(
                    it.id!!,
                    it.profileImageUrl?.mediaUrl(),
                    it.name!!,
                    membershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(userId, it.id!!, UserMembershipHistory.Status.EXPIRED)
                            ?.membership?.badgeImageUrl?.mediaUrl()
            )
        }.toMutableList()

        self.celeb?.let {
            subscribingCelebList.removeIf { it.id == userId }
            subscribingCelebList.add(0, HomeSubscribingCelebDto(userId, self.profileImageUrl?.mediaUrl(), self.name!!))
        }

        return ResponseEntity.ok(HomeCelebListDto(
                allCelebList = allCelebList,
                subscribingCelebList = subscribingCelebList
        ))
    }

    override fun setController(controller: Any) {
        homeController = controller as HomeController
    }

    override fun setDto(dto: Any) {
    }
}