package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageScrapBoardListResponse
import ai.maum.kotlin.model.http.dto.mypage.ScrapBoardData
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageScrapBoradList : HandlerType {
    var myPageController: MyPageController? = null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        // does not require dto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")

        val scrapBoardRepository = fanmeet.scrapBoardRepository
        val scrapBoardRawList = scrapBoardRepository.findAllByOwnerIdAndActiveIsTrue(userId)
        val scrapRepository = fanmeet.scrapRepository
        val scrapBoardList = scrapBoardRawList?.map {
            ScrapBoardData(
                    id = it.id,
                    name = it.name,
                    ownerId = it.ownerId,
                    updated = it.updated,
                    feedCount = scrapRepository.countAllByBoardAndActiveIsTrue(it.id!!)

            )
        }?.toList()

        val responseBody = MyPageScrapBoardListResponse()
        responseBody.scrapBoardList = scrapBoardList

        return ResponseEntity.ok(responseBody)
    }
}