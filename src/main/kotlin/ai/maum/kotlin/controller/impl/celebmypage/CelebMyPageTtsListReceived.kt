package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsListReceivedItemDto
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsListReceivedResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageTtsListReceived : HandlerType {
    var celebMyPageController: CelebMyPageController? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsRepository = fanmeet.ttsRepository
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val receivedItemList = mutableListOf<CelebMyPageTtsListReceivedItemDto>()
        val receivedList = ttsRepository.findByCelebAndStatusAndActiveIsTrue(userId, Tts.Status.APPEALED)
        for (tts in receivedList) {
            val subscriber =
                    if (tts.subscriber != null) fanmeet.userRepository.findByIdAndActiveIsTrue(tts.subscriber!!)
                    else if (tts.status == Tts.Status.SELF) fanmeet.userRepository.findByIdAndActiveIsTrue(userId)
                    else continue

            val receivedItem = CelebMyPageTtsListReceivedItemDto()
            receivedItem.created = tts.created
            receivedItem.subscriberName = subscriber?.name ?: "(존재하지 않는 사용자입니다.)"
            receivedItem.text = tts.text
            receivedItem.tts = tts.id
            receivedItem.ttsUrl = tts.url!!.mediaUrl()

            receivedItemList.add(receivedItem)
        }

        val responseBody = CelebMyPageTtsListReceivedResponseDto()
        responseBody.receivedList = receivedItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}