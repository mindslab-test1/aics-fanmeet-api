package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.feed.Feed
import ai.maum.kotlin.jpa.common.feed.FeedFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedWriteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedWrite : HandlerType {
    private var feedController: FeedController? = null
    private var feedWriteRequestDto: FeedWriteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val feedCategoryRepository = fanmeet.feedCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = feedWriteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        var mediaExclusion = 0
        dto.pictures?.let { mediaExclusion++ }
        dto.video?.let { mediaExclusion++ }
        dto.youtube?.let { mediaExclusion++ }
        if (mediaExclusion > 1)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "MediaExclusion")

        // Data range check
        dto.text?.let { if (it.length > 1000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.pictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        if (dto.category!! > 0)
            feedCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
        else dto.category = 0

        // TTS Ownership check
        val tts = dto.tts?.let {
            ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto TTS")
        }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Uploading media files
        var fileNameList = listOf<String>()
        dto.pictures?.let {
            if (dto.pictures!!.isNotEmpty())
                fileNameList = fanmeet.mediaUploader.uploadAsyncFeedImage(dto.pictures!!.toList())
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Pictures")
        }

        var videoName: String? = null
        var thumbnailName: String? = null
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")
            thumbnailName = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            videoName = fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
        }

        // Write feed
        val article = Feed()
        article.who = userId
        article.celeb = dto.celeb!!
        article.commentCount = 0
        article.likeCount = 0
        article.scrapCount = 0
        article.text = dto.text ?: ""
        article.category = dto.category
        article.youtube = dto.youtube
        article.tts = tts
        article.modifyYn = 0
        article.pictureList = mutableListOf()
        if (fileNameList.isNotEmpty()) {
            for (fileName in fileNameList) {
                val feedFile = FeedFile()
                feedFile.url = fileName
                feedFile.what = FeedFile.FileType.IMAGE
                feedFile.who = userId
                article.pictureList!!.add(feedFile)
            }
        }

        videoName?.let {
            val videoFile = FeedFile()
            videoFile.url = videoName
            videoFile.what = FeedFile.FileType.VIDEO
            videoFile.who = userId
            article.video = videoFile
        }
        thumbnailName?.let {
            val thumbnailFile = FeedFile()
            thumbnailFile.url = thumbnailName
            thumbnailFile.what = FeedFile.FileType.IMAGE
            thumbnailFile.who = userId
            article.thumbnail = thumbnailFile
        }

        feedRepository.save(article)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedWriteRequestDto = dto as FeedWriteRequestDto
    }
}