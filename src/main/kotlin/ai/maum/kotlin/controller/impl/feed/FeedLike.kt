package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedLikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedLike : HandlerType {
    var feedController: FeedController? = null
    var feedLikeRequestDto: FeedLikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val feedRepository = fanmeet.feedRepository
        val dto = feedLikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val feed = feedRepository.findByIdAndActiveIsTrue(dto.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.feed!!, Like.TargetType.USER_FEED)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        val unliked = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.feed!!, Like.TargetType.USER_FEED)
        val exists = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.feed!!, Like.TargetType.USER_FEED)

        if (exists) {
            fanmeet.userInfoFactory.`object`.notification = false
            if (unliked != null)
                likeRepository.delete(unliked)
            return ResponseEntity.ok(Unit)
        }

        unliked?.let {
            fanmeet.userInfoFactory.`object`.notification = false

            feed.likeCount = feed.likeCount!! + 1

            unliked.active = true

            feedRepository.save(feed)
            likeRepository.save(unliked)

            return ResponseEntity.ok(Unit)
        }

        if (!exists) {
            feed.likeCount = feed.likeCount!! + 1

            val like = Like()
            like.targetId = dto.feed!!
            like.targetType = Like.TargetType.USER_FEED
            like.userId = userId

            feedRepository.save(feed)
            likeRepository.save(like)

            return ResponseEntity.ok(Unit)
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedLikeRequestDto = dto as FeedLikeRequestDto
    }
}