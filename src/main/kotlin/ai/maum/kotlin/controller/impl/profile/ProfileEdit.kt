package ai.maum.kotlin.controller.impl.profile

import ai.maum.kotlin.controller.ProfileController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.profile.ProfileEditRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ProfileEdit : HandlerType {
    var profileController: ProfileController? = null
    var profileEditRequestDto: ProfileEditRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = profileController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val celebRepository = fanmeet.celebRepository
        val dto = profileEditRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val user = userRepository.findUserByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User")
        val celeb = celebRepository.findByUserAndActiveIsTrue(user)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Celeb")

        // Name duplication check
        dto.name?.let {
            if (dto.name!! == user.name)
                return@let
            userRepository.findByNameAndActiveIsTrue(dto.name!!)
                    ?: return@let

            throw ResponseStatusException(HttpStatus.CONFLICT, "Duplicate Name")
        }

        // Edit introduction
        dto.introductionDescription0?.let { celeb.introductionDescription0 = dto.introductionDescription0 }
        dto.introductionDescription1?.let { celeb.introductionDescription1 = dto.introductionDescription1 }
        dto.introductionDescription2?.let { celeb.introductionDescription2 = dto.introductionDescription2 }
        dto.introductionDescription3?.let { celeb.introductionDescription3 = dto.introductionDescription3 }
        dto.introductionTitle0?.let { celeb.introductionTitle0 = dto.introductionTitle0 }
        dto.introductionTitle1?.let { celeb.introductionTitle1 = dto.introductionTitle1 }
        dto.introductionTitle2?.let { celeb.introductionTitle2 = dto.introductionTitle2 }
        dto.introductionTitle3?.let { celeb.introductionTitle3 = dto.introductionTitle3 }

        dto.name?.let { user.name = dto.name }
        dto.profileImage?.let {
            val fileName = fanmeet.mediaUploader.uploadAsync(dto.profileImage!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "ProfileImage")

            user.profileImageUrl = fileName
        }
        dto.bannerImage?.let {
            val fileName = fanmeet.mediaUploader.uploadAsync(dto.bannerImage!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "BannerImage")

            user.bannerImageUrl = fileName
        }

        userRepository.save(user)
        celebRepository.save(celeb)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        profileController = controller as ProfileController
    }

    override fun setDto(dto: Any) {
        profileEditRequestDto = dto as ProfileEditRequestDto
    }
}