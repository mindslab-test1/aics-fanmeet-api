package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.CategoryItemDto
import ai.maum.kotlin.model.http.dto.feed.FeedCategoryListResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedListCategory : HandlerType {
    var feedController: FeedController? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val repository = fanmeet.feedCategoryRepository

        val categoryList = repository.findByActiveIsTrueOrderByCreatedAsc()

        val response = FeedCategoryListResponseDto()

        val categories = mutableListOf<CategoryItemDto>()
        for (category in categoryList) {
            val item = CategoryItemDto()
            item.id = category.id
            item.name = category.name
            categories.add(item)
        }
        response.categories = categories

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}