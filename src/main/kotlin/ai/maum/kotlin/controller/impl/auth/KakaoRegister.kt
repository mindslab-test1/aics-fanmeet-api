package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.common.interest.UserInterested
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.KakaoLoginValidationDto
import ai.maum.kotlin.model.http.dto.auth.KakaoRegisterDto
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException

class KakaoRegister(private val authController: AuthController, private val kakaoRegisterDto: KakaoRegisterDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        val restTemplate = authController.restTemplate

        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer ${kakaoRegisterDto.accessToken}")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        val response = restTemplate.exchange("http://kapi.kakao.com/v1/user/access_token_info", HttpMethod.GET, entity, KakaoLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val validationResult = response.body ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED)

        val regex = Regex("^[가-힣a-zA-Z0-9@!\\-_*]*$")
        if (!regex.matches(kakaoRegisterDto.name!!)) throw ResponseStatusException(HttpStatus.FORBIDDEN)

        if (kakaoRegisterDto.name!!.length >= 15) throw ResponseStatusException(HttpStatus.NOT_ACCEPTABLE)
        kakaoRegisterDto.hello?.let { if (it.length > 100) throw ResponseStatusException(HttpStatus.I_AM_A_TEAPOT) }

        // Check existence in DB
        var user = authController.userRepository.findByKakaoUserIdAndActiveIsTrue(validationResult.id
                ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        user?.let { throw ResponseStatusException(HttpStatus.CONFLICT) }

        // Check name duplication
        user = authController.userRepository.findUserByNameAndActiveIsTrue(kakaoRegisterDto.name!!)
        user?.let { throw ResponseStatusException(HttpStatus.IM_USED) }

        // Add user
        val register = User()

        kakaoRegisterDto.profileImage?.let { register.profileImageUrl = authController.mediaUploader.uploadAsyncProfileImage(kakaoRegisterDto.profileImage!!) }
        kakaoRegisterDto.bannerImage?.let { register.bannerImageUrl = authController.mediaUploader.uploadAsyncBannerImage(kakaoRegisterDto.bannerImage!!) }

        register.kakaoUserId = validationResult.id!!
        register.name = kakaoRegisterDto.name!!
        register.interestedList = mutableListOf()

        val allInterestIds = authController.interestRepository.findAllByActiveIsTrue().map { it.id!! }
        kakaoRegisterDto.interests?.let {
            for (interestId in kakaoRegisterDto.interests!!) {
                val interest = UserInterested()
                interest.what = interestId

                if (!allInterestIds.contains(interestId))
                    continue

                register.interestedList!!.add(interest)
            }
        }

        authController.userRepository.save(register)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}