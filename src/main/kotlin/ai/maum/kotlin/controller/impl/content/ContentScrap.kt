package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentScrapRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentScrap : HandlerType {
    var contentController: ContentController? = null
    var contentScrapRequestDto: ContentScrapRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val scrapRepository = fanmeet.scrapRepository
        val scrapBoardRepository = fanmeet.scrapBoardRepository
        val dto = contentScrapRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val content = dto.content!!
        val scrapBoard = dto.scrapBoard!!

        val contentEntity = contentRepository.findByIdAndActiveIsTrue(content)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "ContentEntity")

        val celeb = contentEntity.who!!

        if (!scrapBoardRepository.existsByIdAndActiveIsTrue(scrapBoard)!!)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ScrapBoardRepository !Exists!!")

        if (contentRepository.findByIdAndActiveIsTrue(content)!!.who != celeb)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ContentRepository Find!!Celeb")
        contentEntity.scrapCount = contentEntity.scrapCount!! + 1

        val scrap = Scrap()
        scrap.userId = userId
        scrap.celeb = celeb
        scrap.board = dto.scrapBoard
        scrap.targetId = content
        scrap.targetType = Scrap.TargetType.CONTENT

        contentRepository.save(contentEntity)
        val saved = scrapRepository.save(scrap)
        fanmeet.authorization.scrap(saved.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentScrapRequestDto = dto as ContentScrapRequestDto
    }
}