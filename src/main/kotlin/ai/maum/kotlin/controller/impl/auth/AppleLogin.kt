package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.authentication.apple.AppleUserToken
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.*
import com.google.gson.Gson
import io.jsonwebtoken.io.Decoders
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException

@Suppress("UNREACHABLE_CODE")
class AppleLogin(
        private val controller: AuthController,
        private val appleLoginDto: AppleLoginDto,
        private val appleKeyFile: Resource) : HandlerType {

    override fun invoke(): ResponseType {
        //check access token's userid is exist
        val accessTokenPayload = appleLoginDto.accessToken!!.split(".")[1]
        val decodedAccessToken = String(Decoders.BASE64.decode(accessTokenPayload))
        val resultAccessTokenData = Gson().fromJson(decodedAccessToken, AppleIdTokenPayload::class.java)
        val checkForAppleId = resultAccessTokenData.sub
        val checkExistUser = controller.userRepository.findByAppleUserIdAndActiveIsTrue(checkForAppleId
                ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        if (checkExistUser == null) {
            return ResponseEntity.ok(AppleLoginResponseDto(
                    userId = checkExistUser?.id ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED),
                    isCeleb = (checkExistUser.celeb != null),
                    iosRefreshToken = null
            ))
        } else {
            val resultIdTokenData = AppleValidation().getTokenPayload(controller, appleLoginDto.authorizationCode, appleKeyFile)
            val decodedResult = AppleValidation().decodeResponseAppleToken(resultIdTokenData)
            val resultUserAppleId = decodedResult!!.sub

            // Check existence in DB and retrieve user id(row id)
            val user = controller.userRepository.findByAppleUserIdAndActiveIsTrue(resultUserAppleId
                    ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
            val userId = user?.id ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED)

            DeviceIdDuplicationHelper(controller, appleLoginDto.deviceId!!)()

            val appleUserToken = AppleUserToken()
            appleUserToken.accessToken = resultIdTokenData.refresh_token
            appleUserToken.appleUserId = resultUserAppleId;
            appleUserToken.userId = userId
            appleUserToken.deviceId = appleLoginDto.deviceId
            controller.appleUserTokenRepository.save(appleUserToken)

            return ResponseEntity.ok(AppleLoginResponseDto(
                    userId = userId,
                    isCeleb = (user.celeb != null),
                    iosRefreshToken = resultIdTokenData.refresh_token
            ))
        }
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }


}