package ai.maum.kotlin.controller.impl.home

import ai.maum.kotlin.controller.HomeController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.content.CelebRecentActivityVo
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.home.HomeListElementDto
import ai.maum.kotlin.model.http.dto.home.HomeResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity

class GuestHome : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var homeController: HomeController? = null

    var userRepository: UserRepository? = null
    var celebFeedRepository: CelebFeedRepository? = null
    var contentRepository: ContentRepository? = null

    private val otherCelebSet = mutableSetOf<Long>()

    private fun getOtherCelebList(): MutableList<User> {
        val existingCelebSet = mutableSetOf<Long>()
        existingCelebSet.add((-1e18).toLong())
        val otherCelebList = userRepository?.findAllOtherCeleb(existingCelebSet) ?: emptyList()
        otherCelebList.forEach {
            existingCelebSet.add(it.id!!)
            otherCelebSet.add(it.id!!)
        }
        return otherCelebList.toMutableList()
    }

    private fun getRecentActivityList(targetCelebSet: MutableSet<Long>): MutableList<CelebRecentActivityVo> {
        val recentContentVoList = mutableListOf<CelebRecentActivityVo>()
        targetCelebSet.forEach {
            val entity = contentRepository?.findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(it, 0L)
            if (entity != null) {
                val vo = CelebRecentActivityVo(entity)
                recentContentVoList.add(vo)
            }
        }

        val recentCelebFeedVoList = mutableListOf<CelebRecentActivityVo>()
        targetCelebSet.forEach {
            val entity = celebFeedRepository?.findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(it, 0L)
            if (entity != null) {
                val vo = CelebRecentActivityVo(entity)
                recentCelebFeedVoList.add(vo)
            }
        }

        val resultVoList = mutableListOf<CelebRecentActivityVo>()
        recentContentVoList.sortWith(compareBy{ it.who })
        recentCelebFeedVoList.sortWith(compareBy{ it.who })
        var contentIndex = 0
        var feedIndex = 0
        while (contentIndex < recentContentVoList.size || feedIndex < recentCelebFeedVoList.size) {
            if (contentIndex >= recentContentVoList.size) {
                resultVoList.add(recentCelebFeedVoList[feedIndex])
                feedIndex++
                continue
            }
            if (feedIndex >= recentCelebFeedVoList.size) {
                resultVoList.add(recentContentVoList[contentIndex])
                contentIndex++
                continue
            }

            if (recentContentVoList[contentIndex].who < recentCelebFeedVoList[feedIndex].who) {
                resultVoList.add(recentContentVoList[contentIndex])
                contentIndex++
            }
            else if (recentContentVoList[contentIndex].who > recentCelebFeedVoList[feedIndex].who) {
                resultVoList.add(recentCelebFeedVoList[feedIndex])
                feedIndex++
            }
            else {
                if (recentContentVoList[contentIndex].timestamp > recentCelebFeedVoList[feedIndex].timestamp) {
                    resultVoList.add(recentContentVoList[contentIndex])
                }
                else if (recentContentVoList[contentIndex].timestamp < recentCelebFeedVoList[feedIndex].timestamp) {
                    resultVoList.add(recentCelebFeedVoList[feedIndex])
                }
                contentIndex++
                feedIndex++
            }
        }

        return resultVoList
    }

    private fun mergeOtherCelebActivity(
            otherCelebList: MutableList<User>,
            otherCelebActivityVoList: MutableList<CelebRecentActivityVo>
    ): MutableList<HomeListElementDto> {
        val response = mutableListOf<HomeListElementDto>()

        otherCelebList.sortWith(compareBy { it.id })
        otherCelebActivityVoList.sortWith(compareBy { it.who })

        if (otherCelebActivityVoList.isEmpty()) {
            return response
        }

        var activityIndex = 0
        otherCelebList.forEach {
            while (
                    activityIndex < otherCelebActivityVoList.size &&
                    otherCelebActivityVoList[activityIndex].who < it.id!!
            ) activityIndex++
            if (activityIndex >= otherCelebActivityVoList.size) return@forEach
            if (otherCelebActivityVoList[activityIndex].who == it.id) {
                val currentActivity = otherCelebActivityVoList[activityIndex]
                val add = response.add(
                        HomeListElementDto(
                                currentActivity.timestamp,
                                it.id!!,
                                it.name!!,
                                it.profileImageUrl?.mediaUrl(),
                                currentActivity.type,
                                currentActivity.id,
                                currentActivity.text,
                                currentActivity.mediaUrl?.mediaUrl()
                        )
                )
                activityIndex++
            }
        }

        response.sortWith(compareByDescending { it.timestamp })

        return response
    }

    override operator fun invoke(): ResponseType {
        val controller = homeController!!

        userRepository = controller.fanmeet.userRepository
        celebFeedRepository = controller.fanmeet.celebFeedRepository
        contentRepository = controller.fanmeet.contentRepository

        val otherCelebList = getOtherCelebList()
        val otherCelebActivityVoList = getRecentActivityList(otherCelebSet)
        val otherCelebResponse = mergeOtherCelebActivity(otherCelebList, otherCelebActivityVoList)

        val recentActivityResponse = mutableListOf<HomeListElementDto>()
        recentActivityResponse.addAll(otherCelebResponse)

        val response = HomeResponseDto(
                null,
                recentActivityResponse
        )

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        homeController = controller as HomeController
    }

    override fun setDto(dto: Any) {
    }
}
