package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.content.Content
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.fanmeet.privilege.MembershipTierPrivilege
import ai.maum.kotlin.model.http.dto.content.ContentItemDto
import ai.maum.kotlin.model.http.dto.content.ContentListRequestDto
import ai.maum.kotlin.model.http.dto.content.ContentListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class ContentList : HandlerType {
    var contentController: ContentController? = null
    var contentListRequestDto: ContentListRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val contentCategoryRepository = fanmeet.contentCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val scrapRepository = fanmeet.scrapRepository
        val likeRepository = fanmeet.likeRepository
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val dto = contentListRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId

        // Input calibration
        dto.from = dto.from ?: Instant.now()
        dto.category = dto.category ?: 0

        // Response init
        val responseBody = ContentListResponseDto()
        responseBody.feeds = mutableListOf()

        var userLevel = 0L
        userId?.let outerLet@{
            val membership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, dto.celeb!!)
            membership?.let{
                it.purchaseToken ?: it.run {
                    if (membership.expirationDate!! < Instant.now()) {
                        membership.status = UserMembershipHistory.Status.EXPIRED
                        userMembershipHistoryRepository.save(membership)
                    }
                }

                if (membership.status != UserMembershipHistory.Status.NORMAL && membership.status != UserMembershipHistory.Status.TO_BE_EXPIRED)
                    return@outerLet
            }
            userLevel = membership?.membership?.tier ?: 0
        }

        // Get feeds as much as requested
        var tailCreated = dto.from!!
        var from = dto.from!!
        while (responseBody.feeds!!.size < dto.get!!) {
            // Retrieve feeds by category
            val contents = if (dto.category!! > 0) {
                ContentListingHelper(contentRepository).listFeedsWith(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        dto.category!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            } else {
                ContentListingHelper(contentRepository).listFeeds(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            }

            // Fill feed items
            for (content: Content in contents) {
                val contentItem = ContentItemDto()

                // If logged in, skip feed created by blocked user
                contentItem.blocked = false
                userId?.let {
                    val blocks = blockRepository.findByWhoAndActiveIsTrue(userId)
                    for (block in blocks)
                        if (block.whom == content.who)
                            continue
                }

                // If logged in, check if user has access to the content
                var membershipAccess = false
                if (dto.celeb == userId) membershipAccess = true

                if (!membershipAccess) {
                    userId?.let {
                        if (userLevel >= content.accessLevel!!)
                            membershipAccess = true
                    }
                }

                // If logged in, check if user liked/scraped the feed
                userId?.let {
                    contentItem.liked = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, content.id!!, Like.TargetType.CONTENT)
                    contentItem.scraped = scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, content.id!!, Scrap.TargetType.CONTENT)
                }

                // Information about who created the feed
                contentItem.feedOwner = content.who
                val feedOwner = userRepository.findByIdAndActiveIsTrue(content.who ?: continue)
                        ?: continue
                contentItem.feedOwnerName = feedOwner.name
                contentItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

                val accessLevel = content.accessLevel ?: 0
                contentItem.accessLevelBadgeImageUrl = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(content.who!!, accessLevel)?.badgeImageUrl?.mediaUrl()

                // Information about category specified in the feed
                contentItem.category = content.category
                try {
                    if (content.category!! == 0L) throw NullPointerException()
                    val category = contentCategoryRepository.findById(content.category!!)
                    contentItem.categoryName = category.get().name ?: throw NullPointerException()
                } catch (e: NullPointerException) {
                    contentItem.category = 0
                    contentItem.categoryName = ""
                }

                contentItem.content = content.id
                contentItem.title = content.title
                contentItem.viewCount = content.viewCount
                contentItem.commentCount = content.commentCount
                contentItem.likeCount = content.likeCount
                contentItem.scrapCount = content.scrapCount
                contentItem.accessLevelBadgeImageUrl
                contentItem.updated = content.created
                contentItem.restricted = !membershipAccess
                contentItem.modifyYn = content.modifyYn
                contentItem.text = content.text
                contentItem.accessLevel = content.accessLevel
                val pictures = mutableListOf<String>()
                content.pictureList?.let {
                    for (picture in content.pictureList!!) {
                        if (membershipAccess)
                            pictures.add(picture.url!!.mediaUrl())
                        else
                            pictures.add(picture.blurredUrl!!.mediaUrl())
                    }
                }
                contentItem.pictures = pictures

                if (membershipAccess) {
                    contentItem.video = content.video?.url?.mediaUrl()
                    contentItem.thumbnail = content.thumbnail?.url?.mediaUrl()
                    contentItem.tts = content.tts?.url?.mediaUrl()
                    contentItem.youtube = content.youtube
                }

                if (content.created!! < from)
                    from = content.created!!
                responseBody.feeds!!.add(contentItem)
            }
            if (from < tailCreated)
                tailCreated = from

            // Check if there is no more feeds
            // Avoid infinite loop
            if (contents.size < dto.get!!)
                break
        }
        responseBody.tailCreated = tailCreated

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentListRequestDto = dto as ContentListRequestDto
    }
}