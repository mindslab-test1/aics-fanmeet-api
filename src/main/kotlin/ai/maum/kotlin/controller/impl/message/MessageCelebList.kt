package ai.maum.kotlin.controller.impl.message

import ai.maum.kotlin.controller.MessageController
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.message.MessageCelebListItemDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MessageCelebList : HandlerType {
    var messageController: MessageController? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = messageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val subscribeRepository = fanmeet.subscribeRepository
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val blockRepository = fanmeet.blockRepository
        val receivedMessageRepository = fanmeet.receivedMessageRepository

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val subscriptionList = subscribeRepository.findAllByWhoAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "SubscriptionList")
        val blockedByList = blockRepository.findByWhomAndActiveIsTrue(userId).map { it.who }

        val targetCelebList = subscriptionList.map { it.whom }.toMutableList()
        targetCelebList.removeAll { blockedByList.contains(it) }

        val response = mutableListOf<MessageCelebListItemDto>()
        // 메시지 권한 레벨 있는 Top 1개 peek
        for (targetCeleb in targetCelebList) {
            targetCeleb ?: continue
            val membership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, targetCeleb)
            var membershipLevel = 0L
            membership?.let {
                if (it.status == UserMembershipHistory.Status.NORMAL || it.status == UserMembershipHistory.Status.TO_BE_EXPIRED) {
                    membershipLevel = it.membership?.tier ?: 0
                }
            }

            // 셀럽이 메시지 안보냈을수도 있다(nullable)
            val receivedMessage = receivedMessageRepository.findTopByWhomAndWhatWhoAndActiveIsTrueOrderByCreatedDesc(userId, targetCeleb)

            val celeb = userRepository.findByIdAndActiveIsTrueAndCelebActiveIsTrue(targetCeleb)
                    ?: continue

            val item = MessageCelebListItemDto()
            item.celeb = targetCeleb
            item.createdTime = receivedMessage?.created
            item.name = celeb.name
            item.profileImageUrl = celeb.profileImageUrl?.mediaUrl()
            item.text = receivedMessage?.what?.text
            item.isTts = (receivedMessage?.what?.text == null)

            response.add(item)
        }

        val responseBody = MessageCelebListResponseDto()
        responseBody.list = response

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        messageController = controller as MessageController
    }

    override fun setDto(dto: Any) {
    }
}