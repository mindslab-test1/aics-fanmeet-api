package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedLikeCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedLikeComment : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedLikeCommentRequestDto: CelebFeedLikeCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val commentRepository = fanmeet.commentRepository
        val dto = celebFeedLikeCommentRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Comment")
        if (comment.section != Comment.Section.CELEB_FEED)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Comment Section")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.comment!!, Like.TargetType.CELEB_FEED_COMMENT)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        val unliked = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.comment!!, Like.TargetType.CELEB_FEED_COMMENT)
        val exists = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.comment!!, Like.TargetType.CELEB_FEED_COMMENT)

        if (exists) {
            fanmeet.userInfoFactory.`object`.notification = false
            if (unliked != null)
                likeRepository.delete(unliked)
            return ResponseEntity.ok(Unit)
        }

        unliked?.let {
            fanmeet.userInfoFactory.`object`.notification = false

            comment.likeCount = comment.likeCount!! + 1

            unliked.active = true

            commentRepository.save(comment)
            likeRepository.save(unliked)

            return ResponseEntity.ok(Unit)
        }

        if (!exists) {
            comment.likeCount = comment.likeCount!! + 1

            val like = Like()

            like.targetId = dto.comment
            like.targetType = Like.TargetType.CELEB_FEED_COMMENT
            like.userId = userId

            commentRepository.save(comment)
            likeRepository.save(like)

            return ResponseEntity.ok(Unit)
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedLikeCommentRequestDto = dto as CelebFeedLikeCommentRequestDto
    }
}