package ai.maum.kotlin.controller.impl.profile

import ai.maum.kotlin.controller.ProfileController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.profile.ProfileViewRequestDto
import ai.maum.kotlin.model.http.dto.profile.ProfileViewResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ProfileView : HandlerType {
    var profileController: ProfileController? = null
    var profileViewRequestDto: ProfileViewRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = profileController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = profileViewRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val responseBody = ProfileViewResponseDto()

        val callerId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // 일단 셀럽만 profile view가 가능한 것으로
        val user = fanmeet.userRepository.findByIdAndActiveIsTrue(dto.user!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User")
        val celeb = fanmeet.celebRepository.findByUserAndActiveIsTrue(user)
                ?: throw ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, "Celeb") // 지금은 셀럽만 되므로
        val isSubscribing = fanmeet.subscribeRepository.existsByWhoAndWhomAndActiveIsTrue(callerId, dto.user!!)
        val subscriberCount = fanmeet.subscribeRepository.countByWhomAndActiveIsTrue(dto.user!!)
        val feedCount = fanmeet.feedRepository.countByCelebAndActiveIsTrue(dto.user!!)
        val activeInterests = fanmeet.activeInterestRepository.findAllByWhoAndActiveIsTrue(dto.user!!)
        val introduction = mutableMapOf<String, String>()

        celeb.introductionTitle0?.let {
            introduction.put(celeb.introductionTitle0!!, celeb.introductionDescription0 ?: "")
        }
        celeb.introductionTitle1?.let {
            introduction.put(celeb.introductionTitle1!!, celeb.introductionDescription1 ?: "")
        }
        celeb.introductionTitle2?.let {
            introduction.put(celeb.introductionTitle2!!, celeb.introductionDescription2 ?: "")
        }
        celeb.introductionTitle3?.let {
            introduction.put(celeb.introductionTitle3!!, celeb.introductionDescription3 ?: "")
        }

        val sns = mutableMapOf<String, String>()
        celeb.instagramUrl?.let { sns.put("instagram", celeb.instagramUrl!!) }
        celeb.facebookUrl?.let { sns.put("facebook", celeb.facebookUrl!!) }
        celeb.lineUrl?.let { sns.put("line", celeb.lineUrl!!) }
        celeb.twitterUrl?.let { sns.put("twitter", celeb.twitterUrl!!) }

        responseBody.hello = celeb.hello
        responseBody.activeInterests = activeInterests.map { it.what!! }
        responseBody.feedCount = feedCount
        responseBody.subscriberCount = subscriberCount
        responseBody.introduction = introduction
        responseBody.isCeleb = celeb.active
        responseBody.isSubscribing = isSubscribing
        responseBody.name = user.name
        responseBody.profileImageUrl = user.profileImageUrl?.mediaUrl()
        responseBody.bannerImageUrl = user.bannerImageUrl?.mediaUrl()
        responseBody.sns = sns

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        profileController = controller as ProfileController
    }

    override fun setDto(dto: Any) {
        profileViewRequestDto = dto as ProfileViewRequestDto
    }
}