package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsListAllResponse
import ai.maum.kotlin.model.http.dto.mypage.RequestedTtsData
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageTtsListAll : HandlerType{
    var myPageController: MyPageController? = null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
//        myPageTtsListAllDto = dto as MyPageTtsListAllDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val ttsListRaw = fanmeet.ttsRepository.findBySubscriberAndActiveIsTrue(userId)
        val ttsListAll = ttsListRaw?.map {
            val celeb = fanmeet.userRepository.findUserById(it.celeb!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "no celeb")
            RequestedTtsData(
                    id = it.id,
                    celebId = it.celeb,
                    celebName = celeb.name,
                    text = it.text,
                    url = it.url!!.mediaUrl(),
                    previewUrl = it.previewUrl!!.mediaUrl(),
                    status = it.status,
                    timestamp = it.updated
            )
        }?.toList()

        val responseBody = MyPageTtsListAllResponse()
        responseBody.ttsListAll = ttsListAll

        return ResponseEntity.ok(responseBody)
    }
}