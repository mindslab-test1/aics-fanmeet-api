package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedUnlikeCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedUnlikeComment : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedUnlikeCommentRequestDto: CelebFeedUnlikeCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val commentRepository = fanmeet.commentRepository
        val dto = celebFeedUnlikeCommentRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Comment")
        if (comment.section != Comment.Section.CELEB_FEED)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Comment Section")

        val like = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.comment!!, Like.TargetType.CELEB_FEED_COMMENT)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Like")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.comment!!, Like.TargetType.CELEB_FEED_COMMENT)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        comment.likeCount = comment.likeCount!! - 1
        like.active = false

        commentRepository.save(comment)
        likeRepository.save(like)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedUnlikeCommentRequestDto = dto as CelebFeedUnlikeCommentRequestDto
    }
}