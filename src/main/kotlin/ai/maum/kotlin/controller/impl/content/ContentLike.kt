package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentLikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentLike : HandlerType {
    var contentController: ContentController? = null
    var contentLikeRequestDto: ContentLikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val contentRepository = fanmeet.contentRepository
        val dto = contentLikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val celebFeed = contentRepository.findByIdAndActiveIsTrue(dto.content!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Content")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.content!!, Like.TargetType.CONTENT)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        val unliked = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.content!!, Like.TargetType.CONTENT)
        val exists = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.content!!, Like.TargetType.CONTENT)

        if (exists) {
            fanmeet.userInfoFactory.`object`.notification = false
            if (unliked != null)
                likeRepository.delete(unliked)
            return ResponseEntity.ok(Unit)
        }

        unliked?.let {
            fanmeet.userInfoFactory.`object`.notification = false

            celebFeed.likeCount = celebFeed.likeCount!! + 1

            unliked.active = true

            contentRepository.save(celebFeed)
            likeRepository.save(unliked)

            return ResponseEntity.ok(Unit)
        }

        if (!exists) {
            celebFeed.likeCount = celebFeed.likeCount!! + 1

            val like = Like()
            like.targetId = dto.content!!
            like.targetType = Like.TargetType.CONTENT
            like.userId = userId

            contentRepository.save(celebFeed)
            likeRepository.save(like)

            return ResponseEntity.ok(Unit)
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentLikeRequestDto = dto as ContentLikeRequestDto
    }
}