package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController

class DeviceIdDuplicationHelper(val controller: AuthController, val deviceId: String) : () -> Unit {
    override fun invoke() {
        val googleRepository = controller.googleUserTokenRepository
        val kakaoRepository = controller.kakaoUserTokenRepository
        val naverRepository = controller.naverUserTokenRepository
        val appleRepository = controller.appleUserTokenRepository

        val googleDeviceDuplicates = googleRepository.findByDeviceId(deviceId) ?: emptyList()
        val kakaoDeviceDuplicates = kakaoRepository.findByDeviceId(deviceId) ?: emptyList()
        val naverDeviceDuplicates = naverRepository.findByDeviceId(deviceId) ?: emptyList()
        val appleDeviceDuplicates = appleRepository.findByDeviceId(deviceId) ?: emptyList()

        if (googleDeviceDuplicates.isNotEmpty())
            for (token in googleDeviceDuplicates) googleRepository.deleteByDeviceId(token.deviceId!!)
        if (kakaoDeviceDuplicates.isNotEmpty())
            for (token in kakaoDeviceDuplicates) kakaoRepository.deleteByDeviceId(token.deviceId!!)
        if (naverDeviceDuplicates.isNotEmpty())
            for (token in naverDeviceDuplicates) naverRepository.deleteByDeviceId(token.deviceId!!)
        if(appleDeviceDuplicates.isNotEmpty())
            for (token in appleDeviceDuplicates) appleRepository.deleteByDeviceId(token.deviceId!!)
    }
}