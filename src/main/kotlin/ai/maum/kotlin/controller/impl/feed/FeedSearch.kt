package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedSearchRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedSearch : HandlerType {
    var feedController: FeedController? = null
    var feedSearchRequestDto: FeedSearchRequestDto? = null

    override operator fun invoke(): ResponseType {
        TODO("Future functionality")
        var feedRepository = feedController?.fanmeet?.feedRepository ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        val dto = feedSearchRequestDto ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        dto.celeb ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val keyword = dto.keyword ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)


        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedSearchRequestDto = dto as FeedSearchRequestDto
    }
}