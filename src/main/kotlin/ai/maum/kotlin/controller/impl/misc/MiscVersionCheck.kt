package ai.maum.kotlin.controller.impl.misc

import ai.maum.kotlin.controller.MiscController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.misc.MiscVersionDto
import org.springframework.http.ResponseEntity

class MiscVersionCheck : HandlerType {
    var miscController: MiscController? = null

    var versionString: String = "1.1.10"

    override fun setController(controller: Any) {
        miscController = controller as MiscController
    }

    override fun setDto(dto: Any) {

    }

    override fun invoke(): ResponseType {
        val dto = MiscVersionDto()
        dto.version = versionString
        return ResponseEntity.ok(dto)
    }
}