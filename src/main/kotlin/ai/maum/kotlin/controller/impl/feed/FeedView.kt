package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.block.Block
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.*
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedView : HandlerType {
    private var feedController: FeedController? = null
    private var feedViewRequestDto: FeedViewRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val feedCategoryRepository = fanmeet.feedCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val likeRepository = fanmeet.likeRepository
        val scrapRepository = fanmeet.scrapRepository
        val commentRepository = fanmeet.commentRepository

        val dto = feedViewRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId // null allowed
        val signedIn = (userId != null)

        val feed = feedRepository.findByIdAndActiveIsTrue(dto.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        var blocks : List<Block> = listOf()
        // If logged in, check if the user blocked feed writer
        if (signedIn) {
            blocks = blockRepository.findByWhoAndActiveIsTrue(userId!!)
            for (block in blocks)
                if (block.whom == feed.who)
                    throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Block") // User blocked feed writer
        }

        /* Filling response body */
        /* Feed info */
        val responseBody = FeedViewResponseDto()
        val feedItem = FeedItemDto()

        feedItem.blocked = false
        feedItem.feed = feed.id
        feedItem.celeb = feed.celeb
        feedItem.feedOwner = feed.who

        // Feed writer info
        val feedOwner = userRepository.findByIdAndActiveIsTrue(feed.who!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "FeedOwner")
        feedItem.feedOwnerName = feedOwner.name
        feedItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

        val lastPurchased =fanmeet.userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(feed.who!!, feed.celeb!!)
        lastPurchased?.let{
            if (it.status == UserMembershipHistory.Status.EXPIRED)
                return@let
            feedItem.feedOwnerMembershipBadgeImageUrl = it.membership!!.badgeImageUrl?.mediaUrl()
        }

        // Category info
        val category = feedCategoryRepository.findByIdAndActiveIsTrue(feed.category!!)
        feedItem.category = feed.category!!
        feedItem.categoryName = category?.name // nullable

        // Like/scrap info
        feedItem.likeCount = feed.likeCount
        feedItem.scrapCount = feed.scrapCount
        feedItem.commentCount = feed.commentCount
        feedItem.liked = if (signedIn) likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, feed.id!!, Like.TargetType.USER_FEED) else false
        feedItem.scraped = if (signedIn) scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, feed.id!!, Scrap.TargetType.USER_FEED) else false

        // Media info
        feedItem.pictures = mutableListOf()
        feed.pictureList?.let {
            for (picture in feed.pictureList!!) {
                feedItem.pictures!!.add(picture.url!!.mediaUrl())
            }
        }
        feedItem.video = feed.video?.url?.mediaUrl()
        feedItem.thumbnail = feed.thumbnail?.url?.mediaUrl()
        feedItem.tts = feed.tts?.url?.mediaUrl()
        feedItem.youtube = feed.youtube

        // Others
        feedItem.text = feed.text
        feedItem.updated = feed.created
        feedItem.modifyYn = feed.modifyYn

        /* Comment info */
        // Active may not be true since nested comments can be active and should be shown
        val comments = commentRepository.findByFeedAndSectionOrderByCreatedDesc(feed.id!!, Comment.Section.USER_FEED) ?: listOf()
        val commentItemList = mutableListOf<CommentItemDto>()
        val offspringItemList = mutableMapOf<Long, MutableList<CommentItemDto>>()

        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        for (comment in comments) {
            val commentItem = CommentItemDto()

            commentItem.id = comment.id

            commentItem.deleted = false
            commentItem.blocked = false

            val commentOwner = userRepository.findByIdAndActiveIsTrue(comment.who!!)

            if (comment.active == false || commentOwner == null) {
                commentItem.deleted = true
                continue
            }
            val block = blocks.find { it.whom == comment.who }
            if (block != null) {
                commentItem.blocked = true
                continue
            }

            commentItem.commentOwner = comment.who!!
            commentItem.commentOwnerName = commentOwner.name
            commentItem.commentOwnerProfileImage = commentOwner.profileImageUrl?.mediaUrl()
            commentItem.badgeUrl = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                    commentOwner.id!!,
                    feed.who!!,
                    UserMembershipHistory.Status.EXPIRED
            )?.membership?.badgeImageUrl?.mediaUrl()
            commentItem.text = comment.text
            commentItem.likeCount = comment.likeCount
            commentItem.picture = comment.picture?.mediaUrl()
            commentItem.tts = comment.tts?.url?.mediaUrl()
            commentItem.updated = comment.created
            commentItem.parent = comment.parent
            commentItem.offspring = mutableListOf()
            commentItem.liked = if (signedIn) likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, comment.id!!, Like.TargetType.USER_FEED_COMMENT) else false
            commentItem.modifyYn = comment.modifyYn
            if (commentItem.parent == null)
                commentItemList.add(commentItem)
            else {
                if (!offspringItemList.contains(commentItem.parent!!))
                    offspringItemList[commentItem.parent!!] = mutableListOf()
                offspringItemList[commentItem.parent!!]!!.add(commentItem)
            }
        }

        for (commentItem in commentItemList)
            if (offspringItemList.contains(commentItem.id!!))
                commentItem.offspring!!.addAll(offspringItemList[commentItem.id!!]!!).also { commentItem.offspring!!.sortBy { it.updated }}

        responseBody.feedItem = feedItem
        responseBody.comments = commentItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedViewRequestDto = dto as FeedViewRequestDto
    }
}