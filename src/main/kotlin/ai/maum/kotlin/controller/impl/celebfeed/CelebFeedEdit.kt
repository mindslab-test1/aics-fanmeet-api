package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedFile
import ai.maum.kotlin.jpa.common.content.ContentFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedEditRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedEdit : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedEditRequestDto: CelebFeedEditRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val celebFeedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = celebFeedEditRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val removedList = dto.removedPictures?.replace(" ","")?.removeSurrounding("[", "]")?.split(",")?.map { it.toLong() }
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // Data range check
        dto.text?.let { if (it.length > 10000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.addedPictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        dto.category?.let {
            if (dto.category!! > 0)
                celebFeedCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
            else dto.category = null
        }

        // Access level validity check
        dto.accessLevel?.let {
            if (it <= 0)
                return@let

            val minimumMembership = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(userId, it)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        }

        // TTS Ownership check
        val tts = dto.tts?.let { ttsRepository.findByIdAndActiveIsTrue(dto.tts!!) }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Retrieve Feed
        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(celebFeedEditRequestDto!!.celebFeed!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "CelebFeed")

        // Picture Count & Index Check
        var pictureCount = celebFeed.pictureList?.size ?: 0
        removedList?.let {
            if (removedList.size > pictureCount)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto RemovedPictures Size")
            for (index in removedList)
                if (index >= celebFeed.pictureList!!.size)
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Feed PictureList Size")
            pictureCount -= removedList.size
        }
        dto.addedPictures?.let {
            pictureCount += dto.addedPictures!!.size
            if (pictureCount > 5)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "PictureCount")
        }

        val updatedPictureList = celebFeed.pictureList?.toMutableList() ?: mutableListOf()
        // Remove Pictures
        removedList?.let {
            val descendingOrder = removedList.sortedDescending()
            for (index in descendingOrder) {
                updatedPictureList.removeAt(index.toInt())
            }
        }

        // Picture Upload
        dto.addedPictures?.let {
            if (dto.addedPictures!!.isEmpty())
                return@let
            val uploadedFiles = celebFeedController!!.fanmeet.mediaUploader.uploadAsyncFeedImage(dto.addedPictures!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "UploadedFiles")
            for (fileName in uploadedFiles) {
                val celebFeedFile = CelebFeedFile()
                celebFeedFile.url = fileName
                celebFeedFile.what = CelebFeedFile.FileType.IMAGE
                celebFeedFile.who = userId
                updatedPictureList.add(celebFeedFile)
            }
        }

        // Apply Updated Picture List
        celebFeed.pictureList = updatedPictureList

        // Video Upload & Apply Updated
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")

            val thumbnailFile = CelebFeedFile()
            thumbnailFile.url = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            thumbnailFile.what = CelebFeedFile.FileType.IMAGE
            celebFeed.thumbnail = thumbnailFile

            val celebFeedFile = CelebFeedFile()
            celebFeedFile.url = celebFeedController!!.fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
            celebFeedFile.what = CelebFeedFile.FileType.VIDEO
            celebFeed.video = celebFeedFile
        }

        dto.category?.let { celebFeed.category = dto.category }
        dto.text?.let { celebFeed.text = dto.text }
        dto.youtube?.let { celebFeed.youtube = dto.youtube }
        dto.tts?.let { celebFeed.tts = tts }
        dto.accessLevel?.let { celebFeed.accessLevel = dto.accessLevel }
        celebFeed.modifyYn = 1
        celebFeedRepository.save(celebFeed)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedEditRequestDto = dto as CelebFeedEditRequestDto
    }
}