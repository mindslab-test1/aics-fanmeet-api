package ai.maum.kotlin.controller.impl.report

import ai.maum.kotlin.controller.ReportController
import ai.maum.kotlin.jpa.common.report.Report.ReportType
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.report.ReportedUserListElementVo
import ai.maum.kotlin.model.http.dto.report.ReportedUserListResponseDto
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ReportedUserList : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var reportController: ReportController? = null

    override operator fun invoke() : ResponseType {
        val controller = reportController!!

        val celebId: Long = controller.fanmeet.userInfoFactory.`object`.userId!!

        val userRepository = controller.fanmeet.userRepository
        val userFeedRepository = controller.fanmeet.feedRepository
        val commentRepository = controller.fanmeet.commentRepository
        val reportRepository = controller.fanmeet.reportRepository

        val reportList = reportRepository.findAllByJudgeAndActiveIsTrue(celebId)
                ?: return ResponseEntity.ok(ReportedUserListResponseDto(userList = null))
        val comparator = Comparator<Long> { a, b ->
            when {
                (a == b) -> 0
                (a < b) -> 1
                else -> -1
            }
        }
        val reportMap = emptyMap<Long, ReportedUserListElementVo>().toSortedMap(comparator)
        reportList.forEach {
            if (!it.active!!) return@forEach
            if (reportMap[it.whom] == null) {
                val user = userRepository.findUserByIdAndActiveIsTrue(it.whom!!) ?: return@forEach
                reportMap[it.whom] = ReportedUserListElementVo(
                        userId = it.whom!!,
                        userName = user.name!!,
                        profileImageUrl = user.profileImageUrl,
                        reportCount = 0L
                )
            }
            when (it.what) {
                ReportType.USER_FEED -> {
                    val exists = userFeedRepository.existsByIdAndActiveIsTrue(it.which!!)
                    if (exists) reportMap[it.whom]!!.reportCount++
                }
                ReportType.USER_FEED_COMMENT,
                ReportType.CELEB_FEED_COMMENT,
                ReportType.CONTENT_COMMENT -> {
                    val exists = commentRepository.existsByIdAndActiveIsTrue(it.which!!)
                    if (exists) reportMap[it.whom]!!.reportCount++
                }
//                ReportType.CELEB_FEED -> TODO()
//                ReportType.CONTENT -> TODO()
                else -> throw ResponseStatusException(HttpStatus.BAD_REQUEST)
            }
        }
        reportMap.forEach {
            if (it.value.reportCount == 0L) {
                reportMap.remove(it.key)
            }
        }

        return ResponseEntity.ok(ReportedUserListResponseDto(
                userList = reportMap.values.toMutableList()
        ))
    }

    override fun setController(controller: Any) {
        reportController = controller as ReportController
    }

    override fun setDto(dto: Any) {
    }
}
