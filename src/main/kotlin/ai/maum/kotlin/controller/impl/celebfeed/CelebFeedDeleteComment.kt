package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedDeleteCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedDeleteComment : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedDeleteCommentRequestDto: CelebFeedDeleteCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val commentRepository = fanmeet.commentRepository
        val celebFeedRepository = fanmeet.celebFeedRepository
        val dto = celebFeedDeleteCommentRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Comment")
        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(comment.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeed")

        celebFeed.commentCount = celebFeed.commentCount!! - 1
        comment.active = false

        celebFeedRepository.save(celebFeed)
        commentRepository.save(comment)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedDeleteCommentRequestDto = dto as CelebFeedDeleteCommentRequestDto
    }
}