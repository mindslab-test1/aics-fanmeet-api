package ai.maum.kotlin.controller.impl.service.apple

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.model.http.dto.service.*
import ai.maum.kotlin.mybatis.mapper.IosReceiptMapper
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import javax.servlet.http.HttpServletRequest

@Service
class AppleServiceMembership(val mapper: IosReceiptMapper, private val membershipRepository: MembershipRepository) {

    private val logger: Logger = LoggerFactory.getLogger(AppleServiceMembership::class.java)

    @Value("\${apple.purchase.verify.sandbox.url}")
    private val sandboxVerifyUrl: String? = null

    @Value("\${apple.purchase.verify.prod.url}")
    private val prodVerifyUrl: String? = null

    private val appSharePassword: String? = "ace74decb0eb4fbfb5725d4c7e485f15"
    private val gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()
    var serviceController: ServiceController? = null
    val fanmeet = serviceController?.fanmeet


    @Throws(AppleReceiptException::class)
    fun verifyReceipt(params: IosReceiptVerifyParams): Int {
        val result: Boolean
        var receiptFilePath: String? = null

        val response: IosReceiptVerifyResponse? = verifyIosServer(params)
        result = response?.status == 0

        if (!result) {
            logger.error((response!!.status.toString()))
            logger.error(java.lang.String.format("%s, %s", "Verify Fail", response.toString()))
            return response.status!!
        }
        val lastPaid: IosNotiReceiptInfo = getLastPaid(response!!) ?: return 0

        if (!params.receiptData.equals(response.latestReceipt)) {
            try {
                receiptFilePath = writeDataFile(ConstValue.RECEIPT_DIR, ConstValue.RECEIPT_SUFFIX, params.userId.toString() + "", response.latestReceipt)
            } catch (exception: Exception) {
                println("${exception.printStackTrace()}")
                logger.error("Make ReceiptFile Error : " + params.userId)
                throw Exception(exception)
            }
        }
        val isSubscribingAppleId: Boolean = checkAppleSubscribing(lastPaid.originalTransactionId!!)
        if (!isSubscribingAppleId) {
            return 204
        }
        val purchaseId = selectOriginalId(params, lastPaid)
        if (purchaseId == null) {
            insertOriginalId(params, lastPaid, receiptFilePath)
        }
        return 0
    }

    @Throws(AppleReceiptException::class)
    fun subscribeNoti(request: HttpServletRequest) {
        val builder = StringBuilder()
        var reader: BufferedReader? = null
        try {
            val inputStream: InputStream? = request.inputStream
            if (inputStream != null) {
                reader = BufferedReader(InputStreamReader(inputStream))
                val charBuffer = CharArray(128)
                var byteRead = -1
                while (reader.read(charBuffer).also { byteRead = it } > 0) {
                    builder.append(charBuffer, 0, byteRead)
                }
            }
        } catch (exception: IOException) {
            logger.error("Alert Request Reader IOException")
            throw AppleReceiptException(exception)
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (exception: IOException) {
                    logger.error("Alert Request Reader IOException")
                    throw AppleReceiptException(exception)
                }
            }
        }
        try {
            val date = Date()
            val format = SimpleDateFormat("yyyy-MM-dd_HHmmss_SSS")
            writeDataFile("request/", ".tmp", format.format(date), builder.toString())
        } catch (exception: IOException) {
            exception.printStackTrace()
        }
        val response: IosNotificationResponse = gson.fromJson(builder.toString(), IosNotificationResponse::class.java)
        val notificationType: String = response.notificationType!!
        val purchaseData: IosNotiReceiptInfo = getLastPurchaseInfo(response)!!
        purchaseData.notificationType = response.notificationType
        purchaseData.autoRenewStatus = response.autoRenewStatus
        logger.info(notificationType)

        when (notificationType) {
            "CANCEL" -> {
            }
            "DID_CHANGE_RENEWAL_STATUS" -> checkChangedStatus(purchaseData)
            "DID_FAIL_TO_RENEW" -> {
            }
            "DID_RECOVER", "DID_RENEW" ->                 // 현재는 플로우 상, 구독상태 확인 프로세스랑 다를 게 없어서 동일한 플로우로 진행 예정
                checkChangedStatus(purchaseData)
            "INITIAL_BUY", "INTERACTIVE_RENEWAL" -> {
                checkChangedStatus(purchaseData)
                insertOtherCaseNotification(response)
            }
            "PRICE_INCREASE_CONSENT", "DID_CHANGE_RENEWAL_PREF", "REFUND" -> insertOtherCaseNotification(response)
            else -> insertOtherCaseNotification(response)
        }
    }


    @Throws(AppleReceiptException::class)
    fun completePurchase(params: IosReceiptVerifyParams) {
        logger.info("Complete_Purchase invoked")
        var receiptFilePath: String? = null
        val response: IosReceiptVerifyResponse?
        response = verifyIosServer(params)
        val result = response.status == 0

        if (!result) {
            logger.error(response.status.toString() + "")
            logger.error(java.lang.String.format("%s, %s", "CompletePurchase Connection Fail", response.toString()))
            throw AppleReceiptException("CompletePurchase Connection Fail")
        }

        val lastPaid = getLastPaid(response)

        if (!params.receiptData.equals(response.latestReceipt)) {
            receiptFilePath = try {
                writeDataFile(ConstValue.RECEIPT_DIR, ConstValue.RECEIPT_SUFFIX, params.userId.toString() + "", response.latestReceipt)
            } catch (exception: IOException) {
                logger.error("Make ReceiptFile Error : " + params.userId)
                throw AppleReceiptException(exception)
            }
        }
        val purchaseId: IosPurchaseIdDto? = selectOriginalId(params, lastPaid!!)
        if (purchaseId == null) {
            insertOriginalId(params, lastPaid, receiptFilePath)
        }
        insertIosShoppingHistory(response)
    }

    @Throws(AppleReceiptException::class)
    private fun checkChangedStatus(response: IosNotiReceiptInfo): Boolean {
//        val purchaseData: IosNotiReceiptInfo = getLastPurchaseInfo(response)!!
        val orderId: String?
        try {
            orderId = response.webOrderLineItemId
        } catch (exception: NullPointerException) {
            throw AppleReceiptException("WebOrderID NPE")
        }
        insertIosShoppingHistory(response)
        return true
    }

    @Throws(AppleReceiptException::class)
    private fun getLastPurchaseInfo(response: IosNotificationResponse): IosNotiReceiptInfo? {
        val latestReceipts: List<IosNotiReceiptInfo> = response.unifiedReceipt!!.latestReceiptInfo!!
        if (latestReceipts.size == 0) {
            logger.error("Not Include Receipts")
            val date = Date()
            val dirFormat = SimpleDateFormat("yyyy/MM/dd/HH")
            val fileFormat = SimpleDateFormat("MM-ss")
            val exceptionDir: String = ConstValue.EXCEPTION_DIR.toString() + dirFormat.format(date)
            try {
                writeDataFile(exceptionDir, ConstValue.EXCEPTION_SUFFIX, fileFormat.format(date), response.toString())
            } catch (exception: IOException) {
                throw AppleReceiptException("File Write Error")
            }
            return null
        }
        val lastOne = latestReceipts[0]
        logger.info(lastOne.toString())
        val orgTranId: String = lastOne.originalTransactionId!!
        val purchaseIds: List<IosPurchaseIdDto> = mapper!!.selectIosPurchaseIdByOrgTranId(IosPurchaseIdDto(orgTranId = orgTranId))
        if (purchaseIds.size > 1) {
            logger.warn(String.format("Over Size orgTranId (%d) : %s", purchaseIds.size, orgTranId))
        }
        if (purchaseIds.size != 0) {
            lastOne.userId = purchaseIds[0].userId
        }
        return lastOne
    }

    @Throws(AppleReceiptException::class)
    private fun insertOtherCaseNotification(response: IosNotificationResponse) {
        val date = Date()
        val dirFormat = SimpleDateFormat("/yyyy-MM/dd")
        val fileFormat = SimpleDateFormat("HH-mm-ss-SSS")
        try {
            writeDataFile(ConstValue.OTHERCASE_DIR + dirFormat.format(date), ConstValue.OTHERCASE_SUFFIX, fileFormat.format(date), response.toString())
        } catch (exception: IOException) {
            logger.error("OtherCase Write File Exception : " + exception.message, exception)
            throw AppleReceiptException("OtherCase Write File Exception")
        }
    }

    @Throws(IOException::class)
    private fun writeDataFile(dir: String, suffix: String, fileName: String, fileContext: String): String? {
        val path = Paths.get(dir)
        Files.createDirectories(path)
        val fileDir = dir + fileName + suffix
        val filepath = Paths.get(fileDir)
        if (!Files.exists(filepath)) {
            Files.createFile(filepath)
        }
        Files.write(filepath, fileContext.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
        return fileDir
    }


    @Throws(AppleReceiptException::class)
    private fun insertPurchaseData(purchaseData: IosNotiReceiptInfo?) {
        if (purchaseData == null) {
            return
        }
        val historyData: GoodsShoppingHistoryDto = parseHistoryData(purchaseData)
        if (mapper.countMembershipHistoryByWebOrderId(historyData.webOrderId, historyData.expirationDate.toString()) > 0) {
            logger.warn("Already Inserted WebOrderId : " + historyData.webOrderId + "\n expirationDate : "+ historyData.expirationDate)
            return
        }
        if (mapper.insertMembershipHistory(historyData) != 1) {
            throw AppleReceiptException("Data Insert Error")
        }
//        else {
//            if (mapper.updateMembershipHistoryStatus(historyData.orgTranId) == 0) {
//                logger.info("Previous data's status \"NOT\" updated // orgTranId : " + historyData.orgTranId)
//            } else {
//                logger.info("Previous data's status updated // orgTranId : " + historyData.orgTranId)
//            }
//        }
    }

    @Throws(Exception::class)
    private fun parseHistoryData(purchaseData: IosNotiReceiptInfo): GoodsShoppingHistoryDto {
        val startDate = purchaseData.purchaseDateMs!!
        val endDate = purchaseData.expiresDateMs!!
//        val voucherNo: Int = parseVoucherNo(purchaseData.productId!!)
        val membership = membershipRepository.findByProductIdAndActiveIsTrue(purchaseData.productId!!)
        return GoodsShoppingHistoryDto(userId = purchaseData.userId, created = Date(startDate.toLong()).toInstant(), expirationDate = Date(endDate.toLong()).toInstant(), webOrderId = purchaseData.webOrderLineItemId!!, status = 0, membershipId = membership!!.id!!.toInt(), orgTranId = purchaseData.originalTransactionId)
    }

    @Throws(Exception::class)

    private fun insertIosShoppingHistory(purchaseId: IosPurchaseIdDto, expDateMs: Instant, productId: String, renewStatus: String, notificationType: String, webOrderId: String) {

        val historyData = IosPurchaseDataDto(null, purchaseId.iosShoppingId, expDateMs, productId, renewStatus, notificationType, webOrderId)
        if (mapper.selectIosShoppingHisByWebOrderId(webOrderId) === 0) {
            mapper.insertIosShoppingHistory(historyData)
        }
    }

    @Throws(AppleReceiptException::class)
    private fun insertIosShoppingHistory(response: IosNotiReceiptInfo) {
//        val lastReceipt: IosNotiReceiptInfo = getLatestReceipt(response.unifiedReceipt!!.latestReceiptInfo)
        var purchaseId = IosPurchaseIdDto(orgTranId = response.originalTransactionId)
        purchaseId = mapper.selectIosPurchaseIdByOrgTranId(purchaseId)[0]
        response.userId = purchaseId.userId
        insertIosShoppingHistory(purchaseId, Date(response.expiresDateMs!!.toLong()).toInstant(), response.productId!!, response.autoRenewStatus!!, response.notificationType!!, response.webOrderLineItemId!!)
        try {
            insertPurchaseData(response)
        } catch (exception: AppleReceiptException) {
            exception.printStackTrace()
        }
    }

    @Throws(Exception::class)
    private fun insertIosShoppingHistory(verityResponse: IosReceiptVerifyResponse) {
        val lastReceipt = getLatestReceipt(verityResponse.latestReceiptInfo)
        var purchaseId = IosPurchaseIdDto(orgTranId = lastReceipt.originalTransactionId)

        purchaseId = mapper.selectIosPurchaseIdByOrgTranId(purchaseId).get(0)
        lastReceipt.userId = purchaseId.userId
//        lastReceipt.ishId = purchaseId.iosShoppingId
        insertIosShoppingHistory(purchaseId, Date(lastReceipt.expiresDateMs!!.toLong()).toInstant(), lastReceipt.productId!!, "OtherCase", "APPLICATION_CALL", lastReceipt.webOrderLineItemId!!)
        try {
            insertPurchaseData(lastReceipt)
        } catch (exception: AppleReceiptException) {
            exception.printStackTrace()
        }
    }


    private fun checkAppleSubscribing(originalTransactionId: String): Boolean {
        val searchId = IosPurchaseIdDto()
        searchId.orgTranId = originalTransactionId
        val purchaseIds = mapper.selectIosPurchaseIdByOrgTranId(searchId)

        if (purchaseIds.size == 0) {
            println("RETURN TRUE")
            return true
        }

        val ishIds: MutableList<Int> = mutableListOf<Int>()
        for (purchaseId in purchaseIds) {
            ishIds.add(purchaseId.iosShoppingId!!)
        }
        return mapper!!.checkAppleSubscribing(ishIds) === 0
    }

    private fun getLastPaid(response: IosReceiptVerifyResponse): IosNotiReceiptInfo? {
        val latestReceiptInfo: List<IosNotiReceiptInfo> = response.latestReceiptInfo
        return if (latestReceiptInfo.size > 0) {
            latestReceiptInfo.get(0)
        } else null
    }

    private fun insertOriginalId(params: IosReceiptVerifyParams, purchaseData: IosNotiReceiptInfo, receiptFilePath: String?) {
        val insertData = IosPurchaseIdDto()
        val userId: Int = params.userId!!
        val orgTranId: String = purchaseData.originalTransactionId!!
        var result = 0
        insertData.userId = userId
        insertData.orgTranId = orgTranId
        val purchaseIds: List<IosPurchaseIdDto> = mapper!!.selectAlreadyRegisted(insertData)
        if (receiptFilePath != null) {
            insertData.receiptFilePath = receiptFilePath
        }
        if (purchaseIds.size > 0) {
            for (purchaseId in purchaseIds) {
                if (purchaseId.userId !== userId) {
                    if (purchaseId.active === 0) {
                        purchaseId.active = -1
                        mapper.updateActivationPurchaseId(purchaseId)
                        continue
                    }
                }
                if (purchaseId.active !== 0) {
                    if (purchaseId.userId === userId && purchaseId.orgTranId.equals(orgTranId)) {
                        purchaseId.active = 0
                        mapper.updateActivationPurchaseId(purchaseId)
                        continue
                    }
                }
            }
        } else {
            println("insertData :: ${insertData.userId}, ${insertData.orgTranId}, ${insertData.receiptFilePath}")
            result = mapper.insertIosShoppingId(insertData)
        }
        if (result != 1) {
            println("DataBase Error")
            println(insertData.toString())
        }
    }

    private fun getLatestReceipt(receiptInfoList: List<IosNotiReceiptInfo>?): IosNotiReceiptInfo {
//        var latestPurchaseDateMs = 0L
//        var latestReceipt: IosNotiReceiptInfo? = null
//        for (receiptInfo in receiptInfoList!!) {
//            val thisPurchaseDateMs: Long = receiptInfo.expiresDateMs!!.toLong()
//            if (latestPurchaseDateMs < thisPurchaseDateMs) {
//                latestPurchaseDateMs = thisPurchaseDateMs
//                latestReceipt = receiptInfo
//            }
//        }
//        return latestReceipt!!
        return receiptInfoList!![0]
    }

    @Throws(AppleReceiptException::class)
    private fun verifyIosServer(params: IosReceiptVerifyParams): IosReceiptVerifyResponse {
        var response: IosReceiptVerifyResponse = sendReceiptIosServer(prodVerifyUrl!!, params)
        if (response.status == 21007) {
            response = sendReceiptIosServer(sandboxVerifyUrl!!, params)
        }
        return response
    }

    @Throws(AppleReceiptException::class)
    private fun sendReceiptIosServer(appleUrl: String, params: IosReceiptVerifyParams): IosReceiptVerifyResponse {
        var connection: HttpURLConnection? = null
        try {
            val url = URL(appleUrl)
            val verifyParam: HashMap<Any?, Any?> = HashMap<Any?, Any?>()
            verifyParam["receipt-data"] = params.receiptData
            verifyParam["password"] = appSharePassword
            connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("Accept", "application/json")
            connection.doOutput = true
            val outputStream = connection.outputStream
            val requestData = gson.toJson(verifyParam).toByteArray()
            outputStream.write(requestData)
            val reader = BufferedReader(InputStreamReader(connection.inputStream))
            val responseBuilder = StringBuilder()
            var responseLine: String?
            while (reader?.readLine().also { responseLine = it } != null) {
                responseBuilder?.append(responseLine?.trim { it <= ' ' })
            }
            val response: IosReceiptVerifyResponse = gson.fromJson(responseBuilder.toString(), IosReceiptVerifyResponse::class.java)
            return response
        } catch (exception: MalformedURLException) {
            println("iOS Server URL Error")
            throw AppleReceiptException(exception)
        } catch (exception: IOException) {
            println("getIosServerResponse IOException")
            throw AppleReceiptException(exception)
        } catch (exception: Exception) {
            println("${exception.printStackTrace()}")
            throw Exception(exception)
        } finally {
            connection!!.disconnect()
        }
    }

    private fun selectOriginalId(params: IosReceiptVerifyParams, lastPaid: IosNotiReceiptInfo): IosPurchaseIdDto? {
        val iosPurchaseId = IosPurchaseIdDto()
        iosPurchaseId.userId = params.userId
        iosPurchaseId.orgTranId = lastPaid.originalTransactionId
        iosPurchaseId.active = 0
        val iosPurchaseIds: List<IosPurchaseIdDto> = mapper!!.selectIosPurchaseIdByOrgTranId(iosPurchaseId)
        return if (iosPurchaseIds.size == 0) null else iosPurchaseIds[0]
    }

}