package ai.maum.kotlin.controller.impl.message

import ai.maum.kotlin.controller.MessageController
import ai.maum.kotlin.jpa.common.message.MessageFile
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailItemDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MessageCelebMyList: HandlerType {
    var messageController: MessageController? = null


    override fun setController(controller: Any) {
        messageController = controller as MessageController
    }

    override fun setDto(dto: Any) {
        // do nothing
    }

    override fun invoke(): ResponseType {
        val fanmeet = messageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")

        val messageRepository = fanmeet.messageRepository
        val ttsRepository = fanmeet.ttsRepository

        val receivedMessages = messageRepository.findTop20ByWhoAndActiveIsTrueOrderByCreatedDesc(userId)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "received")

        val response = mutableListOf<MessageCelebDetailItemDto>()
        for (message in receivedMessages) {
            val detailItem = MessageCelebDetailItemDto()
            val isTts = message.tts != null
            if(isTts)
                detailItem.ttsUrl = ttsRepository.findByIdAndActiveIsTrue(message.tts!!.id!!)?.url?.mediaUrl()
            detailItem.isTts = isTts
            detailItem.text = message.text
            detailItem.createdTime = message.created

            response.add(detailItem)
        }

        val responseBody = MessageCelebDetailResponseDto()
        responseBody.messages = response

        return ResponseEntity.ok(responseBody)
    }
}