package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.scrap.ScrapBoard
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageScrapBoardAddDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageScrapBoardAdd: HandlerType {
    var myPageController: MyPageController ?= null
    var myPageScrapBoardAddDto: MyPageScrapBoardAddDto ?= null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageScrapBoardAddDto = dto as  MyPageScrapBoardAddDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val dto = myPageScrapBoardAddDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val scrapBoardRepository = fanmeet.scrapBoardRepository

        val newScrapBoard = ScrapBoard()
        newScrapBoard.ownerId = userId
        newScrapBoard.name = dto.boardName?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "boardName")
        scrapBoardRepository.save(newScrapBoard)

        return ResponseEntity.ok(Unit)
    }
}