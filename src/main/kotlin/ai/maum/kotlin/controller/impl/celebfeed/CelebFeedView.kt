package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.block.Block
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedCommentItemDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedItemDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedViewRequestDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedViewResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.toMono

class CelebFeedView : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedViewRequestDto: CelebFeedViewRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val celebFeedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val likeRepository = fanmeet.likeRepository
        val scrapRepository = fanmeet.scrapRepository
        val commentRepository = fanmeet.commentRepository

        val dto = celebFeedViewRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId // null allowed
        val signedIn = (userId != null)

        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(dto.celebFeed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        var blocks : List<Block> = listOf()
        // If logged in, check if the user blocked feed writer
        if (signedIn) {
            blocks = blockRepository.findByWhoAndActiveIsTrue(userId!!)
            for (block in blocks)
                if (block.whom == celebFeed.who)
                    throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Block") // User blocked feed writer
        }

        /* Filling response body */
        /* Feed info */
        val responseBody = CelebFeedViewResponseDto()
        val celebFeedItem = CelebFeedItemDto()

        celebFeedItem.blocked = false
        celebFeedItem.celebFeed = celebFeed.id
        celebFeedItem.feedOwner = celebFeed.who

        // Feed writer info
        val feedOwner = userRepository.findByIdAndActiveIsTrue(celebFeed.who!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "FeedOwner")
        celebFeedItem.feedOwnerName = feedOwner.name
        celebFeedItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

        val accessLevel = celebFeed.accessLevel ?: 0
        celebFeedItem.accessLevelBadgeImageUrl = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(celebFeed.who!!, accessLevel)?.badgeImageUrl?.mediaUrl()

        // Category info
        val category = celebFeedCategoryRepository.findByIdAndActiveIsTrue(celebFeed.category!!)
        celebFeedItem.category = celebFeed.category!!
        celebFeedItem.categoryName = category?.name // nullable

        // Like/scrap info
        celebFeedItem.likeCount = celebFeed.likeCount
        celebFeedItem.scrapCount = celebFeed.scrapCount
        celebFeedItem.commentCount = celebFeed.commentCount
        celebFeedItem.liked = if (signedIn) likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, celebFeed.id!!, Like.TargetType.CELEB_FEED) else false
        celebFeedItem.scraped = if (signedIn) scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, celebFeed.id!!, Scrap.TargetType.CELEB_FEED) else false

        // Media info
        celebFeedItem.pictures = mutableListOf()
        celebFeed.pictureList?.let {
            for (picture in celebFeed.pictureList!!) {
                celebFeedItem.pictures!!.add(picture.url!!.mediaUrl())
            }
        }
        celebFeedItem.video = celebFeed.video?.url?.mediaUrl()
        celebFeedItem.thumbnail = celebFeed.thumbnail?.url?.mediaUrl()
        celebFeedItem.tts = celebFeed.tts?.url?.mediaUrl()
        celebFeedItem.youtube = celebFeed.youtube
        celebFeedItem.accessLevel = celebFeed.accessLevel
        celebFeedItem.modifyYn = celebFeed.modifyYn
        // Others
        celebFeedItem.text = celebFeed.text
        celebFeedItem.updated = celebFeed.created

        /* Comment info */
        // Active may not be true since nested comments can be active and should be shown
        val comments = commentRepository.findByFeedAndSectionOrderByCreatedDesc(celebFeed.id!!, Comment.Section.CELEB_FEED) ?: listOf()
        val commentItemList = mutableListOf<CelebFeedCommentItemDto>()
        val offspringItemList = mutableMapOf<Long, MutableList<CelebFeedCommentItemDto>>()
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        for (comment in comments) {
            val commentItem = CelebFeedCommentItemDto()

            commentItem.id = comment.id

            commentItem.deleted = false
            commentItem.blocked = false

            val commentOwner = userRepository.findByIdAndActiveIsTrue(comment.who!!)

            if (comment.active == false || commentOwner == null) {
                commentItem.deleted = true
                continue
            }
            val block = blocks.find { it.whom == comment.who }
            if (block != null) {
                commentItem.blocked = true
                continue
            }

            commentItem.commentOwner = comment.who!!
            commentItem.commentOwnerName = commentOwner.name
            commentItem.commentOwnerProfileImage = commentOwner.profileImageUrl?.mediaUrl()
            commentItem.badgeUrl = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                    commentOwner.id!!,
                    celebFeed.who!!,
                    UserMembershipHistory.Status.EXPIRED
            )?.membership?.badgeImageUrl?.mediaUrl()
            commentItem.text = comment.text
            commentItem.likeCount = comment.likeCount
            commentItem.picture = comment.picture?.mediaUrl()
            commentItem.tts = comment.tts?.url?.mediaUrl()
            commentItem.updated = comment.created
            commentItem.parent = comment.parent
            commentItem.offspring = mutableListOf()
            commentItem.liked = if (signedIn) likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId!!, comment.id!!, Like.TargetType.CELEB_FEED_COMMENT) else false
            commentItem.modifyYn = comment.modifyYn
            if (commentItem.parent == null)
                commentItemList.add(commentItem)
            else {
                if (!offspringItemList.contains(commentItem.parent!!))
                    offspringItemList[commentItem.parent!!] = mutableListOf()
                offspringItemList[commentItem.parent!!]!!.add(commentItem)
            }
        }

        for (commentItem in commentItemList)
            if (offspringItemList.contains(commentItem.id!!))
                commentItem.offspring!!.addAll(offspringItemList[commentItem.id!!]!!).also { commentItem.offspring!!.sortBy { it.updated }}

        responseBody.feedItem = celebFeedItem
        responseBody.comments = commentItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedViewRequestDto = dto as CelebFeedViewRequestDto
    }
}