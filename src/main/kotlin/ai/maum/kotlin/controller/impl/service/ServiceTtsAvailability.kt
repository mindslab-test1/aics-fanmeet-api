package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.model.http.dto.service.ServiceTtsAvailabilityRequestDto
import org.springframework.http.ResponseEntity

class ServiceTtsAvailability(serviceController: ServiceController, serviceTtsAvailabilityRequestDto: ServiceTtsAvailabilityRequestDto) : () -> ResponseEntity<Unit> {
    override operator fun invoke(): ResponseEntity<Unit> {
        return ResponseEntity.ok(Unit)
    }
}
