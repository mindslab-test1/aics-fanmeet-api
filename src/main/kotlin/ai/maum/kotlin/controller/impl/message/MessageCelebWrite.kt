package ai.maum.kotlin.controller.impl.message

import ai.maum.kotlin.controller.MessageController
import ai.maum.kotlin.jpa.common.message.Message
import ai.maum.kotlin.jpa.common.message.MessageFile
import ai.maum.kotlin.jpa.common.message.ReceivedMessage
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.message.MessageCelebWriteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MessageCelebWrite : HandlerType {
    var messageController: MessageController? = null
    var messageCelebWriteRequestDto: MessageCelebWriteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = messageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val subscribeRepository = fanmeet.subscribeRepository
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val blockRepository = fanmeet.blockRepository
        val messageRepository = fanmeet.messageRepository
        val ttsRepository = fanmeet.ttsRepository
        val receivedMessageRepository = fanmeet.receivedMessageRepository
        val dto = messageCelebWriteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        if(dto.text == null && dto.tts == null) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "no content")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val subscribers = subscribeRepository.findAllByWhomAndActiveIsTrue(userId).map { it.who }
        val blocked = blockRepository.findByWhoAndActiveIsTrue(userId).map { it.whom }
        val candidates = subscribers.toMutableList()
        candidates.removeAll { blocked.contains(it) }

        val message = Message()
        //message.file // After applying image/video
        message.id // null
        message.messageType = Message.MessageType.CELEB_MESSAGE
        message.text = dto.text
        message.who = userId
        message.whom = null // Celeb message

        dto.tts?.let{
            message.tts = ttsRepository.findByIdAndActiveIsTrue(dto.tts!!) ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Tts")
            if (message.tts!!.celeb != userId)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Tts Ownership")
        }

        val messageRecord = messageRepository.save(message)

        val receivedList = mutableListOf<ReceivedMessage>()
        for (candidate in candidates) {
            candidate ?: continue
            if(dto.tts != null){
                val membership =userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(candidate, userId)
                val valid = membership?.membership?.voiceLetter ?: continue
                if (!valid) continue
            }

            val received = ReceivedMessage()
            received.id // null
            received.what = messageRecord
            received.whom = candidate
            receivedList.add(received)
        }
        receivedMessageRepository.saveAll(receivedList)

        fanmeet.authorization.message(messageRecord.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        messageController = controller as MessageController
    }

    override fun setDto(dto: Any) {
        messageCelebWriteRequestDto = dto as MessageCelebWriteRequestDto
    }
}