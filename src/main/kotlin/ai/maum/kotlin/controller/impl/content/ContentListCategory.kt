package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentCategoryItemDto
import ai.maum.kotlin.model.http.dto.content.ContentListCategoryRequestDto
import ai.maum.kotlin.model.http.dto.content.ContentListCategoryResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentListCategory : HandlerType {
    var contentController: ContentController? = null
    var contentListCategoryRequestDto: ContentListCategoryRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val repository = fanmeet.contentCategoryRepository
        val dto = contentListCategoryRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val categoryList = repository.findByCelebAndActiveIsTrueOrderByCreatedAsc(dto.celeb!!)

        val response = ContentListCategoryResponseDto()

        val categories = mutableListOf<ContentCategoryItemDto>()
        for (category in categoryList) {
            val item = ContentCategoryItemDto()
            item.id = category.id
            item.name = category.name
            categories.add(item)
        }
        response.categories = categories

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentListCategoryRequestDto = dto as ContentListCategoryRequestDto
    }
}