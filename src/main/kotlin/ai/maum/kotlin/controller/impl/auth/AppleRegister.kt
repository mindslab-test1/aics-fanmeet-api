package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.common.interest.UserInterested
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.jpa.common.subscribe.Subscribe
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.*
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException


class AppleRegister(private val authController: AuthController,
                    private val appleRegisterDto: AppleRegisterDto,
                    private val appleKeyFile: Resource) : HandlerType {
    override fun invoke(): ResponseType {
        val jsonData = AppleValidation().getTokenPayload(authController, appleRegisterDto.authorizationCode, appleKeyFile)
        val validationResult = AppleValidation().decodeResponseAppleToken(jsonData)

        val regex = Regex("^[가-힣a-zA-Z0-9@!\\-_*]*$")
        if (!regex.matches(appleRegisterDto.name!!)) throw ResponseStatusException(HttpStatus.FORBIDDEN)

        if (appleRegisterDto.name!!.length >= 15) throw ResponseStatusException(HttpStatus.NOT_ACCEPTABLE)
        appleRegisterDto.hello?.let { if (it.length > 100) throw ResponseStatusException(HttpStatus.I_AM_A_TEAPOT) }

        // Check existence in DB
        var user = authController.userRepository.findByAppleUserIdAndActiveIsTrue(validationResult?.sub
                ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        user?.let { throw ResponseStatusException(HttpStatus.CONFLICT) }

        // Check name duplication
        user = authController.userRepository.findUserByNameAndActiveIsTrue(appleRegisterDto.name!!)
        user?.let { throw ResponseStatusException(HttpStatus.IM_USED) }

        // Add user
        val register = User()

        appleRegisterDto.profileImage?.let { register.profileImageUrl = authController.mediaUploader.uploadAsyncProfileImage(appleRegisterDto.profileImage!!) }
        appleRegisterDto.bannerImage?.let { register.bannerImageUrl = authController.mediaUploader.uploadAsyncBannerImage(appleRegisterDto.bannerImage!!) }

        register.appleUserId = validationResult.sub!!
        register.name = appleRegisterDto.name!!
        register.interestedList = mutableListOf()

        val allInterestIds = authController.interestRepository.findAllByActiveIsTrue().map { it.id }
        if (appleRegisterDto.interests != null && appleRegisterDto.interests != "[]") {
            val interests = appleRegisterDto.interests!!.substring(1, appleRegisterDto.interests!!.length - 1).split(", ").map { it.toLong() }.toList()
            for (interestId in appleRegisterDto.interests!!) {
                val interest = UserInterested()
                interest.what = interestId.toLong()

                if (!allInterestIds.contains(interestId.toLong()))
                    continue

                register.interestedList!!.add(interest)
            }
        }
        val savedUser = authController.userRepository.save(register)

        val setting = Setting()
        setting.userId = savedUser.id
        setting.sound = true
        setting.vibration = true
        setting.myLike = true
        setting.myScrap = true
        setting.myComment = true
        setting.celebFeed = true
        setting.celebContent = true
        setting.celebMessage = true
        setting.celebActivity = true
        setting.ttsRequest = true
        setting.ttsResult = true

        val subscribe = Subscribe()
        subscribe.who = savedUser.id
        subscribe.whom = 2

        authController.settingRepository.save(setting)
        authController.subscribeRepository.save(subscribe)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
    }

    override fun setDto(dto: Any) {
    }

}