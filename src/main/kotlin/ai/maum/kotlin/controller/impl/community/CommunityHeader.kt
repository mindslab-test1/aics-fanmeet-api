package ai.maum.kotlin.controller.impl.community

import ai.maum.kotlin.controller.CommunityController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.community.CommunityHeaderRequestDto
import ai.maum.kotlin.model.http.dto.community.CommunityHeaderResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CommunityHeader: HandlerType {
    var communityController: CommunityController? = null
    var communityHeaderRequestDto: CommunityHeaderRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = communityController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = communityHeaderRequestDto ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto")

        val celebUser = fanmeet.userRepository.findByIdAndActiveIsTrueAndCelebActiveIsTrue(dto.celeb!!) ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebUser")
        val celeb = fanmeet.celebRepository.findByUserAndActiveIsTrue(celebUser) ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Celeb")
        val feedCount = fanmeet.feedRepository.countByCelebAndActiveIsTrue(dto.celeb!!)
        val subscriberCount = fanmeet.subscribeRepository.countByWhomAndActiveIsTrue(dto.celeb!!)

        val responseBody = CommunityHeaderResponseDto()
        responseBody.celeb = dto.celeb
        responseBody.celebHello = celeb.hello
        responseBody.celebName = celebUser.name
        responseBody.celebProfileImage = celebUser.profileImageUrl?.mediaUrl()
        responseBody.feedCount = feedCount
        responseBody.subscriberCount = subscriberCount

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        communityController = controller as CommunityController
    }

    override fun setDto(dto: Any) {
        communityHeaderRequestDto = dto as CommunityHeaderRequestDto
    }
}