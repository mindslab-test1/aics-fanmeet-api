package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.feed.Feed
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.CheckLocationRequestDto
import ai.maum.kotlin.model.http.dto.admin.CheckLocationResponseDto
import ai.maum.kotlin.model.http.dto.admin.InitSettingsRequestDto
import ai.maum.kotlin.model.http.dto.feed.FeedItemDto
import ai.maum.kotlin.model.http.dto.feed.FeedListRequestDto
import ai.maum.kotlin.model.http.dto.feed.FeedListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class CheckLocation : HandlerType {
    var adminController: AdminController? = null
    var checkLocationRequestDto: CheckLocationRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = checkLocationRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        if (dto.secret != "sksmsQkrQkrdlek")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val response = CheckLocationResponseDto()
        response.profiles = adminController!!.environment.activeProfiles.toList()

        return ResponseEntity.ok(response)
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        checkLocationRequestDto = dto as CheckLocationRequestDto
    }
}