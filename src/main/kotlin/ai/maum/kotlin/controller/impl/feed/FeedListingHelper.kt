package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.jpa.common.feed.Feed
import ai.maum.kotlin.jpa.common.feed.FeedRepository
import org.springframework.data.domain.Sort
import java.time.Instant

class FeedListingHelper(val feedRepository: FeedRepository) {
    fun listFeeds(get: Long, celeb: Long, created: Instant, sort: Sort) : List<Feed>? {
        return when (get) {
            1L -> feedRepository.findTop1ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            2L -> feedRepository.findTop2ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            3L -> feedRepository.findTop3ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            4L -> feedRepository.findTop4ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            5L -> feedRepository.findTop5ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            6L -> feedRepository.findTop6ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            7L -> feedRepository.findTop7ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            8L -> feedRepository.findTop8ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            9L -> feedRepository.findTop9ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            10L -> feedRepository.findTop10ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            11L -> feedRepository.findTop11ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            12L -> feedRepository.findTop12ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            13L -> feedRepository.findTop13ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            14L -> feedRepository.findTop14ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            15L -> feedRepository.findTop15ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            16L -> feedRepository.findTop16ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            17L -> feedRepository.findTop17ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            18L -> feedRepository.findTop18ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            19L -> feedRepository.findTop19ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            20L -> feedRepository.findTop20ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            21L -> feedRepository.findTop21ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            22L -> feedRepository.findTop22ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            23L -> feedRepository.findTop23ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            24L -> feedRepository.findTop24ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            25L -> feedRepository.findTop25ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            26L -> feedRepository.findTop26ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            27L -> feedRepository.findTop27ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            28L -> feedRepository.findTop28ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            29L -> feedRepository.findTop29ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            30L -> feedRepository.findTop30ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            else -> feedRepository.findTop1ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
        }
    }

    fun listFeedsWith(get: Long, celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>? {
        return when (get) {
            1L -> feedRepository.findTop1ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            2L -> feedRepository.findTop2ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            3L -> feedRepository.findTop3ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            4L -> feedRepository.findTop4ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            5L -> feedRepository.findTop5ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            6L -> feedRepository.findTop6ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            7L -> feedRepository.findTop7ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            8L -> feedRepository.findTop8ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            9L -> feedRepository.findTop9ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            10L -> feedRepository.findTop10ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            11L -> feedRepository.findTop11ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            12L -> feedRepository.findTop12ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            13L -> feedRepository.findTop13ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            14L -> feedRepository.findTop14ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            15L -> feedRepository.findTop15ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            16L -> feedRepository.findTop16ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            17L -> feedRepository.findTop17ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            18L -> feedRepository.findTop18ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            19L -> feedRepository.findTop19ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            20L -> feedRepository.findTop20ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            21L -> feedRepository.findTop21ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            22L -> feedRepository.findTop22ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            23L -> feedRepository.findTop23ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            24L -> feedRepository.findTop24ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            25L -> feedRepository.findTop25ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            26L -> feedRepository.findTop26ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            27L -> feedRepository.findTop27ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            28L -> feedRepository.findTop28ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            29L -> feedRepository.findTop29ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            30L -> feedRepository.findTop30ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            else -> feedRepository.findTop1ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
        }
    }
}