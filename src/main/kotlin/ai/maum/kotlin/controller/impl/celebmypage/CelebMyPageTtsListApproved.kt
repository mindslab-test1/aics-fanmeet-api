package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsListApprovedItemDto
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsListApprovedResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageTtsListApproved : HandlerType {
    var celebMyPageController: CelebMyPageController? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsRepository = fanmeet.ttsRepository
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val approvedItemList = mutableListOf<CelebMyPageTtsListApprovedItemDto>()
        val approvedList = ttsRepository.findByCelebAndStatusAndActiveIsTrue(userId, Tts.Status.APPROVED)
        for (tts in approvedList) {
            val subscriber =
                    if (tts.subscriber != null) fanmeet.userRepository.findByIdAndActiveIsTrue(tts.subscriber!!)
                    else if (tts.status == Tts.Status.SELF) fanmeet.userRepository.findByIdAndActiveIsTrue(userId)
                    else continue

            val approvedItem = CelebMyPageTtsListApprovedItemDto()
            approvedItem.created = tts.created
            approvedItem.subscriberName = subscriber?.name ?: "(존재하지 않는 사용자입니다.)"
            approvedItem.text = tts.text
            approvedItem.tts = tts.id
            approvedItem.ttsUrl = tts.url!!.mediaUrl()

            approvedItemList.add(approvedItem)
        }

        val responseBody = CelebMyPageTtsListApprovedResponseDto()
        responseBody.approvedList = approvedItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}