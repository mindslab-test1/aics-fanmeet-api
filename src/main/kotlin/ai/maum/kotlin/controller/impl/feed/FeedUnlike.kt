package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedUnlikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedUnlike : HandlerType {
    var feedController: FeedController? = null
    var feedUnlikeRequestDto: FeedUnlikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val feedRepository = fanmeet.feedRepository
        val dto = feedUnlikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val feed = feedRepository.findByIdAndActiveIsTrue(dto.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        val like = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.feed!!, Like.TargetType.USER_FEED)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Like")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.feed!!, Like.TargetType.USER_FEED)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        feed.likeCount = feed.likeCount!! - 1
        like.active = false

        feedRepository.save(feed)
        likeRepository.save(like)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedUnlikeRequestDto = dto as FeedUnlikeRequestDto
    }
}