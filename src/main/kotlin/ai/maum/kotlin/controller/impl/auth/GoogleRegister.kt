package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.common.interest.UserInterested
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.GoogleRegisterDto
import com.google.api.client.extensions.appengine.http.UrlFetchTransport
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.json.jackson2.JacksonFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.util.*

class GoogleRegister(private val authController: AuthController, private val googleRegisterDto: GoogleRegisterDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        // ID token verification
        val verifier = GoogleIdTokenVerifier.Builder(UrlFetchTransport.getDefaultInstance(), JacksonFactory()) //
                .setAudience(Collections.singletonList("612983419070-7dac4sjcpjhicu0ro1e9u3a6sb6bli01.apps.googleusercontent.com")) //
                .build()
        val idToken = verifier.verify(googleRegisterDto.idToken)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val payload = idToken.payload ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED)

        val regex = Regex("^[가-힣a-zA-Z0-9@!\\-_*]*$")
        if (!regex.matches(googleRegisterDto.name!!)) throw ResponseStatusException(HttpStatus.FORBIDDEN)

        if (googleRegisterDto.name!!.length >= 15) throw ResponseStatusException(HttpStatus.NOT_ACCEPTABLE)
        googleRegisterDto.hello?.let { if (it.length > 100) throw ResponseStatusException(HttpStatus.I_AM_A_TEAPOT) }

        // Check existence in DB
        var user = authController.userRepository.findByGoogleUserIdAndActiveIsTrue(payload.subject)
        user?.let { throw ResponseStatusException(HttpStatus.CONFLICT) }

        // Check name duplication
        user = authController.userRepository.findUserByNameAndActiveIsTrue(googleRegisterDto.name!!)
        user?.let { throw ResponseStatusException(HttpStatus.IM_USED) }

        // Add user
        val register = User()

        googleRegisterDto.profileImage?.let { register.profileImageUrl = authController.mediaUploader.uploadAsyncProfileImage(googleRegisterDto.profileImage!!) }
        googleRegisterDto.bannerImage?.let { register.bannerImageUrl = authController.mediaUploader.uploadAsyncBannerImage(googleRegisterDto.bannerImage!!) }

        register.googleUserId = payload.subject
        register.name = googleRegisterDto.name!!
        register.interestedList = mutableListOf()

        val allInterestIds = authController.interestRepository.findAllByActiveIsTrue().map { it.id!! }
        googleRegisterDto.interests?.let {
            for (interestId in googleRegisterDto.interests!!) {
                val interest = UserInterested()
                interest.what = interestId

                if (!allInterestIds.contains(interestId))
                    continue

                register.interestedList!!.add(interest)
            }
        }

        authController.userRepository.save(register)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}