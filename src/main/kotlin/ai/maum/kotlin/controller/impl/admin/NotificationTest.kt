package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.NotificationTestRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class NotificationTest : HandlerType {
    var adminController: AdminController? = null
    var notificationTestRequestDto: NotificationTestRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = notificationTestRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        if (dto.secret != "dkffkgndkzmqkfm")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val celeb = 2L
        val me = 25L
        val other = 24L
        val myComment = 189L
        val celebFeed = 106L
        val celebContent = 26L

        fanmeet.userLikeMyComment.send(other, me, myComment)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        notificationTestRequestDto = dto as NotificationTestRequestDto
    }
}