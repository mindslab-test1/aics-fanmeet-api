package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.service.ServiceGetMembershipDetailRequestDto
import ai.maum.kotlin.model.http.dto.service.ServiceGetMembershipDetailResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ServiceGetMembershipDetail : HandlerType {
    var serviceController: ServiceController? = null
    var serviceGetMembershipDetailRequestDto: ServiceGetMembershipDetailRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = serviceController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val repository = fanmeet.membershipRepository
        val dto = serviceGetMembershipDetailRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        dto.productId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ProductId")
        val membership = repository.findByProductIdAndActiveIsTrue(dto.productId!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "No such product")

        val list = emptyList<String>().toMutableList()
        if (membership.badge!!) list.add("멤버십 전용 뱃지")
        if (membership.sticker!!) list.add("전용 스티커")
        if (membership.voiceLetter!!) list.add("TTS 보이스레터 수신")
        if (membership.ttsWrite!!) list.add("TTS 직접 생성 ${membership.ttsWriteLimit}자")
        if (membership.id!! == 3L || membership.id!! == 9L ) list.add("멤버십 콘텐츠 열람")
        if (membership.id!! == 7L) list.add("멤버십 콘텐츠 열람(꽃꽂이 클래스 영상)")

        return ResponseEntity.ok(ServiceGetMembershipDetailResponseDto(
                tier = membership.tier,
                name = membership.name,
                badgeImageUrl = membership.badgeImageUrl?.mediaUrl(),
                badge = membership.badge,
                sticker = membership.sticker,
                voiceLetter = membership.voiceLetter,
                ttsWrite = membership.ttsWrite,
                ttsWriteLimit = membership.ttsWriteLimit,
                benefitDescriptionList = list
        ))
    }

    override fun setController(controller: Any) {
        serviceController = controller as ServiceController
    }

    override fun setDto(dto: Any) {
        serviceGetMembershipDetailRequestDto = dto as ServiceGetMembershipDetailRequestDto
    }
}