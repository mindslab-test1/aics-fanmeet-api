package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.feed.Feed
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.InitSettingsRequestDto
import ai.maum.kotlin.model.http.dto.feed.FeedItemDto
import ai.maum.kotlin.model.http.dto.feed.FeedListRequestDto
import ai.maum.kotlin.model.http.dto.feed.FeedListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class InitSettings : HandlerType {
    var adminController: AdminController? = null
    var initSettingsRequestDto: InitSettingsRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val settingRepository = fanmeet.settingRepository
        val dto = initSettingsRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        if (dto.secret != "sksmsqmfkwlfEkdzhd")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val users = userRepository.findByActiveIsTrue()
        val settings = mutableListOf<Setting>()
        for (user in users) {
            // continue if exists
            if (settingRepository.findByUserIdAndActiveIsTrue(user.id!!) != null)
                continue

            val setting = Setting()

            setting.userId = user.id
            setting.sound = true
            setting.vibration = true
            setting.myLike = true
            setting.myScrap = true
            setting.myComment = true
            setting.celebFeed = true
            setting.celebContent = true
            setting.celebMessage = true
            setting.celebActivity = true
            setting.ttsRequest = true
            setting.ttsResult = true

            settings.add(setting)
        }
        settingRepository.saveAll(settings)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        initSettingsRequestDto = dto as InitSettingsRequestDto
    }
}