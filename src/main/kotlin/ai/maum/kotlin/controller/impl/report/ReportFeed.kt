package ai.maum.kotlin.controller.impl.report

import ai.maum.kotlin.controller.ReportController
import ai.maum.kotlin.jpa.common.report.Report
import ai.maum.kotlin.jpa.common.report.Report.ReportType
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.report.ReportUserFeedRequestDto
import ai.maum.kotlin.model.http.dto.report.ReportUserFeedResponseDto
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ReportFeed : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var reportController: ReportController? = null
    var reportUserFeedRequestDto: ReportUserFeedRequestDto? = null

    override operator fun invoke(): ResponseType {
        val controller = reportController!!

        val feedRepository = controller.fanmeet.feedRepository
        val reportRepository = controller.fanmeet.reportRepository

        val userId: Long = controller.fanmeet.userInfoFactory.`object`.userId!!

        val feedId = reportUserFeedRequestDto?.feedId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val whom = reportUserFeedRequestDto?.whom ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val reason = reportUserFeedRequestDto?.reason

        val report = Report()
        report.who = userId
        report.whom = whom
        report.which = feedId
        report.what = ReportType.USER_FEED
        report.judge = feedRepository.findByIdAndWhoAndActiveIsTrue(feedId, whom)?.celeb ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        report.reason = reason

        reportRepository.save(report)

        return ResponseEntity.ok(ReportUserFeedResponseDto("OK"))
    }

    override fun setController(controller: Any) {
        reportController = controller as ReportController
    }

    override fun setDto(dto: Any) {
        reportUserFeedRequestDto = dto as ReportUserFeedRequestDto
    }
}
