package ai.maum.kotlin.controller.impl.misc

import ai.maum.kotlin.controller.MiscController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.misc.MiscNameDuplicationRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MiscNameDuplication : HandlerType {
    var miscController: MiscController? = null
    var miscNameDuplicationRequestDto: MiscNameDuplicationRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = miscController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val dto = miscNameDuplicationRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
//        val userId = fanmeet.userInfoFactory.`object`.userId
//                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        if (userRepository.findByNameAndActiveIsTrue(dto.name!!) != null)
            throw ResponseStatusException(HttpStatus.CONFLICT, "Name Duplication")

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        miscController = controller as MiscController
    }

    override fun setDto(dto: Any) {
        miscNameDuplicationRequestDto = dto as MiscNameDuplicationRequestDto
    }
}