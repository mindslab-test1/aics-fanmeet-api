package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.misc.ListInterest
import ai.maum.kotlin.controller.impl.misc.MiscNameDuplication
import ai.maum.kotlin.controller.impl.misc.MiscVersionCheck
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.misc.MiscNameDuplicationRequestDto
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class MiscController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @PostMapping("/misc/list/interest")
    fun listInterest() = ListInterest(this)

    @PostMapping("/misc/name/duplication")
    fun miscNameDuplication(@RequestBody @Valid dto: MiscNameDuplicationRequestDto) =
            auth.begin(this, dto, MiscNameDuplication())
                    .handle().end()

    @GetMapping("/misc/version/check")
    fun miscVersionCheck() = auth.begin(this, MiscVersionCheck()).handle().end()

}