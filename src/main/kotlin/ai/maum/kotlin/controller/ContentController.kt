package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.content.*
import ai.maum.kotlin.model.http.dto.content.*
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class ContentController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/community/content/write")
    fun contentWrite(@Valid dto: ContentWriteRequestDto) =
            auth.begin(this, dto, ContentWrite())
                    .with(auth.celebAuthority)
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebUploadContent.send(auth.userId()!!, auth.content()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/content/edit")
    fun contentEdit(@Valid dto: ContentEditRequestDto) =
            auth.content(dto.content).begin(this, dto, ContentEdit())
                    .with(auth.contentWriterAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/delete")
    fun contentDelete(@RequestBody @Valid dto: ContentDeleteRequestDto) =
            auth.communityOwnerFromContent(dto.content).content(dto.content).begin(this, dto, ContentDelete())
                    .or(
                            { with(auth.contentWriterAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    /*
        // TODO: Future functionality
        @Transactional
        @PostMapping("/community/content/search")
        fun contentSearch(@RequestBody @Valid dto: ContentSearchRequestDto) =
                auth.begin(this, dto, ContentSearch())
                        .handle().end()
    */

    @Transactional
    @PostMapping("/community/content/list")
    fun contentList(@RequestBody @Valid dto: ContentListRequestDto) =
            auth.begin(this, dto, ContentList())
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/listcategory")
    fun contentListCategory(@RequestBody @Valid dto: ContentListCategoryRequestDto) =
            auth.begin(this, dto, ContentListCategory())
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/view")
    fun contentView(@RequestBody @Valid dto: ContentViewRequestDto) =
            auth.communityOwnerFromContent(dto.content).content(dto.content).begin(this, dto, ContentView())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/scrap")
    fun contentScrap(@RequestBody @Valid dto: ContentScrapRequestDto) =
            auth.communityOwnerFromContent(dto.content).contentOwnerFromContent(dto.content).content(dto.content).begin(this, dto, ContentScrap())
                    .or(
                            { without(auth.communityOwnershipAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority).without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    //주체가 셀럽일 경우, 유저가 내 콘텐츠를 스크랩 하였음
//                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebScrapContent.send(auth.userId()!!, auth.contentOwner()!!, auth.scrap()!!) } }
                    //주체가 유저일 경우, 셀럽이 내 콘텐츠를 스크랩 하였음
                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userScrapContent.send(auth.userId()!!, auth.contentOwner()!!, auth.scrap()!!) } }
                    .end()


    @Transactional
    @PostMapping("/community/content/like")
    fun contentLike(@RequestBody @Valid dto: ContentLikeRequestDto) =
            auth.communityOwnerFromContent(dto.content).contentOwnerFromContent(dto.content).content(dto.content).begin(this, dto, ContentLike())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()

                    //주체가 셀럽일 경우, 셀럽이 컨텐츠에 좋아요를 남김
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeContent.send(auth.userId()!!, auth.contentOwner()!!, auth.content()!!) } }
                    //주체가 유저일 경우, 유저가 내 콘텐츠에 좋아요를 남김
                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeContent.send(auth.userId()!!, auth.contentOwner()!!, auth.content()!!) } }

                    .end()
    @Transactional
    @PostMapping("/community/content/unlike")
    fun contentUnlike(@RequestBody @Valid dto: ContentUnlikeRequestDto) =
            auth.communityOwnerFromContent(dto.content).content(dto.content).begin(this, dto, ContentUnlike())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/comment/write")
    fun contentWriteComment(@Valid dto: ContentWriteCommentRequestDto) =
            auth.communityOwner(dto.celeb).contentOwnerFromContent(dto.content).content(dto.content).parent(dto.parent).parentOwnerFromParent(dto.parent).begin(this, dto, ContentWriteComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    //댓글이며, 주체가 셀럽일 경우, 셀럽이 콘텐츠에 댓글을 남김
                    .cast { auth.parent()?: let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebCommentContent.send(auth.userId()!!,auth.contentOwner()!!, auth.content()!!, auth.comment()!!) } } }
                    //댓글이며, 주체가 유저일 경우, 유저가 콘텐츠에 댓글을 남김
                    .cast { auth.parent()?: let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userCommentContent.send(auth.userId()!!, auth.contentOwner()!!, auth.comment()!!) } } }
                    //대댓글이며, 주체가 셀럽일 경우 댓글에 셀럽이 답글을 남김
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebSubcomment.send(auth.userId()!!, auth.parentOwner()!!, auth.parent()!!, auth.comment()!!) } } }
                    //대댓글이며, 주체가 유저일 경우, 댓글에 유저가 답글을 남김
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userSubcommentMyComment.send(auth.userId()!!, auth.parentOwner()!!, auth.comment()!!) } } }
                    .end()

    @Transactional
    @PostMapping("/community/content/comment/edit")
    fun contentEditComment(@RequestBody @Valid dto: ContentEditCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).contentFromComment(dto.comment).comment(dto.comment).begin(this, dto, ContentEditComment())
                    .with(auth.commentWriterAuthority).without(auth.blockedByAuthority)
                    .or(
                            { with(auth.communityOwnershipAuthority) },
                            { with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { with(auth.scrappedPrivilege) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/comment/delete")
    fun contentDeleteComment(@RequestBody @Valid dto: ContentDeleteCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).contentFromComment(dto.comment).comment(dto.comment).begin(this, dto, ContentDeleteComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).with(auth.commentWriterAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { without(auth.communityOwnershipAuthority).with(auth.commentWriterAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/comment/like")
    fun contentLikeComment(@RequestBody @Valid dto: ContentLikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).contentFromComment(dto.comment).commentOwnerFromComment(dto.comment).comment(dto.comment).begin(this, dto, ContentLikeComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeComment.send(auth.userId()!!, auth.feedOwner()!!, auth.comment()!!) } }
//                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeMyComment.send(auth.userId()!!, auth.feedOwner()!!, auth.comment()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/content/comment/unlike")
    fun contentUnlikeComment(@RequestBody @Valid dto: ContentUnlikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).contentFromComment(dto.comment).begin(this, dto, ContentUnlikeComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/content/editcategory")
    fun contentEditCategory(@RequestBody @Valid dto: ContentEditCategoryRequestDto) =
            auth.begin(this, dto, ContentEditCategory())
                    .with(auth.celebAuthority)
                    .handle().end()
}