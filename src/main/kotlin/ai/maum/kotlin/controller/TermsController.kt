package ai.maum.kotlin.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping

@Controller
class TermsController {
    @GetMapping("/terms")
    fun blog(model: Model): String {
        return "terms"
    }
}