package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.auth.*
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.interest.InterestRepository
import ai.maum.kotlin.jpa.common.interest.UserInterestedRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.model.http.dto.auth.*
import ai.maum.kotlin.model.media.MediaUploader
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import javax.validation.Valid

@RestController
class AuthController(val restTemplate: RestTemplate,
                     val googleUserTokenRepository: GoogleUserTokenRepository,
                     val kakaoUserTokenRepository: KakaoUserTokenRepository,
                     val naverUserTokenRepository: NaverUserTokenRepository,
                     val appleUserTokenRepository: AppleUserTokenRepository,
                     val userRepository: UserRepository,
                     val userInterestedRepository: UserInterestedRepository,
                     val interestRepository: InterestRepository,
                     val settingRepository: SettingRepository,
                     val subscribeRepository: SubscribeRepository,
                     val mediaUploader: MediaUploader,
                    val appleConfig: AppleConfig
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Transactional
    @PostMapping("/auth/login/google")
    fun googleLogin(@RequestBody @Valid googleLoginDto: GoogleLoginDto) = GoogleLogin(this, googleLoginDto)()

    @Transactional
    @PostMapping("/auth/register/google")
    fun googleRegister(@Valid googleRegisterDto: GoogleRegisterDto) = GoogleRegister(this, googleRegisterDto)()

    @Transactional
    @PostMapping("/auth/login/kakao")
    fun kakaoLogin(@RequestBody @Valid kakaoLoginDto: KakaoLoginDto) = KakaoLogin(this, kakaoLoginDto)()

    @Transactional
    @PostMapping("/auth/register/kakao")
    fun kakaoRegister(@Valid kakaoRegisterDto: KakaoRegisterDto) = KakaoRegister(this, kakaoRegisterDto)()

    @Transactional
    @PostMapping("/auth/login/naver")
    fun naverLogin(@RequestBody @Valid naverLoginDto: NaverLoginDto) = NaverLogin(this, naverLoginDto)()

    @Transactional
    @PostMapping("/auth/register/naver")
    fun naverRegister(@Valid naverRegisterDto: NaverRegisterDto) = NaverRegister(this, naverRegisterDto)()

    @Transactional
    @PostMapping("/auth/login/apple")
    fun appleLogin(@RequestBody @Valid appleLoginDto: AppleLoginDto) = AppleLogin(this, appleLoginDto, appleConfig.appleKeyFile)()

    @Transactional
    @PostMapping("/auth/register/apple")
    fun appleRegister(@Valid appleRegisterDto: AppleRegisterDto) = AppleRegister(this, appleRegisterDto, appleConfig.appleKeyFile)()
}