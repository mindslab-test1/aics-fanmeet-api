package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.service.ServiceCancelMembership
import ai.maum.kotlin.controller.impl.service.ServiceGetMembershipDetail
import ai.maum.kotlin.controller.impl.service.ServiceGetUserMembership
import ai.maum.kotlin.controller.impl.service.ServiceNewMembership
import ai.maum.kotlin.model.http.dto.service.*
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
class ServiceController (val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
//    private val auth = fanmeet.authorization
//
//    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
//    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)
//    fun case() = auth.context()
//
//    @PostMapping("/community/service/tts/availability")
//    fun serviceTtsAvailability(@RequestBody @Valid serviceTtsAvailabilityRequestDto: ServiceTtsAvailabilityRequestDto) = ServiceTtsAvailability(this, serviceTtsAvailabilityRequestDto)
//
//    @PostMapping("/community/service/tts/newscount")
//    fun serviceNewsCount(@RequestBody @Valid serviceNewsCountRequestDto: ServiceNewCountRequestDto) = ServiceNewsCount(this, serviceNewsCountRequestDto)

    private val auth = fanmeet.authorization
    private val service = fanmeet.appleServiceMembership

    @Transactional
    @PostMapping("/community/service/membership/get/user")
    fun serviceGetUserMembership(@RequestBody @Valid dto: ServiceGetUserMembershipRequestDto) =
            auth.begin(this, dto, ServiceGetUserMembership())
                    .handle().end()

    @Transactional
    @PostMapping("/community/service/membership/get/detail")
    fun serviceGetMembershipDetail(@RequestBody @Valid dto: ServiceGetMembershipDetailRequestDto) =
            auth.begin(this, dto, ServiceGetMembershipDetail())
                    .handle().end()

    @Transactional
    @PostMapping("/community/service/membership/new")
    fun serviceNewMembership(@RequestBody @Valid dto: ServiceNewMembershipRequestDto) =
            auth.begin(this, dto, ServiceNewMembership())
                    .handle().end()

    @Transactional
    @PostMapping("/community/service/membership/cancel")
    fun serviceCancelMembership(@RequestBody @Valid dto: ServiceCancelMembershipRequestDto) =
            auth.communityOwner(dto.celeb).begin(this, dto, ServiceCancelMembership())
                    .with(auth.subscriberAuthority).without(auth.blockedByAuthority).without(auth.communityOwnershipAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/service/apple/membership/verifyReceipt")
    fun verifyReceipt(@RequestBody params: IosReceiptVerifyParams?): ResponseEntity<*>? {
        val result: Int = try {
            service.verifyReceipt(params!!)
        } catch (e: Exception) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
        return ResponseEntity.ok(IosVerifyReceiptResponseDto(result))
    }

    @RequestMapping("/community/service/apple/membership/subscribeNoti")
    fun subscribeNoti(request: HttpServletRequest?): ResponseEntity<*>? {
        try {
            service.subscribeNoti(request!!)
        } catch (exception: AppleReceiptException) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
        return ResponseEntity.ok().build<Any>()
    }

    @RequestMapping("/community/service/apple/membership/completePurchase")
    fun completePurchase(@RequestBody params: IosReceiptVerifyParams?): ResponseEntity<*>? {
        try {
            service.completePurchase(params!!)
        } catch (exception: AppleReceiptException) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
        return ResponseEntity.ok().build<Any>()
    }
}