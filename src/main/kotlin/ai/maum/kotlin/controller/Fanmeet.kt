package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.service.apple.AppleServiceMembership
import ai.maum.kotlin.firebase.notification.celeb.activity.comment.CelebCommentContent
import ai.maum.kotlin.firebase.notification.celeb.activity.comment.CelebCommentUserFeed
import ai.maum.kotlin.firebase.notification.celeb.activity.comment.CelebSubcomment
import ai.maum.kotlin.firebase.notification.celeb.activity.commnet.CelebCommentCelebFeed
import ai.maum.kotlin.firebase.notification.celeb.activity.like.CelebLikeCelebFeed
import ai.maum.kotlin.firebase.notification.celeb.activity.like.CelebLikeComment
import ai.maum.kotlin.firebase.notification.celeb.activity.like.CelebLikeContent
import ai.maum.kotlin.firebase.notification.celeb.activity.like.CelebLikeUserFeed
import ai.maum.kotlin.firebase.notification.celeb.activity.scrap.CelebScrapCelebFeed
import ai.maum.kotlin.firebase.notification.celeb.activity.scrap.CelebScrapContent
import ai.maum.kotlin.firebase.notification.celeb.activity.scrap.CelebScrapUserFeed
import ai.maum.kotlin.firebase.notification.celeb.tts.CelebAcceptTts
import ai.maum.kotlin.firebase.notification.celeb.tts.CelebDeclineTts
import ai.maum.kotlin.firebase.notification.celeb.upload.CelebUploadContent
import ai.maum.kotlin.firebase.notification.celeb.upload.CelebUploadFeed
import ai.maum.kotlin.firebase.notification.celeb.upload.CelebUploadMessage
import ai.maum.kotlin.firebase.notification.user.activity.comment.UserCommentCelebFeed
import ai.maum.kotlin.firebase.notification.user.activity.comment.UserCommentContent
import ai.maum.kotlin.firebase.notification.user.activity.comment.UserCommentMyFeed
import ai.maum.kotlin.firebase.notification.user.activity.comment.UserSubcommentMyComment
import ai.maum.kotlin.firebase.notification.user.activity.like.UserLikeCelebFeed
import ai.maum.kotlin.firebase.notification.user.activity.like.UserLikeContent
import ai.maum.kotlin.firebase.notification.user.activity.like.UserLikeMyComment
import ai.maum.kotlin.firebase.notification.user.activity.like.UserLikeMyFeed
import ai.maum.kotlin.firebase.notification.user.activity.scrap.UserScrapCelebFeed
import ai.maum.kotlin.firebase.notification.user.activity.scrap.UserScrapContent
import ai.maum.kotlin.firebase.notification.user.activity.scrap.UserScrapMyFeed
import ai.maum.kotlin.firebase.notification.user.tts.UserRequestTts
import ai.maum.kotlin.gcp.pubsub.core.helper.NewSubscriptionHandler
import ai.maum.kotlin.gcp.pubsub.core.helper.SubscriptionCanceler
import ai.maum.kotlin.grpc.TtsManager
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.celeb.CelebRepository
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedCategoryRepository
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedFileRepository
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.content.ContentCategoryRepository
import ai.maum.kotlin.jpa.common.content.ContentFileRepository
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.feed.FeedCategoryRepository
import ai.maum.kotlin.jpa.common.feed.FeedFileRepository
import ai.maum.kotlin.jpa.common.feed.FeedRepository
import ai.maum.kotlin.jpa.common.interest.ActiveInterestRepository
import ai.maum.kotlin.jpa.common.interest.InterestRepository
import ai.maum.kotlin.jpa.common.interest.UserInterestedRepository
import ai.maum.kotlin.jpa.common.like.LikeRepository
import ai.maum.kotlin.jpa.common.message.MessageFileRepository
import ai.maum.kotlin.jpa.common.message.MessageRepository
import ai.maum.kotlin.jpa.common.message.ReceivedMessageRepository
import ai.maum.kotlin.jpa.common.report.ReportRepository
import ai.maum.kotlin.jpa.common.scrap.ScrapBoardRepository
import ai.maum.kotlin.jpa.common.scrap.ScrapRepository
import ai.maum.kotlin.jpa.common.service.TtsRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.jpa.ttsgen.TtsAddressRepository
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorization
import ai.maum.kotlin.model.http.dto.service.IosMembershipExpire
import ai.maum.kotlin.model.http.rdo.UserInfo
import ai.maum.kotlin.model.media.MediaUploader
import ai.maum.kotlin.model.tts.TtsGenerator
import org.springframework.beans.factory.ObjectFactory
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class Fanmeet(
    /* Http Rest Template */
    val restTemplate: RestTemplate,

    /* Authentication RDO */
    val userInfoFactory: ObjectFactory<UserInfo>,

    /* Authorization */
    val authorization: FanmeetAuthorization,

    /* Authentication Repositories */
    val googleUserTokenRepository: GoogleUserTokenRepository,
    val kakaoUserTokenRepository: KakaoUserTokenRepository,
    val naverUserTokenRepository: NaverUserTokenRepository,
    val appleUserTokenRepository: AppleUserTokenRepository,

    /* Service Repositories */
    val userRepository: UserRepository,
    val subscribeRepository: SubscribeRepository,
    val scrapRepository: ScrapRepository,
    val scrapBoardRepository: ScrapBoardRepository,
    val reportRepository: ReportRepository,
    val likeRepository: LikeRepository,
    val celebFeedRepository: CelebFeedRepository,
    val feedRepository: FeedRepository,
    val contentRepository: ContentRepository,
    val commentRepository: CommentRepository,
    val celebRepository: CelebRepository,
    val interestRepository: InterestRepository,
    val userInterestedRepository: UserInterestedRepository,
    val activeInterestRepository: ActiveInterestRepository,
    val blockRepository: BlockRepository,
    val celebFeedFileRepository: CelebFeedFileRepository,
    val feedFileRepository: FeedFileRepository,
    val contentFileRepository: ContentFileRepository,
    val feedCategoryRepository: FeedCategoryRepository,
    val celebFeedCategoryRepository: CelebFeedCategoryRepository,
    val contentCategoryRepository: ContentCategoryRepository,
    val ttsRepository: TtsRepository,
    val ttsAddressRepository: TtsAddressRepository,
    val membershipRepository: MembershipRepository,
    val userMembershipHistoryRepository: UserMembershipHistoryRepository,
    val messageRepository: MessageRepository,
    val receivedMessageRepository: ReceivedMessageRepository,
    val messageFileRepository: MessageFileRepository,
    val settingRepository: SettingRepository,
    val appleServiceMembership: AppleServiceMembership,
//        val appleMapper: IosReceiptMapper,

    /* Celeb Activity Notifications */
    val celebCommentCelebFeed: CelebCommentCelebFeed,
    val celebCommentContent: CelebCommentContent,
    val celebCommentUserFeed: CelebCommentUserFeed,
    val celebLikeComment: CelebLikeComment,
    val celebLikeUserFeed: CelebLikeUserFeed,
    val celebLikeCelebFeed: CelebLikeCelebFeed,
    val celebLikeContent: CelebLikeContent,
    val celebScrapCelebFeed: CelebScrapCelebFeed,
    val celebScrapContent: CelebScrapContent,
    val celebScrapUserFeed: CelebScrapUserFeed,
    val celebSubcomment: CelebSubcomment,

    /* Media Server Endpoint */
    val mediaUploader: MediaUploader,

    /* Tts Manager(Grpc) */
    val ttsManager: TtsManager,

    /* Tts Generator(Media-JPA) */
    val ttsGenerator: TtsGenerator,

    /* Celeb Tts Notifications */
    val celebAcceptTts: CelebAcceptTts,
    val celebDeclineTts: CelebDeclineTts,
    /* Celeb Upload Notifications */
    val celebUploadContent: CelebUploadContent,
    val celebUploadFeed: CelebUploadFeed,
    val celebUploadMessage: CelebUploadMessage,

    /* User Comment Notifications */
    val userCommentMyFeed: UserCommentMyFeed,
    val userSubcommentMyComment: UserSubcommentMyComment,
    val userCommentCelebFeed: UserCommentCelebFeed,
    val userCommentContent: UserCommentContent,
    /* User Like Notifications */
    val userLikeMyComment: UserLikeMyComment,
    val userLikeMyFeed: UserLikeMyFeed,
    val userLikeCelebFeed: UserLikeCelebFeed,
    val userLikeContent: UserLikeContent,
    /* User Scrap Notifications */
    val userScrapMyFeed: UserScrapMyFeed,
    val userScrapCelebFeed: UserScrapCelebFeed,
    val userScrapContent: UserScrapContent,

    val userRequestTts: UserRequestTts,

    /* Billing Process Helpers */
    val newSubscriptionHandler: NewSubscriptionHandler,
    val subscriptionCanceler: SubscriptionCanceler,

    val iosMembershipExpire: IosMembershipExpire
)
