package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.setting.SettingListMembership
import ai.maum.kotlin.controller.impl.setting.SettingLoadFlags
import ai.maum.kotlin.controller.impl.setting.SettingSaveFlags
import ai.maum.kotlin.model.http.dto.setting.SettingSaveFlagsRequestDto
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class SettingController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/setting/membership/list")
    fun settingListMembership() =
            auth.begin(this, SettingListMembership())
                    .handle().end()

    @Transactional
    @PostMapping("/setting/saveflags")
    fun settingSaveFlags(@RequestBody @Valid dto: SettingSaveFlagsRequestDto) =
            auth.begin(this, dto, SettingSaveFlags())
                    .handle().end()

    @Transactional
    @PostMapping("/setting/loadflags")
    fun settingLoadFlags() =
            auth.begin(this, SettingLoadFlags())
                    .handle().end()
}