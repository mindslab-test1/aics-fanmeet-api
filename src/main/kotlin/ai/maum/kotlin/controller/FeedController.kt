package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.feed.*
import ai.maum.kotlin.model.http.dto.feed.*
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class FeedController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/community/feed/write")
    fun feedWrite(@Valid dto: FeedWriteRequestDto) =
            auth.communityOwner(dto.celeb).begin(this, dto, FeedWrite())
                    .with(auth.subscriberAuthority).without(auth.blockedByAuthority).without(auth.communityOwnershipAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/edit")
    fun feedEdit(@Valid dto: FeedEditRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).feed(dto.feed).begin(this, dto, FeedEdit())
                    .with(auth.feedWriterAuthority).with(auth.subscriberAuthority).without(auth.blockedByAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/delete")
    fun feedDelete(@RequestBody @Valid dto: FeedDeleteRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).feed(dto.feed).begin(this, dto, FeedDelete())
                    .or(
                            { with(auth.feedWriterAuthority).with(auth.subscriberAuthority).without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/search")
    fun feedSearch(@RequestBody @Valid dto: FeedSearchRequestDto) =
            auth.begin(this, dto, FeedSearch())
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/list")
    fun feedList(@RequestBody @Valid dto: FeedListRequestDto) =
            auth.begin(this, dto, FeedList())
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/listcategory")
    fun feedListCategory() =
            auth.begin(this, FeedListCategory())
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/view")
    fun feedView(@RequestBody @Valid dto: FeedViewRequestDto) =
            auth.begin(this, dto, FeedView())
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/scrap")
    fun feedScrap(@RequestBody @Valid dto: FeedScrapRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).feedOwnerFromFeed(dto.feed).feed(dto.feed).begin(this, dto, FeedScrap())
                    .or(
                            { with(auth.subscriberAuthority).without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
//                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebScrapUserFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.scrap()!!) } }

                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userScrapMyFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.scrap()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/feed/like")
    fun feedLike(@RequestBody @Valid dto: FeedLikeRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).feedOwnerFromFeed(dto.feed).feed(dto.feed).begin(this, dto, FeedLike())
                    .or(
                            { without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    .cast {
                        if (auth.userId()!! == auth.communityOwner()!!)
                            without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeUserFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.feed()!!, auth.communityOwner()!!) }
                        else
                            without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeUserFeed.sendHighlight(auth.userId()!!, auth.feedOwner()!!, auth.feed()!!) }
                    }
                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeMyFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.feed()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/feed/unlike")
    fun feedUnlike(@RequestBody @Valid dto: FeedUnlikeRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).begin(this, dto, FeedUnlike())
                    .or(
                            { without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/comment/write")
    fun feedWriteComment(@Valid dto: FeedWriteCommentRequestDto) =
            auth.communityOwnerFromFeed(dto.feed).feedOwnerFromFeed(dto.feed).feed(dto.feed).parent(dto.parent).parentOwnerFromParent(dto.parent).begin(this, dto, FeedWriteComment())
                    .or(
                            { without(auth.blockedByAuthority).with(auth.subscriberAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()

                    .cast {
                        if (auth.userId()!! == auth.communityOwner()!!)
                            auth.parent()
                                    ?: let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebCommentUserFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.feed()!!, auth.comment()!!, auth.communityOwner()!!) } }
                        else
                            auth.parent()
                                    ?: let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebCommentUserFeed.sendHighlight(auth.userId()!!, auth.feedOwner()!!, auth.feed()!!, auth.comment()!!) } }
                    }
                    .cast {
                        auth.parent()
                                ?: let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userCommentMyFeed.send(auth.userId()!!, auth.feedOwner()!!, auth.comment()!!) } }
                    }
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebSubcomment.send(auth.userId()!!, auth.parentOwner()!!, auth.parent()!!, auth.comment()!!) } } }
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userSubcommentMyComment.send(auth.userId()!!, auth.parentOwner()!!, auth.comment()!!) } } }
                    .end()

    @Transactional
    @PostMapping("/community/feed/comment/edit")
    fun feedEditComment(@RequestBody @Valid dto: FeedEditCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).comment(dto.comment).begin(this, dto, FeedEditComment())
                    .or(
                            { with(auth.communityOwnershipAuthority) },
                            { with(auth.commentWriterAuthority).with(auth.subscriberAuthority).without(auth.blockedByAuthority) }
                    )

                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/comment/delete")
    fun feedDeleteComment(@RequestBody @Valid dto: FeedDeleteCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).comment(dto.comment).begin(this, dto, FeedDeleteComment())
                    .or(
                            { with(auth.commentWriterAuthority).with(auth.subscriberAuthority).without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/feed/comment/like")
    fun feedLikeComment(@RequestBody @Valid dto: FeedLikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).commentOwnerFromComment(dto.comment).comment(dto.comment).begin(this, dto, FeedLikeComment())
                    .or(
                            { without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeComment.send(auth.userId()!!, auth.commentOwner()!!, auth.comment()!!) } }
//                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeMyComment.send(auth.userId()!!, auth.commentOwner()!!, auth.comment()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/feed/comment/unlike")
    fun feedUnlikeComment(@RequestBody @Valid dto: FeedUnlikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).begin(this, dto, FeedUnlikeComment())
                    .or(
                            { without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()
}