package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.profile.*
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.profile.*
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class ProfileController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    @Transactional
    @PostMapping("/profile/view")
    fun profileView(@RequestBody @Valid dto: ProfileViewRequestDto) = begin(this, dto, ProfileView())
            .handle().end()

    @Transactional
    @PostMapping("/profile/subscribe")
    fun profileSubscribe(@RequestBody @Valid dto: ProfileSubscribeRequestDto) = begin(this, dto, ProfileSubscribe())
            .handle().end()

    @Transactional
    @PostMapping("/profile/unsubscribe")
    fun profileUnsubscribe(@RequestBody @Valid dto: ProfileUnsubscribeRequestDto) = begin(this, dto, ProfileUnsubscribe())
            .handle().end()

    @Transactional
    @PostMapping("/community/profile/edit")
    fun profileEdit(@RequestBody @Valid dto: ProfileEditRequestDto) =
            auth.begin(this, dto, ProfileEdit())
                    .handle().end()

    @Transactional
    @PostMapping("/community/profile/editactiveinterest")
    fun profileEditActiveInterest(@RequestBody @Valid dto: ProfileEditActiveInterestRequestDto) =
            auth.begin(this, dto, ProfileEditActiveInterest())
                    .handle().end()
}