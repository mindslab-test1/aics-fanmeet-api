package ai.maum.kotlin.grpc

import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import maum.brain.tts.NgTtsServiceGrpc
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceBlockingStub
import maum.brain.tts.NgTtsServiceGrpc.NgTtsServiceStub
import maum.brain.tts.TtsMediaResponse
import maum.brain.tts.TtsRequest
import maum.common.LangOuterClass
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

class GrpcTtsEndpoint(
        private val ttsServerIp: String,
        private val ttsServerPort: Int
) {
    private val logger = LoggerFactory.getLogger(GrpcTtsEndpoint::class.java)
    private val channel: ManagedChannel
    private val blockingStub: NgTtsServiceBlockingStub
    private val asyncStub: NgTtsServiceStub

    @Throws(InterruptedException::class)
    fun shutdown() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }

    @Throws(Exception::class)
    fun getTtsWaveBytes(ttsText: String): List<Byte> {
        val ttsRequest = TtsRequest.newBuilder()
                .setLang(LangOuterClass.Lang.ko_KR)
                .setSampleRate(22050)
                .setText(ttsText)
                .setSpeaker(0)
                .build()
        val waveBytes: Iterator<TtsMediaResponse>
        val byteList: MutableList<Byte> = ArrayList()
        try {
            waveBytes = blockingStub.speakWav(ttsRequest)
            while (waveBytes.hasNext()) {
                val ttsMediaResponse = waveBytes.next()
                byteList.addAll(ttsMediaResponse.mediaData.toList())
            }
        } catch (e: Exception) {
            logger.error(e.toString())
            throw Exception("Error while streaming with tts server: ($ttsServerIp:$ttsServerPort)")
        }

        return byteList
    }

    init {
        try {
            val channelBuilder = ManagedChannelBuilder.forAddress(ttsServerIp, ttsServerPort).usePlaintext()
            channel = channelBuilder.build()
            blockingStub = NgTtsServiceGrpc.newBlockingStub(channel)
            asyncStub = NgTtsServiceGrpc.newStub(channel)
        } catch (e: Exception) {
            logger.error(e.toString())
            throw Exception("Failed to create GrpcTtsEndpoint object: Failed to create channel or stub.")
        }
    }
}