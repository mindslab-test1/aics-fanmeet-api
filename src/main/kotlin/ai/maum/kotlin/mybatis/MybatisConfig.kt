package ai.maum.kotlin.mybatis

import org.apache.ibatis.session.SqlSessionFactory
import org.apache.ibatis.type.JdbcType
import org.mybatis.spring.SqlSessionFactoryBean
import org.mybatis.spring.SqlSessionTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import javax.sql.DataSource

@Configuration
class MybatisConfig {
    @Bean
    @Throws(Exception::class)
    fun sqlSessionFactory(dataSource: DataSource?): SqlSessionFactory? {
        val sqlSessionFactoryBean = SqlSessionFactoryBean()
        sqlSessionFactoryBean.setDataSource(dataSource)
        val resolver = PathMatchingResourcePatternResolver()
         sqlSessionFactoryBean.setMapperLocations(*resolver.getResources("sqlmap/iosPurchaseQuery.xml"))
        val sqlSessionFactory = sqlSessionFactoryBean.getObject()
        sqlSessionFactory!!.configuration.jdbcTypeForNull = JdbcType.NULL
        sqlSessionFactory.configuration.isMapUnderscoreToCamelCase = true
        return sqlSessionFactoryBean.getObject()
    }

    @Bean
    fun sqlSession(sqlSessionFactory: SqlSessionFactory?): SqlSessionTemplate {
        return SqlSessionTemplate(sqlSessionFactory)
    }
}
