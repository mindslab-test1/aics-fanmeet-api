package ai.maum.kotlin.mybatis.mapper;

import ai.maum.kotlin.model.http.dto.service.GoodsShoppingHistoryDto;
import ai.maum.kotlin.model.http.dto.service.IosPurchaseDataDto;
import ai.maum.kotlin.model.http.dto.service.IosPurchaseIdDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Mapper
public interface IosReceiptMapper {
    int selectAlreadyPaid(int userId);
    int insertIosShoppingId(IosPurchaseIdDto insertData);
    List<IosPurchaseIdDto> selectAlreadyRegisted(IosPurchaseIdDto purchaseId);
    List<IosPurchaseIdDto> selectIosPurchaseIdByOrgTranId(IosPurchaseIdDto iosPurchaseId);
    int countIosShoppingHistoryByOrderId(String orderId);
    int insertIosShoppingHistory(IosPurchaseDataDto iosHistoryData);
    int insertMembershipHistory(GoodsShoppingHistoryDto historyData);
    int checkAppleSubscribing(List<Integer> ishIds);
    int selectIosShoppingHisByWebOrderId(String webOrderId);
    int countMembershipHistoryByWebOrderId(String webOrderId, String expirationDate);
    int updateActivationPurchaseId(IosPurchaseIdDto purchaseId);
    int updateMembershipHistoryStatus(String orgTranID);
}
